alter table chemical_entity_table add column source_identifier_id int4;
update chemical_entity_table set source_identifier_id = (select identifier from drug_identifier where drug=id);
alter table chemical_entity_table alter COLUMN source_identifier_id set not null;
drop table drug_identifier;