insert into privilege_table (type) SELECT 'IS_ADMIN' 
WHERE NOT EXISTS (
        SELECT type FROM privilege_table WHERE type = 'IS_ADMIN'
);
