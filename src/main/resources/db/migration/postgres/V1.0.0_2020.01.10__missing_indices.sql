create index chemical_entity_synonyms_foreign_key_idx on chemical_entity_synonyms(chemical_entity_id);

create index target_identifier_foreign_key_idx on target_identifier(target);

create index target_foreign_key_chemical_entity_idx on target_table(chemical_entity_id);

create index drug_brand_names_foreign_key_idx on drug_brand_names(drug_id);
