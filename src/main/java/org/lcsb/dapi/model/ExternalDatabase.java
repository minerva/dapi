package org.lcsb.dapi.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"name"}, name="external_database_unique_name"))
public class ExternalDatabase {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(nullable = false)
  private String name;

  @JsonManagedReference
  @OneToMany(mappedBy = "externalDatabase", fetch = FetchType.EAGER)
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
  private List<Release> releases = new ArrayList<>();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Release> getReleases() {
    return releases;
  }

  public void setReleases(List<Release> releases) {
    this.releases = releases;
  }

  public void addRelease(Release release) {
    this.releases.add(release);
  }
}
