package org.lcsb.dapi.model;

import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Target {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(nullable = false)
  private String name;

  @Column(length = 4096, nullable = true)
  private String description;

  @OneToOne
  private Identifier organism;

  @OneToOne
  private Identifier sourceIdentifier;

  @OneToOne
  private Identifier associatedDisease;

  @JsonManagedReference
  @OneToMany
  @Fetch(FetchMode.SUBSELECT)
  @JoinTable(name = "target_identifier", joinColumns = @JoinColumn(name = "target"), inverseJoinColumns = @JoinColumn(name = "identifier"))
  private List<Identifier> identifiers = new ArrayList<>();

  @JsonManagedReference
  @OneToMany
  @Fetch(FetchMode.SUBSELECT)
  @JoinTable(name = "target_reference", joinColumns = @JoinColumn(name = "target"), inverseJoinColumns = @JoinColumn(name = "identifier"))
  private Set<Identifier> references = new HashSet<>();

  @ElementCollection
  @Fetch(FetchMode.SUBSELECT)
  private Set<String> type = new HashSet<>();

  @JsonBackReference
  @ManyToOne(optional = false)
  private ChemicalEntity chemicalEntity;

  public ChemicalEntity getChemicalEntity() {
    return chemicalEntity;
  }

  public void setChemicalEntity(ChemicalEntity chemicalEntity) {
    this.chemicalEntity = chemicalEntity;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Identifier getOrganism() {
    return organism;
  }

  public void setOrganism(Identifier organism) {
    this.organism = organism;
  }

  public Identifier getSourceIdentifier() {
    return sourceIdentifier;
  }

  public void setSourceIdentifier(Identifier sourceIdentifier) {
    this.sourceIdentifier = sourceIdentifier;
  }

  public List<Identifier> getIdentifiers() {
    return identifiers;
  }

  public Set<String> getTypes() {
    return type;
  }

  public void addType(String type) {
    this.type.add(type);
  }

  public void addIdentifiers(List<Identifier> identifiers) {
    for (Identifier identifier : identifiers) {
      addIdentifier(identifier);
    }

  }

  public void addIdentifier(Identifier identifier) {
    this.identifiers.add(identifier);
  }

  public void addTypes(String[] types) {
    for (String string : types) {
      addType(string);
    }
  }

  public void addTypes(Collection<String> types) {
    for (String string : types) {
      addType(string);
    }
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void addReference(Identifier identifier) {
    this.references.add(identifier);
  }

  public Identifier getAssociatedDisease() {
    return associatedDisease;
  }

  public void setAssociatedDisease(Identifier associatedDisease) {
    this.associatedDisease = associatedDisease;
  }

  public Set<Identifier> getReferences() {
    return references;
  }

  public void setReferences(Set<Identifier> references) {
    this.references = references;
  }

  public void addReferences(Collection<Identifier> references) {
    for (Identifier identifier : references) {
      addReference(identifier);
    }
  }

}
