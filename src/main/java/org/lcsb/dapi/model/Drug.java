package org.lcsb.dapi.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.*;

import javax.persistence.*;

@Entity
@DiscriminatorValue("DRUG")
public class Drug extends ChemicalEntity {

  private Boolean bloodBrainBarrier;

  private Boolean approved;

  @ElementCollection()
  @Fetch(FetchMode.SUBSELECT)
  private Set<String> brandNames = new HashSet<>();

  public Boolean isBloodBrainBarrier() {
    return bloodBrainBarrier;
  }

  public void setBloodBrainBarrier(Boolean bloodBrainBarrier) {
    this.bloodBrainBarrier = bloodBrainBarrier;
  }

  public Boolean isApproved() {
    return approved;
  }

  public void setApproved(Boolean approved) {
    this.approved = approved;
  }

  public Set<String> getBrandNames() {
    return brandNames;
  }

  public void addBrandName(String brandName) {
    if (!brandName.isEmpty()) {
      this.brandNames.add(brandName);
    }
  }

  public void addBrandNames(List<String> brandNames) {
    for (String brandName : brandNames) {
      addBrandName(brandName);
    }
  }

}
