package org.lcsb.dapi.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String name;

  @Column(unique = true)
  private String email;

  @Column(nullable = false)
  private boolean deleted = false;

  @Column(unique = true)
  private String login;

  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
  @Column(nullable = false)
  private String cryptedPassword;

  @Column(nullable = false)
  private boolean enabled = false;

  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
  @JsonManagedReference
  @OneToOne(targetEntity = VerificationToken.class, fetch = FetchType.EAGER, orphanRemoval = true)
  private VerificationToken token;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "user_privilege_map_table", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "privilege_id"))
  private Set<Privilege> privileges = new HashSet<>();

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  private Set<AcceptedReleaseLicense> acceptedReleaseLicenses = new HashSet<>();

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void addPrivilege(Privilege privilege) {
    this.privileges.add(privilege);
  }

  public void removePrivilege(Privilege privilege) {
    this.privileges.remove(privilege);
  }

  public String getLogin() {
    return this.login;
  }

  public Set<Privilege> getPrivileges() {
    return privileges;
  }

  public String getCryptedPassword() {
    return this.cryptedPassword;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public void setCryptedPassword(String cryptedPassword) {
    this.cryptedPassword = cryptedPassword;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  public void addAcceptedReleaseLicense(AcceptedReleaseLicense acceptedReleaseLicense) {
    acceptedReleaseLicenses.add(acceptedReleaseLicense);
    acceptedReleaseLicense.setUser(this);
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public VerificationToken getToken() {
    return token;
  }

  public void setToken(VerificationToken token) {
    this.token = token;
  }

  public Set<AcceptedReleaseLicense> getAcceptedReleaseLicenses() {
    return acceptedReleaseLicenses;
  }

  public void deleteAcceptedReleaseLicense(AcceptedReleaseLicense acceptedReleaseLicense) {
    acceptedReleaseLicenses.remove(acceptedReleaseLicense);
    acceptedReleaseLicense.setUser(null);
  }
}