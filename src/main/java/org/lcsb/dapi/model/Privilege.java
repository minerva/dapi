package org.lcsb.dapi.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "type", "objectid" }))
public class Privilege {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
  private int id;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private PrivilegeType type;

  private Integer objectId;

  /**
   * Constructor for hibernate.
   */
  protected Privilege() {

  }

  public Privilege(PrivilegeType type) {
    this.type = type;
  }

  public Privilege(PrivilegeType acceptedReleaseLicense, int objectId) {
    this.type = acceptedReleaseLicense;
    this.objectId = objectId;
  }

  @Override
  public String toString() {
    if (objectId == null) {
      return type.name();
    } else {
      return type.name() + ":" + objectId;
    }
  }

  public PrivilegeType getType() {
    return type;
  }

  public void setType(PrivilegeType type) {
    this.type = type;
  }

  public Integer getObjectId() {
    return objectId;
  }

  public void setObjectId(Integer objectId) {
    this.objectId = objectId;
  }
}
