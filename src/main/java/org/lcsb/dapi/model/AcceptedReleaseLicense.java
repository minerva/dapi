package org.lcsb.dapi.model;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class AcceptedReleaseLicense {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  @Column(nullable = false)
  private Date date;

  @ManyToOne(optional = false)
  @OnDelete(action = OnDeleteAction.NO_ACTION)
  @JsonIgnore
  private User user;

  @ManyToOne(optional = false)
  @OnDelete(action = OnDeleteAction.NO_ACTION)
  private Release release;

  @PrePersist
  protected void onCreate() {
    date = new Date();
  }

  protected AcceptedReleaseLicense() {

  }

  public AcceptedReleaseLicense(Release release) {
    this.release = release;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Release getRelease() {
    return release;
  }

  public void setRelease(Release release) {
    this.release = release;
  }

}
