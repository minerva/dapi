package org.lcsb.dapi.model;

import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "name", "external_database_id" }))
public class Release {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY)
  private int id;

  @Column(nullable = false)
  private String name;

  @ManyToOne(optional = false)
  private License license;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  @Column(nullable = false)
  private Date timestamp;

  @ManyToOne(optional = false)
  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
  private UploadedFile sourceFile;

  @JsonManagedReference
  @OneToMany(mappedBy = "release", fetch = FetchType.LAZY)
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
  private List<ChemicalEntity> chemicalEntities = new ArrayList<>();

  @JsonBackReference
  @ManyToOne()
  @JoinColumn(nullable = false)
  @JsonProperty(access = com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
  private ExternalDatabase externalDatabase;

  public ExternalDatabase getExternalDatabase() {
    return externalDatabase;
  }

  public void setExternalDatabase(ExternalDatabase externalDatabase) {
    this.externalDatabase = externalDatabase;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public List<ChemicalEntity> getChemicalEntities() {
    return chemicalEntities;
  }

  public void addChemicalEntity(ChemicalEntity drug) {
    chemicalEntities.add(drug);
    drug.setRelease(this);
  }

  public UploadedFile getSourceFile() {
    return sourceFile;
  }

  public void setSourceFile(UploadedFile sourceFile) {
    this.sourceFile = sourceFile;
  }

  public void addChemicalEntities(List<? extends ChemicalEntity> chemicals) {
    for (ChemicalEntity chemicalEntity : chemicals) {
      addChemicalEntity(chemicalEntity);
    }
  }

  public License getLicense() {
    return license;
  }

  public void setLicense(License license) {
    this.license = license;
  }

  public int getId() {
    return id;
  }
}
