package org.lcsb.dapi.model;

import java.util.Arrays;
import java.util.List;

public enum IdentifierType {
  CAS(new String[] { "urn:miriam:cas" }, "cas"),
  DRUG_BANK(new String[] { "urn:miriam:drugbank" }, "drugbank"),
  DRUG_BANK_TARGET(new String[] { "urn:miriam:drugbankv4.target" }, "drugbankv4.target"),
  ENTREZ(new String[] { "urn:miriam:ncbigene" }, "ncbigene"),
  HGNC(new String[] { "urn:miriam:hgnc.symbol" }, "hgnc.symbol"),
  MESH(new String[] { "urn:miriam:mesh" }, "mesh"),
  OMIM(new String[] { "urn:miriam:omim" }, "mim"),
  PUBMED(new String[] { "urn:miriam:pubmed" }, "pubmed"),
  UNIPROT(new String[] { "urn:miriam:uniprot" }, "uniprot"),
  TAXONOMY(new String[] { "urn:miriam:taxonomy" }, "taxonomy"),
  ;

  private List<String> uris;

  private String identifierPrefix;

  private IdentifierType(String[] uris, String prefix) {
    this.uris = Arrays.asList(uris);
    this.identifierPrefix = prefix;
  }

  public List<String> getUris() {
    return uris;
  }

  public String getIdentifierPrefix() {
    return identifierPrefix;
  }
}
