package org.lcsb.dapi.model;

import javax.persistence.*;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.lcsb.dapi.config.EhcacheConfig;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "databaseIdentifier", "resourceIdentifier" }))
@Cacheable
@org.hibernate.annotations.Cache(region = EhcacheConfig.IDENTIFIER_CACHE_INSTANCE, usage = CacheConcurrencyStrategy.READ_WRITE)
public class Identifier {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  /**
   * The type of database for which we have the identifier.
   */
  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private IdentifierType databaseIdentifier;

  /**
   * The unique identifier in a database described by {@link #databaseIdentifier}.
   */
  @Column(nullable = false)
  private String resourceIdentifier;

  protected Identifier() {

  }

  public Identifier(IdentifierType type, String resource) {
    this.databaseIdentifier = type;
    this.resourceIdentifier = resource;
  }

  public IdentifierType getDatabaseIdentifier() {
    return databaseIdentifier;
  }

  public String getResourceIdentifier() {
    return resourceIdentifier;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }

    if (this == o) {
      return true;
    }

    if (o instanceof Identifier) {
      return o.toString().equals(this.toString());
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return this.toString().hashCode();
  }

  @Override
  public String toString() {
    return this.getDatabaseIdentifier().name() + ":" + this.getResourceIdentifier();
  }

  public int getId() {
    return id;
  }
}
