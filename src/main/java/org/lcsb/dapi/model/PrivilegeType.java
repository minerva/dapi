package org.lcsb.dapi.model;

public enum PrivilegeType {
  IS_ADMIN,
  READ
}
