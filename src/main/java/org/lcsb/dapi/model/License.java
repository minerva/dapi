package org.lcsb.dapi.model;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "content", "url" }))
public class License {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(nullable = false)
  private String name;

  @Column(length = 65535)
  private String content = "";

  private String url = "";

  protected License() {

  }

  public License(String name, String url, String content) {
    this.name = name;
    this.url = url;
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public String getContent() {
    return content;
  }

  public String getUrl() {
    return url;
  }

  public int getId() {
    return id;
  }

}
