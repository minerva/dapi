package org.lcsb.dapi.model;

import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "name", "release_id" }))
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "entity_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("ABSTRACT")
@JsonFilter("chemicalEntityFilter")
public abstract class ChemicalEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(nullable = false, length = 256)
  private String name;

  @Column(length = 66535)
  private String description;

  @ElementCollection
  @Fetch(FetchMode.SUBSELECT)
  @Column(length = 1024)
  private Set<String> synonyms = new HashSet<>();

  @JsonManagedReference
  @ManyToOne
  private Identifier sourceIdentifier;

  @JsonManagedReference
  @OneToMany
  @Fetch(FetchMode.SUBSELECT)
  @JoinTable(name = "chemical_annotation", joinColumns = @JoinColumn(name = "chemical"), inverseJoinColumns = @JoinColumn(name = "identifier"))
  private Set<Identifier> annotations = new HashSet<>();

  @JsonManagedReference
  @OneToMany(mappedBy = "chemicalEntity")
  @Fetch(FetchMode.SUBSELECT)
  @Cascade({ org.hibernate.annotations.CascadeType.ALL })
  private List<Target> targets = new ArrayList<>();

  @JsonBackReference
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(nullable = false)
  private Release release;

  protected ChemicalEntity() {
    super();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Identifier getSourceIdentifier() {
    return sourceIdentifier;
  }

  public void setSourceIdentifier(Identifier sourceIdentifier) {
    this.sourceIdentifier = sourceIdentifier;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Set<String> getSynonyms() {
    return synonyms;
  }

  public void addSynonym(String synonym) {
    if (!synonym.isEmpty()) {
      this.synonyms.add(synonym);
    }
  }

  public List<Target> getTargets() {
    return targets;
  }

  public Release getRelease() {
    return release;
  }

  public void setRelease(Release release) {
    this.release = release;
  }

  public void addSynonyms(Collection<String> synonyms) {
    for (String synonym : synonyms) {
      addSynonym(synonym);
    }
  }

  public void addTargets(List<Target> list) {
    for (Target target : list) {
      addTarget(target);
    }
  }

  public void addTarget(Target target) {
    this.targets.add(target);
    target.setChemicalEntity(this);
  }

  public Set<Identifier> getAnnotations() {
    return annotations;
  }

  public void addAnnotations(Set<Identifier> annotations) {
    for (Identifier identifier : annotations) {
      addAnnotation(identifier);
    }
  }

  public void addAnnotation(Identifier identifier) {
    this.annotations.add(identifier);
  }
}