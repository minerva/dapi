package org.lcsb.dapi;

import java.net.URL;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.logging.log4j.core.LoggerContext;
import org.lcsb.dapi.config.SpringSecurityConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[] { HibernateConfig.class,
        SpringSecurityConfig.class
    };
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class[] { WebMvcConfig.class };
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/"};
  }

  @Override
  public void onStartup(ServletContext container) throws ServletException {

    // FIXME: Why this does not work?
    // assert container.setInitParameter("log4jConfigLocation",
    // "/WEB-INF/classes/log4j2.properties");

    // Workaround for above
    String file = "/WEB-INF/classes/log4j2.properties";
    try {
      URL configStream = container.getResource(file);
      LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) org.apache.logging.log4j.LogManager
          .getContext(false);
      context.setConfigLocation(configStream.toURI());
    } catch (Exception e) {
      logger.error("Problem with loading log4j configuration: " + file);
    }
    super.onStartup(container);
  }

}