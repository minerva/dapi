package org.lcsb.dapi;

import java.sql.Connection;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.stereotype.Service;

@Service
public class CustomDatabasePopulator implements DatabasePopulator {

  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  private ConfigProperties config;

  private DataSource dataSource;

  @Autowired
  public CustomDatabasePopulator(ConfigProperties config, DataSource dataSource) {
    this.config = config;
    this.dataSource = dataSource;
  }

  @Override
  public void populate(Connection connection) throws ScriptException {
    String vendor = null;
    if (config.getDatabaseDriver().equals("org.hsqldb.jdbcDriver")) {
      vendor = "hsql";
    } else if (config.getDatabaseDriver().equals("org.postgresql.Driver")) {
      vendor = "postgres";
    } else {
      throw new NotImplementedException("Unsupported database: " + config.getDatabaseDriver());
    }

    Flyway.configure().dataSource(config.getDatabaseUrl(), config.getDatabaseUsername(), config.getDatabasePassword())
        .locations("classpath:db.migration." + vendor)
        .load().migrate();
  }

  public DataSource getDataSource() {
    return dataSource;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

}
