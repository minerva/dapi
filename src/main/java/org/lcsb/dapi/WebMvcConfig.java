package org.lcsb.dapi;

import java.util.List;

import javax.xml.transform.Source;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.controller.IdentifierSerializer;
import org.lcsb.dapi.controller.PageJsonSerializer;
import org.lcsb.dapi.model.Identifier;
import org.springframework.context.annotation.*;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.converter.*;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
@EnableWebMvc
@EnableSpringDataWebSupport
@ComponentScan(basePackages = { "org.lcsb.dapi" })
public class WebMvcConfig implements WebMvcConfigurer {

  Logger logger = LogManager.getLogger();

  @Override
  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
      configurer.enable();
  }
  
  @Bean
  public JavaMailSenderImpl mailSender(ConfigProperties configProperties) {
    JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

    javaMailSender.setProtocol("smtp");
    javaMailSender.setHost(configProperties.getSmtpHost());
    javaMailSender.setPort(configProperties.getSmtpPort());
    javaMailSender.setUsername(configProperties.getSmtpUsername());
    javaMailSender.setPassword(configProperties.getSmtpPassword());

    return javaMailSender;
  }

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    // let know spring how to resolve pagination parameters in controller
    argumentResolvers.add(new PageableHandlerMethodArgumentResolver());
  }

  @Bean
  public Jackson2ObjectMapperBuilder objectMapperBuilder() {
    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
    builder.serializerByType(PageImpl.class, new PageJsonSerializer());
    builder.serializerByType(Identifier.class, new IdentifierSerializer());
    return builder;
  }

  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
    builder.serializerByType(PageImpl.class, new PageJsonSerializer());
    builder.serializerByType(Identifier.class, new IdentifierSerializer());
    builder.indentOutput(true);
    converters.add(new ByteArrayHttpMessageConverter());
    converters.add(new StringHttpMessageConverter());
    converters.add(new SourceHttpMessageConverter<Source>());
    converters.add(new ResourceHttpMessageConverter());
    converters.add(new FormHttpMessageConverter());
    converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
