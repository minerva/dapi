package org.lcsb.dapi.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.*;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
@ComponentScan(basePackages = { "org.lcsb.dapi.config" })
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

  private AuthenticationSuccessHandler successHandler;
  private AuthenticationFailureHandler failureHandler;
  private LogoutSuccessHandler logoutSuccessHandler;
  private List<AuthenticationProvider> authenticationProviders;

  @Autowired
  SpringSecurityConfig(AuthenticationSuccessHandler successHandler,
      AuthenticationFailureHandler failureHandler,
      LogoutSuccessHandler logoutSuccessHandler,
      List<AuthenticationProvider> authenticationProviders) {
    super(false);
    this.successHandler = successHandler;
    this.failureHandler = failureHandler;
    this.logoutSuccessHandler = logoutSuccessHandler;
    this.authenticationProviders = authenticationProviders;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    authenticationProviders.forEach(auth::authenticationProvider);
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring()
        .antMatchers("/")
//        .antMatchers("/api/doLogin")
        .antMatchers("*.html");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
        .and()
        .anonymous()
        .and()
        .exceptionHandling()
        .authenticationEntryPoint(new Http403ForbiddenEntryPoint())
        .and()
        .formLogin()
        .usernameParameter("login")
        .passwordParameter("password")
        .loginProcessingUrl("/api/doLogin")
        .successHandler(successHandler)
        .failureHandler(failureHandler)
        .and()
        .logout()
        .logoutUrl("/api/doLogout").permitAll()
        .logoutSuccessHandler(logoutSuccessHandler)
        .deleteCookies("DAPI_TOKEN")
        .and()
        .authorizeRequests()
        .and()
        .csrf().disable();
  }

}