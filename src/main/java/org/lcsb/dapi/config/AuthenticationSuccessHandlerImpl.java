package org.lcsb.dapi.config;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.model.User;
import org.lcsb.dapi.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

  private static Logger logger = LogManager.getLogger(AuthenticationSuccessHandlerImpl.class);

  private UserService userService;

  public AuthenticationSuccessHandlerImpl(UserService userService) {
    this.userService = userService;
  }

  @Override
  public void onAuthenticationSuccess(
      HttpServletRequest request,
      HttpServletResponse response,
      Authentication authentication) throws IOException {
    response.setStatus(HttpStatus.OK.value());

    User user = userService.getUserByLogin(authentication.getName());
    if (user == null) {
      logger.fatal("Inconsistent state. User doesn't exist");
    } else {
      logger.debug("User " + authentication.getName() + " successfully logged in");

      Map<String, Object> result = new TreeMap<>();
      result.put("info", "Login successful.");
      result.put("token", request.getSession().getId());
      result.put("login", user.getLogin());

      String json = new ObjectMapper().writeValueAsString(result);

      response.setContentType("application/json");
      response.getWriter().print(json);

    }

  }

}
