package org.lcsb.dapi.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.controller.dto.UserDto;
import org.lcsb.dapi.model.User;
import org.lcsb.dapi.service.LdapService;
import org.lcsb.dapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Order(2)
@Service
public class LdapAuthenticationProvider implements AuthenticationProvider {

  private static Logger logger = LogManager.getLogger();
  
  private UserService userService;
  private LdapService ldapService;
  private UserDetailsService userDetailsService;

  @Autowired
  public LdapAuthenticationProvider(UserService userService,
      LdapService ldapService,
      UserDetailsService userDetailsService) {
    this.userService = userService;
    this.ldapService = ldapService;
    this.userDetailsService = userDetailsService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String username = authentication.getName().toLowerCase();
    if (username.isEmpty()) {
      throw new BadCredentialsException("Invalid username.");
    }

    logger.debug("Try logging over LDAP: " + username);

    User existingUser = userService.getUserByLogin(username);

    boolean ldapLoginSuccess;
    try {
      ldapLoginSuccess = ldapService.login(username, (String) authentication.getCredentials());
    } catch (Exception e) {
      throw new AuthenticationServiceException("Connection to LDAP service failed.", e);
    }

    if (!ldapLoginSuccess) {
      throw new BadCredentialsException("Invalid credentials or username.");
    }

    boolean userExistsLocally = existingUser != null;
    if (!userExistsLocally) {
      createLocalUser(authentication);
    }

    return new UsernamePasswordAuthenticationToken(
        username,
        authentication.getCredentials(),
        userDetailsService.loadUserByUsername(username).getAuthorities());
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
  }

  private void createLocalUser(Authentication authentication) {
    UserDto userDTO;
    try {
      userDTO = ldapService.getUserByLogin(authentication.getName());
    } catch (Exception e) {
      throw new UsernameNotFoundException("Could not find username in LDAP.", e);
    }
    User newUser = new User();
    newUser.setLogin(userDTO.getLogin());
    newUser.setEmail(userDTO.getEmail());
    newUser.setEnabled(true);
    // spring requires not null password - the password is hashed and the hash would
    // never be equal to empty string
    newUser.setCryptedPassword("");
    userService.add(newUser);
  }

}
