package org.lcsb.dapi.config;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Order(1)
@Service
public class LocalAuthenticationProvider implements AuthenticationProvider {

  private static Logger logger = LogManager.getLogger();
  
  private DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();

  private UserDetailsService userDetailsService;
  private PasswordEncoder passwordEncoder;

  @Autowired
  public LocalAuthenticationProvider(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
    this.userDetailsService = userDetailsService;
    this.passwordEncoder = passwordEncoder;
  }

  @PostConstruct
  private void init() {
    daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
    daoAuthenticationProvider.setUserDetailsService(userDetailsService);
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String username = authentication.getName();
    if (username.isEmpty()) {
      throw new BadCredentialsException("Username must not be empty.");
    }
    logger.debug("Try loggin locally: "+username);
    return daoAuthenticationProvider.authenticate(authentication);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return daoAuthenticationProvider.supports(authentication);
  }

}
