package org.lcsb.dapi.config;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.dao.UserDao;
import org.lcsb.dapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private UserDao userDao;

  @Autowired
  public UserDetailsServiceImpl(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userDao.getUserByLogin(username);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }
    List<GrantedAuthority> authorities = user.getPrivileges().stream()
        .map(privilege -> new SimpleGrantedAuthority(privilege.toString()))
        .collect(Collectors.toList());
    return org.springframework.security.core.userdetails.User
        .withUsername(username)
        .password(user.getCryptedPassword())
        .disabled(user.isDeleted() || !user.isEnabled())
        .authorities(authorities)
        .build();
  }
}