package org.lcsb.dapi.config;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.NotImplementedException;
import org.lcsb.dapi.model.Release;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {
  Logger logger = LogManager.getLogger();

  @Override
  public boolean hasPermission(Authentication authentication, Object object, Object permission) {
    if (authentication == null) {
      return false;
    }
    if (object == null) {
      return true;
    }
    if (!(permission instanceof String)) {
      throw new NotImplementedException();
    }
    if (object instanceof Release) {
      Release release = (Release) object;
      return authentication.getAuthorities().contains(new SimpleGrantedAuthority(permission + ":" + release.getId()));
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public boolean hasPermission(Authentication authentication, Serializable serializable, String targetType,
      Object permission) {
    throw new NotImplementedException();
  }
}