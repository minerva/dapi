package org.lcsb.dapi.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.interceptor.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.sf.ehcache.config.CacheConfiguration;

@Configuration
@EnableCaching
public class EhcacheConfig implements CachingConfigurer {

  public static final String IDENTIFIER_CACHE_INSTANCE = "identifierRegion";
  public static final String IDENTIFIER_QUERY_CACHE_INSTANCE = "Identifier-query-results-region";

  private final CacheManager cacheManager;
  private final net.sf.ehcache.CacheManager ehCacheManager;

  public EhcacheConfig() {
    CacheConfiguration cacheConfiguration = new CacheConfiguration();
    cacheConfiguration.setMemoryStoreEvictionPolicy("LRU");
    cacheConfiguration.setMaxEntriesLocalHeap(1000);

    net.sf.ehcache.config.Configuration config = new net.sf.ehcache.config.Configuration();
    config.setDefaultCacheConfiguration(cacheConfiguration);

    CacheConfiguration identifierCacheConfiguration = new CacheConfiguration();
    identifierCacheConfiguration.setName(IDENTIFIER_CACHE_INSTANCE);
    identifierCacheConfiguration.setMemoryStoreEvictionPolicy("LRU");
    identifierCacheConfiguration.setMaxEntriesLocalHeap(400000);
    config.addCache(identifierCacheConfiguration);

    CacheConfiguration identifierQueryCacheConfiguration = new CacheConfiguration();
    identifierQueryCacheConfiguration.setName(IDENTIFIER_QUERY_CACHE_INSTANCE);
    identifierQueryCacheConfiguration.setMemoryStoreEvictionPolicy("LRU");
    identifierQueryCacheConfiguration.setMaxEntriesLocalHeap(400000);
    config.addCache(identifierQueryCacheConfiguration);

    CacheConfiguration defaultUpdateTimestampsCacheConfiguration = new CacheConfiguration();
    defaultUpdateTimestampsCacheConfiguration.setName("default-update-timestamps-region");
    defaultUpdateTimestampsCacheConfiguration.setMemoryStoreEvictionPolicy("LRU");
    defaultUpdateTimestampsCacheConfiguration.setMaxEntriesLocalHeap(4096);
    config.addCache(defaultUpdateTimestampsCacheConfiguration);

    CacheConfiguration defaultQueryResultCacheConfiguration = new CacheConfiguration();
    defaultQueryResultCacheConfiguration.setName("default-query-results-region");
    defaultQueryResultCacheConfiguration.setMemoryStoreEvictionPolicy("LRU");
    defaultQueryResultCacheConfiguration.setMaxEntriesLocalHeap(1024);
    config.addCache(defaultQueryResultCacheConfiguration);

    this.ehCacheManager = net.sf.ehcache.CacheManager.newInstance(config);
    this.cacheManager = new EhCacheCacheManager(ehCacheManager);
  }

  @Bean(destroyMethod = "shutdown")
  public net.sf.ehcache.CacheManager ehCacheManager() {
    return ehCacheManager;
  }

  @Bean
  @Override
  public CacheManager cacheManager() {
    return cacheManager;
  }

  @Bean
  @Override
  public KeyGenerator keyGenerator() {
    return new SimpleKeyGenerator();
  }

  @Bean
  @Override
  public CacheResolver cacheResolver() {
    return new SimpleCacheResolver(cacheManager);
  }

  @Bean
  @Override
  public CacheErrorHandler errorHandler() {
    return new SimpleCacheErrorHandler();
  }
}