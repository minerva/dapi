package org.lcsb.dapi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = { "file:/etc/dapi/dapi.properties",
    "classpath:dapi.properties" }, ignoreResourceNotFound = true)
public class ConfigProperties {

  @Value("${fileStorage}")
  private String fileStorage;

  @Value("${baseUrl}")
  private String baseUrl;

  @Value("${smtpPort:25}")
  private Integer smtpPort;

  @Value("${smtpHost}")
  private String smtpHost;

  @Value("${smtpUsername}")
  private String smtpUsername;

  @Value("${smtpPassword}")
  private String smtpPassword;

  @Value("${database.driver}")
  private String databaseDriver;

  @Value("${database.dialect}")
  private String databaseDialect;

  @Value("${database.url}")
  private String databaseUrl;

  @Value("${database.username}")
  private String databaseUsername;

  @Value("${database.password}")
  private String databasePassword;

  @Value("${database.connectionQuery}")
  private String databaseConnectionQuery;

  @Value("${database.showSQL:false}")
  private String databaseShowSQL;

  @Value("${database.secondLevelCache:false}")
  private String databaseSecondLevelCache;

  @Value("${ldap.address:#{null}}")
  private String ldapAddress;

  @Value("${ldap.ssl:#{null}}")
  private String ldapSSL;

  @Value("${ldap.emailField:#{null}}")
  private String ldapEmailAttribute;

  @Value("${ldap.uidField:#{null}}")
  private String ldapUidAttribute;

  @Value("${ldap.baseDn:#{null}}")
  private String ldapBaseDn;

  @Value("${ldap.password:#{null}}")
  private String ldapPassword;

  @Value("${ldap.bindDn:#{null}}")
  private String ldapBindDn;

  @Value("${ldap.port:#{null}}")
  private String ldapPort;

  @Value("${ldap.objectClassFilter:#{null}}")
  private String ldapObjectClassFilter;

  public String getFileStorage() {
    return fileStorage;
  }

  public String getSmtpHost() {
    return smtpHost;
  }

  public int getSmtpPort() {
    return smtpPort;
  }

  public String getSmtpUsername() {
    return smtpUsername;
  }

  public String getSmtpPassword() {
    return smtpPassword;
  }

  public String getDatabaseDriver() {
    return databaseDriver;
  }

  public String getDatabaseDialect() {
    return databaseDialect;
  }

  public String getDatabaseUrl() {
    return databaseUrl;
  }

  public String getDatabaseUsername() {
    return databaseUsername;
  }

  public String getDatabasePassword() {
    return databasePassword;
  }

  public String getDatabaseConnectionQuery() {
    return databaseConnectionQuery;
  }

  public String getDatabaseShowSQL() {
    return databaseShowSQL;
  }

  public void setDatabaseShowSQL(String databaseShowSQL) {
    this.databaseShowSQL = databaseShowSQL;
  }

  public String getLdapAddress() {
    return ldapAddress;
  }

  public boolean getLdapIsSSL() {
    return "true".equalsIgnoreCase(ldapSSL);
  }

  public Integer getLdapPort() {
    String portString = ldapPort;
    if (portString == null || ldapAddress.trim().isEmpty()) {
      if (getLdapIsSSL()) {
        portString = "636";
      } else {
        portString = "389";
      }
    }
    return Integer.parseInt(portString);
  }

  public String getLdapBindDn() {
    return ldapBindDn;
  }

  public String getLdapPassword() {
    return ldapPassword;
  }

  public String getLdapBaseDn() {
    return ldapBaseDn;
  }

  public String getLdapUidAttribute() {
    return ldapUidAttribute;
  }

  public String getLdapEmailAttribute() {
    return ldapEmailAttribute;
  }

  public String getLdapObjectClassFilter() {
    return ldapObjectClassFilter;
  }

  public String getSecondLevelCache() {
    return databaseSecondLevelCache;
  }

  public String getBaseUrl() {
    return baseUrl;
  }

}