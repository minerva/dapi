package org.lcsb.dapi;

public class NotImplementedException extends RuntimeException {

  public NotImplementedException(String string) {
    super(string);
  }

  public NotImplementedException() {
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
