package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImp extends BaseDaoImpl<User> implements UserDao {

  public UserDaoImp() {
    super(User.class);
  }

  @Override
  public User getUserByLogin(String login) {
    return getObjectByParameter("login", login);
  }

  @Override
  public User getUserByEmail(String email) {
    return getObjectByParameter("email", email);
  }

}