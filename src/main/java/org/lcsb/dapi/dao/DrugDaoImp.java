package org.lcsb.dapi.dao;

import java.util.*;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;

import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.model.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class DrugDaoImp extends BaseDaoImpl<ChemicalEntity> implements DrugDao {

  public DrugDaoImp() {
    super(ChemicalEntity.class);
  }

  @Override
  public ChemicalEntity getDrugById(String id, Release release) {
    Map<DrugProperty, Object> options = new HashMap<>();
    options.put(DrugProperty.ID, id);
    options.put(DrugProperty.RELEASE, release);

    List<ChemicalEntity> found = getByFilter(Pageable.unpaged(), options).getContent();

    if (found.isEmpty()) {
      return null;
    } else {
      return found.get(0);
    }
  }

  @Override
  public List<ChemicalEntity> getByRelease(Release release) {
    return getByParameter("release", release);
  }

  @Override
  public void delete(ChemicalEntity drug) {
    drug.getRelease().getChemicalEntities().remove(drug);
    super.delete(drug);
  }

  @Override
  public Page<ChemicalEntity> getByFilter(Pageable pageable, Map<DrugProperty, Object> filterOptions) {
    CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
    CriteriaQuery<ChemicalEntity> criteria = builder.createQuery(ChemicalEntity.class);
    Root<ChemicalEntity> root = criteria.from(ChemicalEntity.class);

    Predicate predicate = createPredicate(filterOptions, root);

    criteria.where(predicate);
    criteria.distinct(true);

    TypedQuery<ChemicalEntity> typedQuery = getCurrentSession().createQuery(criteria);
    if (pageable.isPaged()) {
      typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
      typedQuery.setMaxResults(pageable.getPageSize());
    }
    

    List<ChemicalEntity> sublist = typedQuery.getResultList();
    CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);

    root = countQuery.from(ChemicalEntity.class);

    predicate = createPredicate(filterOptions, root);

    countQuery.select(builder.count(root));
    countQuery.where(predicate);
    countQuery.distinct(true);

    Long count = getCurrentSession().createQuery(countQuery).getSingleResult();
    return new PageImpl<>(sublist, pageable, count);
  }

  private Predicate createPredicate(Map<DrugProperty, Object> filterOptions, Root<ChemicalEntity> root) {
    CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (DrugProperty key : filterOptions.keySet()) {
      if (key.getPropertyName() != null) {
        Object value = filterOptions.get(key);
        if (value instanceof String) {
          Expression<String> keyUpperExpression = builder.upper(root.get(key.getPropertyName()));
          Predicate ctfPredicate = builder.like(keyUpperExpression, value.toString().toUpperCase());
          predicates.add(ctfPredicate);
        } else if (value == null) {
          Expression<String> keyExpression = root.get(key.getPropertyName());
          Predicate ctfPredicate = builder.isNull(keyExpression);
          predicates.add(ctfPredicate);
        } else {
          Expression<String> keyExpression = root.get(key.getPropertyName());
          Predicate ctfPredicate = builder.equal(keyExpression, value);
          predicates.add(ctfPredicate);
        }
      } else if (key.equals(DrugProperty.ID)) {
        Join<ChemicalEntity, Identifier> identifierJoin = root.join("sourceIdentifier");
        Predicate predicate = builder.equal(identifierJoin.get("resourceIdentifier"), filterOptions.get(key));
        predicates.add(predicate);
      } else if (key.equals(DrugProperty.TARGET_IDENTIFIER)) {
        Identifier identifier = (Identifier) filterOptions.get(key);
        Join<ChemicalEntity, Target> targetJoin = root.join("targets");
        Join<Target, Identifier> targetIdentifierJoin = targetJoin.join("identifiers");
        Predicate predicate = builder.and(
            builder.equal(targetIdentifierJoin.get("resourceIdentifier"), identifier.getResourceIdentifier()),
            builder.equal(targetIdentifierJoin.get("databaseIdentifier"), identifier.getDatabaseIdentifier()));
        predicates.add(predicate);
      } else if (key.equals(DrugProperty.TARGET_DISEASE_IDENTIFIER)) {
        Identifier identifier = (Identifier) filterOptions.get(key);
        Join<ChemicalEntity, Target> targetJoin = root.join("targets");
        Join<Target, Identifier> targetIdentifierJoin = targetJoin.join("associatedDisease");
        Predicate predicate = builder.and(
            builder.equal(targetIdentifierJoin.get("resourceIdentifier"), identifier.getResourceIdentifier()),
            builder.equal(targetIdentifierJoin.get("databaseIdentifier"), identifier.getDatabaseIdentifier()));
        predicates.add(predicate);
      } else if (key.equals(DrugProperty.SYNONYM)) {
        Predicate predicate = builder.upper(root.join("synonyms")).in(filterOptions.get(key).toString().toUpperCase());
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }

    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;
  }

}
