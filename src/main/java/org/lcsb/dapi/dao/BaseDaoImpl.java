package org.lcsb.dapi.dao;

import java.util.*;

import javax.persistence.criteria.*;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.query.Query;
import org.lcsb.dapi.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;

@Transactional
public abstract class BaseDaoImpl<T> implements BaseDao<T> {

  protected static Logger logger = LogManager.getLogger();

  @Autowired
  private SessionFactory sessionFactory;

  /**
   * Class of the object that DAO works on.
   */
  private Class<T> clazz;

  /**
   * Default constructor.
   *
   * @param theClass
   *          class of the object that DAO will work on
   */
  public BaseDaoImpl(Class<T> theClass) {
    this.clazz = theClass;
  }

  @Override
  public void add(T entity) {
    getCurrentSession().save(entity);
  }

  @Override
  public void update(T entity) {
    getCurrentSession().update(entity);
  }

  @Override
  public List<T> getAll() {
    Query<T> query = getCurrentSession().createQuery(" from " + clazz.getSimpleName(), clazz);
    return query.list();
  }

  @Override
  public Page<T> getAll(Pageable pageable) {
    List<T> fullList = getAll();
    int startElement = pageable.getPageSize() * pageable.getPageNumber();
    int endElement = Math.min(fullList.size(), startElement + pageable.getPageSize());
    startElement = Math.min(startElement, endElement);

    List<T> sublist = fullList.subList(startElement, endElement);

    return new PageImpl<>(sublist, pageable, fullList.size());
  }

  @Override
  public void evict(T entity) {
    getCurrentSession().evict(entity);
  }

  protected Session getCurrentSession() {
    return getSessionFactory().getCurrentSession();
  }

  @Override
  public void flush() {
    getCurrentSession().flush();
  }

  @Override
  public void delete(T entity) {
    this.sessionFactory.getCurrentSession().delete(entity);
  }

  @Override
  public void clear() {
    for (T entity : getAll()) {
      delete(entity);
    }
  }

  @Override
  public T getById(int id) {
    return getObjectByParameter("id", id);
  }

  @Override
  public T getObjectByParameter(String key, Object parameter) {
    return getObjectByParameter(key, parameter, true);
  }

  @Override
  public T getObjectByParameter(String key, Object parameter, boolean caseSensitive) {
    List<T> list = getByParameter(key, parameter, caseSensitive);
    if (list.size() == 0) {
      return null;
    } else {
      return (T) list.get(0);
    }
  }

  @Override
  public List<T> getByParameter(String key, Object parameter, boolean caseSensitive) {
    return getByParameters(Arrays.asList(new Pair<>(key, parameter)), caseSensitive);
  }

  @Override
  public Page<T> getByParameter(String key, Object parameter, Pageable pageable) {
    return getByParameter(key, parameter, pageable, true);
  }

  @Override
  public List<T> getByParameter(String key, Object parameter) {
    return getByParameter(key, parameter, true);
  }

  @Override
  public List<T> getByParameters(List<Pair<String, Object>> params, boolean caseSensitive) {
    return getByParameters(params, Pageable.unpaged(), caseSensitive).getContent();
  }

  @Override
  public Page<T> getByParameters(List<Pair<String, Object>> params, Pageable pageable, boolean caseSensitive) {
    CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
    CriteriaQuery<T> criteria = builder.createQuery(clazz);
    Root<T> root = criteria.from(clazz);

    List<Predicate> predicates = new ArrayList<>();

    for (Pair<String, Object> param : params) {
      String key = param.getLeft();
      Object value = param.getRight();
      if (!caseSensitive && value instanceof String) {
        Expression<String> keyUpperExpression = builder.upper(root.get(key));
        Predicate ctfPredicate = builder.like(keyUpperExpression, value.toString().toUpperCase());
        predicates.add(ctfPredicate);
      } else if (value == null) {
        Expression<String> keyExpression = root.get(key);
        Predicate ctfPredicate = builder.isNull(keyExpression);
        predicates.add(ctfPredicate);
      } else {
        Expression<String> keyExpression = root.get(key);
        Predicate ctfPredicate = builder.equal(keyExpression, value);
        predicates.add(ctfPredicate);
      }
    }

    criteria.where(builder.and(predicates.toArray(new Predicate[0])));

    Query<T> typedQuery = getCurrentSession().createQuery(criteria);

    if (classShouldBeInLevel2Cache(clazz)) {
      typedQuery.setHint("org.hibernate.cacheable", true);
      typedQuery.setCacheRegion(clazz.getSimpleName() + "-query-results-region");
    }

    Long count = null;
    if (pageable.isPaged()) {
      typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
      typedQuery.setMaxResults(pageable.getPageSize());

      CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);

      countQuery.select(builder.count(countQuery.from(clazz)));
      countQuery.where(builder.and(predicates.toArray(new Predicate[0])));

      count = getCurrentSession().createQuery(countQuery).getSingleResult();
    }

    List<T> sublist = typedQuery.getResultList();

    if (count == null) {
      count = (long) sublist.size();
    }

    return new PageImpl<>(sublist, pageable, count);
  }

  boolean classShouldBeInLevel2Cache(Class<?> clazz2) {
    return clazz2.isAnnotationPresent(Cache.class);
  }

  @Override
  public Page<T> getByParameter(String key, Object parameter, Pageable pageable, boolean caseSensitive) {
    return getByParameters(Arrays.asList(new Pair<>(key, parameter)), pageable, caseSensitive);
  }

  protected SessionFactory getSessionFactory() {
    return sessionFactory;
  }

}
