package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.Identifier;
import org.lcsb.dapi.model.IdentifierType;

public interface IdentifierDao extends BaseDao<Identifier> {

  Identifier getByTypeAndResource(IdentifierType type, String resource);
}