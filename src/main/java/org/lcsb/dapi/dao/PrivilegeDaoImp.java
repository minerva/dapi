package org.lcsb.dapi.dao;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.lcsb.dapi.Pair;
import org.lcsb.dapi.model.Privilege;
import org.lcsb.dapi.model.PrivilegeType;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PrivilegeDaoImp extends BaseDaoImpl<Privilege> implements PrivilegeDao {

  public PrivilegeDaoImp() {
    super(Privilege.class);
  }

  @Override
  public Privilege getPrivilegeForTypeAndObjectId(PrivilegeType type, Integer objectId) {
    List<Privilege> privileges = getByParameters(Arrays.asList(
        new Pair<>("type", type),
        new Pair<>("objectId", objectId)), true);
    if (privileges.size() > 1) {
      throw new IllegalStateException("Impossible DB state. Privileges are constrained to be unique.");
    } else if (privileges.size() == 1) {
      return privileges.get(0);
    } else {
      return new Privilege(type, objectId);
    }
  }

}
