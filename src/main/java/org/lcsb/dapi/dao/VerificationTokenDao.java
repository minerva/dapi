package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.VerificationToken;

public interface VerificationTokenDao extends BaseDao<VerificationToken> {

  VerificationToken getByToken(String token);
}