package org.lcsb.dapi.dao;

public enum DrugProperty {
  ID,
  NAME("name"),
  RELEASE("release"),
  SYNONYM,
  TARGET_IDENTIFIER,
  TARGET_DISEASE_IDENTIFIER,
  ;

  private String propertyName;

  DrugProperty(String propertyName) {
    this.propertyName = propertyName;
  }

  DrugProperty() {
  }

  public String getPropertyName() {
    return propertyName;
  }
}
