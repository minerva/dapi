package org.lcsb.dapi.dao;

import java.util.List;

import org.lcsb.dapi.model.License;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LicenseDao extends BaseDao<License> {

  public List<License> getByUrl(String url);

  public Page<License> getByUrl(String url, Pageable pageable);

  public License getByContentAndUrl(String content, String url);

}