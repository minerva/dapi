package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.*;

public interface PrivilegeDao extends BaseDao<Privilege> {

  public Privilege getPrivilegeForTypeAndObjectId(PrivilegeType type, Integer objectId);

}
