package org.lcsb.dapi.dao;

import java.util.List;
import java.util.Map;

import org.lcsb.dapi.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DrugDao extends BaseDao<ChemicalEntity> {

  public ChemicalEntity getDrugById(String id, Release release);

  public List<ChemicalEntity> getByRelease(Release release);

  public Page<ChemicalEntity> getByFilter(Pageable pageable, Map<DrugProperty, Object> filterOptions);

}