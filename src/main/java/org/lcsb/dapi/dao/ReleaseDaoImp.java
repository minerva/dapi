package org.lcsb.dapi.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.lcsb.dapi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class ReleaseDaoImp extends BaseDaoImpl<Release> implements ReleaseDao {

  @Autowired
  private AcceptedReleaseLicenseDao acceptedReleaseLicenseDao;

  public ReleaseDaoImp() {
    super(Release.class);
  }

  @Override
  public Release getByName(String releaseName) {
    return getObjectByParameter("name", releaseName);
  }

  @Override
  public List<Release> getByExternalDatabase(ExternalDatabase database) {
    return getByParameter("externalDatabase", database);
  }

  @Override
  public Page<Release> getByExternalDatabase(ExternalDatabase database, Pageable pageable) {
    return getByParameter("externalDatabase", database, pageable);
  }

  @Override
  public Release getByExternalDatabase(String releaseName, ExternalDatabase database) {
    List<Release> releases = getByParameter("externalDatabase", database);
    for (Release release : releases) {
      if (release.getName().equals(releaseName)) {
        return release;
      }
    }
    return null;
  }

  @Override
  public void delete(Release release) {
    List<AcceptedReleaseLicense> licenses = acceptedReleaseLicenseDao.getByRelease(release);
    for (AcceptedReleaseLicense acceptedReleaseLicense : licenses) {
      acceptedReleaseLicenseDao.delete(acceptedReleaseLicense);
    }
    super.delete(release);
  }

  @Override
  public List<Release> getByLicense(License license) {
    return getByParameter("license", license);
 }

}