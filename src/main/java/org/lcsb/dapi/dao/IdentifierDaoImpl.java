package org.lcsb.dapi.dao;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.lcsb.dapi.Pair;
import org.lcsb.dapi.model.Identifier;
import org.lcsb.dapi.model.IdentifierType;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class IdentifierDaoImpl extends BaseDaoImpl<Identifier> implements IdentifierDao {

  public IdentifierDaoImpl() {
    super(Identifier.class);
  }

  @Override
  public Identifier getByTypeAndResource(IdentifierType type, String resource) {
    List<Identifier> list = getByParameters(Arrays.asList(
        new Pair<>("databaseIdentifier", type),
        new Pair<>("resourceIdentifier", resource)
        ), true);
    if (list.size() == 0) {
      return null;
    } else {
      return list.get(0);
    }
  }

}
