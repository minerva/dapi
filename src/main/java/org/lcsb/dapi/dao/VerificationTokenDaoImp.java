package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.VerificationToken;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationTokenDaoImp extends BaseDaoImpl<VerificationToken> implements VerificationTokenDao {

  public VerificationTokenDaoImp() {
    super(VerificationToken.class);
  }

  @Override
  public VerificationToken getByToken(String token) {
    return getObjectByParameter("token", token);
  }

}