package org.lcsb.dapi.dao;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.lcsb.dapi.Pair;
import org.lcsb.dapi.model.License;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class LicenseDaoImp extends BaseDaoImpl<License> implements LicenseDao {

  public LicenseDaoImp() {
    super(License.class);
  }

  @Override
  public List<License> getByUrl(String url) {
    return getByParameter("url", url);
  }

  @Override
  public Page<License> getByUrl(String url, Pageable pageable) {
    return getByParameter("url", url, pageable);
  }

  @Override
  public License getByContentAndUrl(String content, String url) {
    List<License> result = getByParameters(Arrays.asList(
        new Pair<>("content", content),
        new Pair<>("url", url)),
        true);

    if (result.size() > 0) {
      return result.get(0);
    } else {
      return null;
    }
  }
}
