package org.lcsb.dapi.dao;

import javax.transaction.Transactional;

import org.lcsb.dapi.model.UploadedFile;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class FileDaoImpl extends BaseDaoImpl<UploadedFile> implements FileDao {

  public FileDaoImpl() {
    super(UploadedFile.class);
  }

}
