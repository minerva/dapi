package org.lcsb.dapi.dao;

import java.util.List;

import org.lcsb.dapi.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReleaseDao extends BaseDao<Release> {

  public Release getByName(String releaseName);

  public List<Release> getByExternalDatabase(ExternalDatabase database);

  public Page<Release> getByExternalDatabase(ExternalDatabase database, Pageable pageable);

  public Release getByExternalDatabase(String releaseName, ExternalDatabase database);

  public List<Release> getByLicense(License license);
}
