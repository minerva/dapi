package org.lcsb.dapi.dao;

import java.util.List;

import org.lcsb.dapi.model.AcceptedReleaseLicense;
import org.lcsb.dapi.model.Release;

public interface AcceptedReleaseLicenseDao extends BaseDao<AcceptedReleaseLicense> {
  List<AcceptedReleaseLicense> getByRelease(Release release);
}