package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.UploadedFile;

public interface FileDao extends BaseDao<UploadedFile> {
}