package org.lcsb.dapi.dao;

import javax.transaction.Transactional;

import org.lcsb.dapi.model.ExternalDatabase;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class ExternalDatabaseDaoImpl extends BaseDaoImpl<ExternalDatabase> implements ExternalDatabaseDao {

  public ExternalDatabaseDaoImpl() {
    super(ExternalDatabase.class);
  }

  @Override
  public ExternalDatabase getByName(String databaseName) {
    return getObjectByParameter("name", databaseName, false);
  }

}