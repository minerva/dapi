package org.lcsb.dapi.dao;

import java.util.List;

import org.lcsb.dapi.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BaseDao<T> {
  void add(T entity);

  public void update(T entity);

  public void evict(T entity);

  public void flush();

  public void clear();

  public void delete(T entity);

  public T getById(int id);

  public List<T> getAll();

  public Page<T> getAll(Pageable pageable);

  T getObjectByParameter(String key, Object parameter);

  T getObjectByParameter(String key, Object parameter, boolean caseSensitive);

  List<T> getByParameter(String key, Object parameter);

  List<T> getByParameter(String key, Object parameter, boolean caseSensitive);

  Page<T> getByParameter(String key, Object parameter, Pageable pageable);

  Page<T> getByParameter(String key, Object parameter, Pageable pageable, boolean caseSensitive);

  List<T> getByParameters(List<Pair<String, Object>> parameters, boolean caseSensitive);

  Page<T> getByParameters(List<Pair<String, Object>> parameters, Pageable pageable, boolean caseSensitive);

}
