package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.User;

public interface UserDao extends BaseDao<User> {
  User getUserByLogin(String username);

  User getUserByEmail(String email);
}