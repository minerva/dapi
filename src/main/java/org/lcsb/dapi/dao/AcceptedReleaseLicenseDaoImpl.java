package org.lcsb.dapi.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.lcsb.dapi.model.AcceptedReleaseLicense;
import org.lcsb.dapi.model.Release;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class AcceptedReleaseLicenseDaoImpl extends BaseDaoImpl<AcceptedReleaseLicense>
    implements AcceptedReleaseLicenseDao {

  public AcceptedReleaseLicenseDaoImpl() {
    super(AcceptedReleaseLicense.class);
  }

  @Override
  public List<AcceptedReleaseLicense> getByRelease(Release release) {
    return getByParameter("release", release);
  }

  @Override
  public void delete(AcceptedReleaseLicense license) {
    license.getUser().deleteAcceptedReleaseLicense(license);
    super.delete(license);
  }

}
