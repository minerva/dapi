package org.lcsb.dapi.dao;

import org.lcsb.dapi.model.ExternalDatabase;

public interface ExternalDatabaseDao extends BaseDao<ExternalDatabase> {

  ExternalDatabase getByName(String databaseName);
}
