package org.lcsb.dapi.controller;

import java.util.*;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.Pair;
import org.lcsb.dapi.dao.DrugProperty;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
public class DrugController extends BaseController {

  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  private DrugService drugService;

  private ReleaseService releaseService;

  private ExternalDatabaseService externalDatabaseService;

  private IdentifierFactory identifierFactory;

  @Autowired
  public DrugController(DrugService drugService, ReleaseService releaseService,
      ExternalDatabaseService externalDatabaseService, IdentifierFactory identifierFactory) {
    this.drugService = drugService;
    this.releaseService = releaseService;
    this.externalDatabaseService = externalDatabaseService;
    this.identifierFactory = identifierFactory;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN') or "
      + "hasPermission(@drugController.getRelease(#releaseName, #databaseName), 'READ')")
  @GetMapping("/api/database/{databaseName}/releases/{releaseName}/drugs/{id}")
  public MappingJacksonValue findById(@PathVariable("id") String id,
      @PathVariable("releaseName") String releaseName,
      @PathVariable("databaseName") String databaseName,
      @RequestParam(value = "columns", required = false) String columns) throws ObjectNotFound {

    Release release = getRelease(releaseName, databaseName);
    ChemicalEntity drug = drugService.findById(id, release, getFilters(columns));
    if (drug == null) {
      throw new ObjectNotFound("Drug with given id does not exist");
    }
    return createMappingJacksonValueWithDrugFiltering(drug, columns);
  }

  private MappingJacksonValue createMappingJacksonValueWithDrugFiltering(Object drug, String columns) {
    MappingJacksonValue wrapper = new MappingJacksonValue(drug);
    SimpleFilterProvider filterProvider = new SimpleFilterProvider();
    for (Pair<String, SimpleBeanPropertyFilter> filter : getFilters(columns)) {
      filterProvider.addFilter(filter.getLeft(), filter.getRight());
    }
    wrapper.setFilters(filterProvider);
    return wrapper;
  }

  private List<Pair<String, SimpleBeanPropertyFilter>> getFilters(String columns) {
    List<Pair<String, SimpleBeanPropertyFilter>> result = new ArrayList<>();
    if (columns != null) {
      result.add(new Pair<>("chemicalEntityFilter",
          SimpleBeanPropertyFilter.filterOutAllExcept(columns.split(","))));
    } else {
      result.add(new Pair<>("chemicalEntityFilter",
          SimpleBeanPropertyFilter.serializeAll()));
    }
    return result;
  }

  public Release getRelease(String releaseName, String databaseName) {
    ExternalDatabase database = externalDatabaseService.getByName(databaseName);
    Release release = releaseService.getByExternalDatabase(releaseName, database);
    return release;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN') or "
      + "hasPermission(@drugController.getRelease(#releaseName, #databaseName), 'READ')")
  @GetMapping("/api/database/{databaseName}/releases/{releaseName}/drugs")
  MappingJacksonValue getDrugsByRelease(@PathVariable("releaseName") String releaseName,
      @PathVariable("databaseName") String databaseName, @NotNull final Pageable pageable,
      @RequestParam Map<String, String> filterParameters,
      @RequestParam(value = "columns", required = false) String columns) throws ObjectNotFound, QueryException {

    Release release = getRelease(releaseName, databaseName);
    if (release == null) {
      throw new ObjectNotFound("Release does not exist");
    }
    Page<ChemicalEntity> result = drugService.findByRelease(release, pageable, extractDrugProperties(filterParameters), getFilters(columns));
    return createMappingJacksonValueWithDrugFiltering(result, columns);
  }

  private Map<DrugProperty, Object> extractDrugProperties(Map<String, String> filterParameters) throws QueryException {
    Map<DrugProperty, Object> result = new HashMap<>();
    for (String string : filterParameters.keySet()) {
      if (string.equalsIgnoreCase("page") || string.equalsIgnoreCase("size") || string.equalsIgnoreCase("columns")) {
        continue;
      }
      try {
        DrugProperty key = DrugProperty.valueOf(string.toUpperCase());
        if (key == DrugProperty.TARGET_IDENTIFIER || key == DrugProperty.TARGET_DISEASE_IDENTIFIER) {
          try {
            Identifier identifier = identifierFactory.createIdentifier(filterParameters.get(string));
            result.put(key, identifier);
          } catch (InvalidArgumentException e) {
            throw new QueryException("Invalid identifier: " + string, e);
          }
        } else {
          result.put(key, filterParameters.get(string));
        }
      } catch (IllegalArgumentException e) {
        throw new QueryException("Unknown parameter: " + string, e);
      }
    }
    return result;
  }
}
