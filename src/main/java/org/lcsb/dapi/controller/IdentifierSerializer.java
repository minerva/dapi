package org.lcsb.dapi.controller;

import java.io.IOException;

import org.lcsb.dapi.model.Identifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class IdentifierSerializer extends JsonSerializer<Identifier> {

  @Override
  public void serialize(Identifier identifier, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
      throws IOException {
    String type = "null";
    if (identifier.getDatabaseIdentifier() != null) {
      type = identifier.getDatabaseIdentifier().getIdentifierPrefix();
    }
    jsonGenerator.writeString(type + ":" + identifier.getResourceIdentifier());
  }
}