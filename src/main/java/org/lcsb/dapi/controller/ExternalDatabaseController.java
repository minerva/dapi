package org.lcsb.dapi.controller;

import javax.validation.constraints.NotNull;

import org.lcsb.dapi.model.ExternalDatabase;
import org.lcsb.dapi.service.ExternalDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExternalDatabaseController extends BaseController {

  private ExternalDatabaseService externalDatabaseService;

  @Autowired
  public ExternalDatabaseController(ExternalDatabaseService externalDatabaseService) {
    this.externalDatabaseService = externalDatabaseService;
  }

  @GetMapping("/api/database/")
  Page<ExternalDatabase> getAll(@NotNull final Pageable pageable) throws Exception {
    return externalDatabaseService.getAll(pageable);
  }

}
