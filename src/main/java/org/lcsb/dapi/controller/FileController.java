package org.lcsb.dapi.controller;

import java.io.IOException;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.model.UploadedFile;
import org.lcsb.dapi.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileController extends BaseController {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private FileService fileService;

  @Autowired
  public FileController(FileService fileService) {
    this.fileService = fileService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PostMapping("/api/files")
  public UploadedFile uploadFile(@RequestBody UploadedFile file) throws IOException {

    fileService.add(file);

    return file;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PatchMapping("/api/files/{id}")
  public UploadedFile uploadContent(@RequestBody byte[] content, @PathVariable Integer id)
      throws IOException, QueryException {
    try {
      return fileService.addContent(id, content);
    } catch (InvalidArgumentException e) {
      throw new QueryException(e);
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @DeleteMapping("/api/files/{id}")
  public void delete(@PathVariable Integer id)
      throws IOException, QueryException {
    fileService.delete(fileService.getById(id));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @GetMapping("/api/files")
  public Page<UploadedFile> getAll(@NotNull final Pageable pageable) throws IOException {
    return fileService.getAll(pageable);
  }

}
