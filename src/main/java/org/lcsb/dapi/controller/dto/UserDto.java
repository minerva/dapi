package org.lcsb.dapi.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDto {
  private String login;
  private String password;
  private String email;
  
  @JsonIgnore
  private String ldapBindDn;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getLdapBindDn() {
    return ldapBindDn;
  }

  public void setLdapBindDn(String ldapBindDn) {
    this.ldapBindDn = ldapBindDn;
  }
}
