package org.lcsb.dapi.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.GitProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigurationController extends BaseController {

  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  private GitProperties gitProperties;

  public ConfigurationController(GitProperties gitProperties) {
    this.gitProperties = gitProperties;
  }

  @GetMapping("/api/configuration/")
  GitProperties getConfiguration() {
    return new GitProperties(gitProperties);
  }

}
