package org.lcsb.dapi.controller;

public class QueryException extends Exception {

  public QueryException(Exception e) {
    super(e);
  }

  public QueryException(String message) {
    super(message);
  }

  public QueryException(String message, Exception e) {
    super(message, e);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
