package org.lcsb.dapi.controller;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.model.License;
import org.lcsb.dapi.service.LicenseService;
import org.lcsb.dapi.service.ObjectInConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class LicenseController extends BaseController {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private LicenseService licenseService;

  @Autowired
  public LicenseController(LicenseService licenseService) {
    this.licenseService = licenseService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PostMapping("/api/license")
  public License createLicense(@RequestBody License license) throws QueryException, ObjectExistsException {
    if (license.getName() == null) {
      throw new QueryException("Name cannot be null");
    }
    if (license.getContent() != null && license.getContent().length() > 65535) {
      throw new QueryException("License content too long");
    }
    licenseService.add(license);
    return license;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @DeleteMapping("/api/license/{licenseId}")
  public void deleteLicense(@PathVariable(name = "licenseId") String licenseId) throws QueryException, ObjectInConflictException {
    licenseService.delete(licenseService.getById(Integer.valueOf(licenseId)));
  }

  @GetMapping("/api/license/")
  Page<License> getAll(@NotNull final Pageable pageable) {
    return licenseService.getAll(pageable);
  }
}
