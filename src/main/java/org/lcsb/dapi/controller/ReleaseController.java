package org.lcsb.dapi.controller;

import java.io.IOException;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.*;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
public class ReleaseController extends BaseController {

  private ReleaseService releaseService;

  private ExternalDatabaseService externalDatabaseService;

  private FileService fileService;

  private LicenseService licenseService;

  private UserService userService;

  @Autowired
  public ReleaseController(ReleaseService releaseService, ExternalDatabaseService externalDatabaseService,
      FileService fileService, LicenseService licenseService, UserService userService) {
    this.releaseService = releaseService;
    this.externalDatabaseService = externalDatabaseService;
    this.fileService = fileService;
    this.licenseService = licenseService;
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PostMapping("/api/database/{databaseName}/releases/")
  Release uploadRelease(@RequestBody Map<String, Object> data,
      @PathVariable("databaseName") String databaseName) throws ObjectNotFound, QueryException, IOException {
    ExternalDatabase database = externalDatabaseService.getByName(databaseName);
    if (database == null) {
      throw new ObjectNotFound("Database doesn't exist");
    }

    UploadedFile file = getFile(data.get("file"));
    License license = getLicense(data.get("license"));

    String name = (String) data.get("name");
    try {
      Release result = releaseService.createReleaseFromFile(file, database, name, license);
      return result;
    } catch (ParserException e) {
      throw new QueryException("Invalid input file", e);
    }
  }

  private UploadedFile getFile(Object fileId) throws QueryException {
    if (fileId == null || !(fileId instanceof Integer)) {
      throw new QueryException("Invalid file id. Expected integer but got: " + fileId);
    }
    Integer id = (Integer) fileId;
    UploadedFile file = fileService.getById(id);
    if (file == null) {
      throw new QueryException("File does not exist");
    }
    return file;
  }

  private License getLicense(Object licenseId) throws QueryException {
    if (licenseId == null || !(licenseId instanceof Integer)) {
      throw new QueryException("Invalid license id. Expected integer but got: " + licenseId);
    }
    Integer id = (Integer) licenseId;
    License license = licenseService.getById(id);
    if (license == null) {
      throw new QueryException("License does not exist");
    }
    return license;
  }

  @GetMapping("/api/database/{databaseName}/releases/")
  Page<Release> getAll(@PathVariable(name = "databaseName", required = true) String databaseName,
      @NotNull final Pageable pageable) throws Exception {
    return releaseService.getByExternalDatabase(externalDatabaseService.getByName(databaseName), pageable);
  }

  @DeleteMapping("/api/database/{databaseName}/releases/{release:.+}")
  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  void delete(@PathVariable(name = "databaseName", required = true) String databaseName,
      @PathVariable(name = "release", required = true) String releaseName) throws Exception {
    ExternalDatabase database = externalDatabaseService.getByName(databaseName);
    if (database == null) {
      throw new ObjectNotFound("Database doesn't exist");
    }
    releaseService.delete(releaseName, database);
  }

  @PostMapping("/api/database/{databaseName}/releases/{release:.+}:acceptLicense")
  @PreAuthorize("isAuthenticated()")
  void acceptLicense(@PathVariable(name = "databaseName", required = true) String databaseName,
      @PathVariable(name = "release", required = true) String releaseName, Authentication authentication)
      throws Exception {
    User user = userService.getUserByLogin(authentication.getName());

    ExternalDatabase database = externalDatabaseService.getByName(databaseName);
    if (database == null) {
      throw new ObjectNotFound("Database doesn't exist");
    }

    Release release = releaseService.getByExternalDatabase(releaseName, database);
    userService.acceptLicense(user, release);
  }

}
