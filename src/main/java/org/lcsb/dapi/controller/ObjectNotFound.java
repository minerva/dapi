package org.lcsb.dapi.controller;

public class ObjectNotFound extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ObjectNotFound(String message) {
    super(message);
  }

}
