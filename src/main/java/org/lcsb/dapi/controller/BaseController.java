package org.lcsb.dapi.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.service.ObjectInConflictException;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.google.gson.Gson;

public class BaseController {

  private static Logger logger = LogManager.getLogger();

  @ExceptionHandler({ Exception.class })
  public ResponseEntity<Object> handleException(Exception e) {
    if (e instanceof ObjectNotFound) {
      return createErrorResponse(e.getMessage(), "Object not found", new HttpHeaders(), HttpStatus.NOT_FOUND);
    } else if (e instanceof QueryException) {
      return createErrorResponse(e.getMessage(), "Bad request", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    } else if (e instanceof ObjectInConflictException || e instanceof ObjectExistsException) {
      return createErrorResponse(e.getMessage(), "Conflict", new HttpHeaders(), HttpStatus.CONFLICT);
    } else if (e instanceof AccessDeniedException) {
      return createErrorResponse(e.getMessage(), "Access denied", new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else if (e instanceof HttpMessageNotReadableException
        || e instanceof MissingServletRequestParameterException
        || e instanceof HttpMediaTypeNotSupportedException
        || e instanceof MethodArgumentTypeMismatchException) {
      logger.error(e, e);
      return createErrorResponse("Query server error.", e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    } else {
      logger.error(e, e);
      return createErrorResponse("Internal server error.", e.getMessage(), new HttpHeaders(),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private ResponseEntity<Object> createErrorResponse(String errorMessage, String error, HttpHeaders httpHeaders,
      HttpStatus status) {

    Map<String, String> response = new HashMap<>();
    response.put("error", errorMessage);
    response.put("reason", error);
    return new ResponseEntity<Object>(new Gson().toJson(response), httpHeaders, status);
  }

}
