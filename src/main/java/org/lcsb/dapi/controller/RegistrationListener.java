package org.lcsb.dapi.controller;

import java.util.UUID;

import org.lcsb.dapi.ConfigProperties;
import org.lcsb.dapi.model.User;
import org.lcsb.dapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

  private UserService userService;

  private JavaMailSender mailSender;

  
  private ConfigProperties configProperties;

  @Autowired
  public RegistrationListener(UserService userService, JavaMailSender mailSender, ConfigProperties configProperties) {
    this.userService = userService;
    this.mailSender = mailSender;
    this.configProperties=configProperties;
  }

  @Override
  public void onApplicationEvent(OnRegistrationCompleteEvent event) {
    this.confirmRegistration(event);
  }

  private void confirmRegistration(OnRegistrationCompleteEvent event) {
    User user = event.getUser();
    String token = UUID.randomUUID().toString();
    userService.createVerificationToken(user, token);

    String recipientAddress = user.getEmail();
    String subject = "DAPI Registration Confirmation";
    String confirmationUrl = configProperties.getBaseUrl() + "/registrationConfirm.html?token=" + token;

    SimpleMailMessage email = new SimpleMailMessage();
    email.setTo(recipientAddress);
    email.setSubject(subject);
    email.setText("To finish registration please click on the link: " + confirmationUrl);
    mailSender.send(email);
  }
}