package org.lcsb.dapi.controller;

import org.lcsb.dapi.model.User;
import org.springframework.context.ApplicationEvent;

public class OnRegistrationCompleteEvent extends ApplicationEvent {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private User user;

  public OnRegistrationCompleteEvent(User user) {
    super(user);
    this.user = user;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
