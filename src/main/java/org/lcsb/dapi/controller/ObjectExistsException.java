package org.lcsb.dapi.controller;

public class ObjectExistsException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ObjectExistsException(String message) {
    super(message);
  }

}
