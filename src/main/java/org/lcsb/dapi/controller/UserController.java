package org.lcsb.dapi.controller;

import java.util.Calendar;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.controller.dto.UserDto;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

@RestController
public class UserController extends BaseController {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private UserService userService;

  private PasswordEncoder passwordEncoder;

  private ApplicationEventPublisher eventPublisher;

  @Autowired
  public UserController(ApplicationEventPublisher eventPublisher, PasswordEncoder passwordEncoder,
      UserService userService) {
    this.eventPublisher = eventPublisher;
    this.passwordEncoder = passwordEncoder;
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @GetMapping(value = "/api/users/")
  public Page<User> getUsers(@NotNull final Pageable pageable) {
    return userService.getAll(pageable);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PostMapping("/api/users/{login:.+}:grantPrivilege")
  public User grantPrivilege(@RequestBody Privilege privilege, @PathVariable("login") String login)
      throws ObjectNotFound, QueryException {
    User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFound("User doesn't exist");
    }
    try {
      userService.grantUserPrivilege(user, privilege.getType(), privilege.getObjectId());
    } catch (InvalidArgumentException e) {
      throw new QueryException(e);
    }
    return userService.getUserByLogin(login);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PostMapping("/api/users/{login:.+}:revokePrivilege")
  public User revokePrivilege(@RequestBody Privilege privilege, @PathVariable("login") String login)
      throws ObjectNotFound, QueryException {
    User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFound("User doesn't exist");
    }
    userService.revokeUserPrivilege(user, privilege.getType(), privilege.getObjectId());
    return userService.getUserByLogin(login);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN') or authentication.getName() == #login")
  @GetMapping("/api/users/{login:.+}")
  public User getUser(@PathVariable("login") String login, WebRequest request) throws ObjectNotFound {
    return userService.getUserByLogin(login);
  }

  @PostMapping("/api/users/{login:.+}")
  public User registerUser(@RequestBody UserDto userDto, @PathVariable("login") String login)
      throws ObjectNotFound, QueryException, ObjectExistsException {
    User user = createUser(userDto, login);
    eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user));
    return user;
  }

  @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
  public User confirmRegistration(@RequestParam("token") String token) throws QueryException {

    VerificationToken verificationToken = userService.getVerificationToken(token);
    if (verificationToken == null) {
      throw new QueryException("invalid token");
    }

    User user = verificationToken.getUser();
    Calendar cal = Calendar.getInstance();
    if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
      throw new QueryException("token expired");
    }

    user.setEnabled(true);
    user.setToken(null);
    userService.update(user);
    return user;
  }

  @RequestMapping(value = "/registrationConfirm.html", method = RequestMethod.GET)
  public String confirmRegistrationToHtml(@RequestParam("token") String token) throws QueryException {
    confirmRegistration(token);
    return "<html><body>Registration is complete</body></html>";
  }

  private User createUser(UserDto userDto, String login) throws QueryException, ObjectExistsException {
    User existingUser = userService.getUserByLogin(userDto.getLogin());
    if (existingUser != null) {
      throw new ObjectExistsException("User with given login already exist");
    }
    existingUser = userService.getUserByEmail(userDto.getEmail());
    if (existingUser != null) {
      throw new ObjectExistsException("User with given login already exist");
    }
    if (!Objects.equals(userDto.getLogin(), login)) {
      throw new QueryException("Login doesn't match");
    }
    User user = new User();
    user.setLogin(userDto.getLogin());
    user.setCryptedPassword(passwordEncoder.encode(userDto.getPassword()));
    user.setEmail(userDto.getEmail());
    userService.add(user);
    return user;
  }

  @PreAuthorize("isAuthenticated()")
  @RequestMapping(value = "/isAuthenticated", method = RequestMethod.GET)
  public void confirmRegistration() throws QueryException {
  }

}
