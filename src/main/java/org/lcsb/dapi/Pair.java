package org.lcsb.dapi;

import java.io.Serializable;

/**
 * Represents pair of two objects. It's immutable as long as param classes are
 * immutable.
 * 
 * @author Piotr Gawron
 * 
 * @param <L>
 *          class type of the first object
 * @param <R>
 *          class type of the second object
 */
public class Pair<L, R> implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * First object.
   */
  private final L left;

  /**
   * Second object.
   */
  private final R right;

  /**
   * Default constructor.
   * 
   * @param left
   *          {@link #left}
   * @param right
   *          {@link #right}
   */
  public Pair(L left, R right) {
    this.left = left;
    this.right = right;
  }

  /**
   * 
   * @return {@link #left}
   */
  public L getLeft() {
    return left;
  }

  /**
   * 
   * @return {@link #right}
   */
  public R getRight() {
    return right;
  }

  @Override
  public int hashCode() {
    return left.hashCode() ^ right.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    if (!(o instanceof Pair)) {
      return false;
    }
    Pair<?, ?> pairo = (Pair<?, ?>) o;
    return this.left.equals(pairo.getLeft()) && this.right.equals(pairo.getRight());
  }

  @Override
  public String toString() {
    return "Pair: " + getLeft().toString() + ", " + getRight().toString();
  }

}