package org.lcsb.dapi;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cache.ehcache.internal.SingletonEhcacheRegionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;



@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "org.lcsb.dapi" })
public class HibernateConfig {

  @Bean(destroyMethod = "close")
  public HikariDataSource dataSource(ConfigProperties properties) {
    HikariDataSource dataSource = new HikariDataSource();
    dataSource.setDriverClassName(properties.getDatabaseDriver());
    dataSource.setJdbcUrl(properties.getDatabaseUrl());
    dataSource.setUsername(properties.getDatabaseUsername());
    dataSource.setPassword(properties.getDatabasePassword());
    dataSource.setIdleTimeout(30000);
    dataSource.setMaximumPoolSize(40);
    dataSource.setMinimumIdle(10);
    dataSource.setConnectionTestQuery(properties.getDatabaseConnectionQuery());
    return dataSource;
  }

  @Bean
  public LocalSessionFactoryBean getSessionFactory(DataSource dataSource, ConfigProperties properties) {
    Properties props = new Properties();
    props.setProperty(Environment.DIALECT, properties.getDatabaseDialect());
    props.setProperty(Environment.SHOW_SQL, properties.getDatabaseShowSQL());
    props.setProperty(Environment.CACHE_REGION_FACTORY, SingletonEhcacheRegionFactory.class.getCanonicalName());
    props.setProperty(Environment.USE_SECOND_LEVEL_CACHE, properties.getSecondLevelCache());
    props.setProperty(Environment.USE_QUERY_CACHE, properties.getSecondLevelCache());

    LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
    factoryBean.setDataSource(dataSource);
    factoryBean.setHibernateProperties(props);

    factoryBean.setPackagesToScan("org.lcsb.dapi.model");
    factoryBean.setImplicitNamingStrategy(new CustomImplicitNamingStrategy());
    factoryBean.setPhysicalNamingStrategy(new SpringPhysicalNamingStrategy());
    return factoryBean;
  }

  @Bean
  public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
    HibernateTransactionManager transactionManager = new HibernateTransactionManager();
    transactionManager.setSessionFactory(sessionFactory);
    return transactionManager;
  }

  @Bean
  public DataSourceInitializer dataSourceInitializer(DataSource dataSource, DatabasePopulator databasePopulator) {
    DataSourceInitializer initializer = new DataSourceInitializer();
    initializer.setDataSource(dataSource);
    initializer.setDatabasePopulator(databasePopulator);
    initializer.setEnabled(true);
    return initializer;
  }

}