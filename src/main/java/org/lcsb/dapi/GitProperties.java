package org.lcsb.dapi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:git.properties")
public class GitProperties {

  @Value("${commit.id}")
  private String commitId;

  @Value("${build.time}")
  private String buildTime;

  @Value("${build.version}")
  private String buildVersion;

  @Value("${dirty}")
  private String dirty;

  public GitProperties() {
    
  }
  
  public GitProperties(GitProperties original) {
    setCommitId(original.getCommitId());
    setBuildTime(original.getBuildTime());
    setBuildVersion(original.getBuildVersion());
    setDirty(original.getDirty());
  }
  
  public String getCommitId() {
    return commitId;
  }

  public void setCommitId(String commitId) {
    this.commitId = commitId;
  }

  public String getBuildTime() {
    return buildTime;
  }

  public void setBuildTime(String buildTime) {
    this.buildTime = buildTime;
  }

  public String getBuildVersion() {
    return buildVersion;
  }

  public void setBuildVersion(String buildVersion) {
    this.buildVersion = buildVersion;
  }

  public String getDirty() {
    return dirty;
  }

  public void setDirty(String dirty) {
    this.dirty = dirty;
  }

}