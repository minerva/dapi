package org.lcsb.dapi.service;

import org.lcsb.dapi.controller.ObjectExistsException;
import org.lcsb.dapi.model.License;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LicenseService {
  License getById(int id);

  Page<License> getAll(Pageable pageable);

  void add(License license) throws ObjectExistsException;

  void delete(License license) throws ObjectInConflictException;
}
