package org.lcsb.dapi.service;

import java.io.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.controller.ObjectNotFound;
import org.lcsb.dapi.dao.ReleaseDao;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.parser.ParserException;
import org.lcsb.dapi.service.parser.ctdbase.CtdParser;
import org.lcsb.dapi.service.parser.drugbank.DrugBankParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ReleaseServiceImp implements ReleaseService {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private ReleaseDao releaseDao;

  private FileService fileService;

  private CtdParser ctdParser;

  private DrugBankParser drugBankParser;

  @Autowired
  public ReleaseServiceImp(DrugBankParser drugBankParser, CtdParser ctdParser, FileService fileService,
      ReleaseDao releaseDao) {
    this.drugBankParser = drugBankParser;
    this.ctdParser = ctdParser;
    this.fileService = fileService;
    this.releaseDao = releaseDao;
  }

  @Override
  public Release createReleaseFromFile(UploadedFile file, ExternalDatabase database, String name, License license)
      throws IOException, ParserException {
    Release release = null;
    InputStream is = new FileInputStream(fileService.getFilePath(file));
    try {
      if (database.getName().equalsIgnoreCase("ctd")) {
        release = ctdParser.extractRelease(is);
      } else if (database.getName().equalsIgnoreCase("drugbank")) {
        release = drugBankParser.extractRelease(is);
      } else {
        throw new InvalidArgumentException("Unknown database type: " + database.getName());
      }
      release.setSourceFile(file);
      release.setExternalDatabase(database);
      release.setLicense(license);

      if (name != null) {
        release.setName(name);
      }
      releaseDao.add(release);
      return release;
    } finally {
      is.close();
    }
  }

  @Override
  public Page<Release> getByExternalDatabase(ExternalDatabase database, Pageable pageable) {
    return releaseDao.getByExternalDatabase(database, pageable);
  }

  @Override
  public Release getByExternalDatabase(String releaseName, ExternalDatabase database) {
    return releaseDao.getByExternalDatabase(releaseName, database);
  }

  @Override
  public void delete(String releaseName, ExternalDatabase database) throws ObjectNotFound {
    Release release = releaseDao.getByExternalDatabase(releaseName, database);
    if (release == null) {
      throw new ObjectNotFound("Release doesn't exist");
    }
    releaseDao.delete(release);

  }
}