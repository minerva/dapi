package org.lcsb.dapi.service;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.Pair;
import org.lcsb.dapi.dao.DrugDao;
import org.lcsb.dapi.dao.DrugProperty;
import org.lcsb.dapi.model.ChemicalEntity;
import org.lcsb.dapi.model.Release;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@Service
public class DrugServiceImp implements DrugService {

  private static Logger logger = LogManager.getLogger();

  private DrugDao drugDao;

  @Autowired
  public DrugServiceImp(DrugDao drugDao) {
    this.drugDao = drugDao;
  }

  @Override
  @Transactional
  public ChemicalEntity findById(String id, Release release,
      List<Pair<String, SimpleBeanPropertyFilter>> resultColumns) {
    ChemicalEntity result = drugDao.getDrugById(id, release);
    fetchLazyInitializedData(resultColumns, result);
    return result;
  }

  @Override
  @Transactional
  public Page<ChemicalEntity> findByRelease(Release release, Pageable pageable, Map<DrugProperty, Object> filterOptions,
      List<Pair<String, SimpleBeanPropertyFilter>> resultColumns) {
    filterOptions.put(DrugProperty.RELEASE, release);
    Page<ChemicalEntity> result = drugDao.getByFilter(pageable, filterOptions);

    fetchLazyInitializedData(resultColumns, result);
    return result;
  }

  private void fetchLazyInitializedData(List<Pair<String, SimpleBeanPropertyFilter>> resultColumns, Object data) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      SimpleFilterProvider filterProvider = new SimpleFilterProvider();
      for (Pair<String, SimpleBeanPropertyFilter> filter : resultColumns) {
        filterProvider.addFilter(filter.getLeft(), filter.getRight());
      }
      objectMapper.writer(filterProvider).writeValueAsString(data);
    } catch (JsonProcessingException e) {
      logger.error("Problem with serialization", e);
    }
  }

}