package org.lcsb.dapi.service;

import java.security.GeneralSecurityException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.ConfigProperties;
import org.lcsb.dapi.controller.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unboundid.ldap.sdk.*;
import com.unboundid.util.ssl.SSLUtil;
import com.unboundid.util.ssl.TrustAllTrustManager;

@Service
public class LdapServiceImpl implements LdapService {

  private static Logger logger = LogManager.getLogger();

  private ConfigProperties configProperties;

  @Autowired
  public LdapServiceImpl(ConfigProperties configProperties) {
    this.configProperties = configProperties;
  }

  @Override
  public LDAPConnection getConnection() throws LDAPException, GeneralSecurityException {
    String address = configProperties.getLdapAddress();
    if (address == null || address.trim().isEmpty()) {
      return null;
    }
    boolean ssl = configProperties.getLdapIsSSL();

    Integer port = configProperties.getLdapPort();
    LDAPConnection connection;
    if (ssl) {
      SSLUtil sslUtil = new SSLUtil(new TrustAllTrustManager());
      connection = new LDAPConnection(sslUtil.createSSLSocketFactory());
    } else {
      connection = new LDAPConnection();
    }
    connection.connect(address, port);

    String bindDn = configProperties.getLdapBindDn();
    String password = configProperties.getLdapPassword();

    if (bindDn == null || bindDn.trim().isEmpty()) {
      connection.bind(new SimpleBindRequest());
    } else {
      connection.bind(bindDn, password);
    }

    return connection;
  }

  @Override
  public boolean login(String login, String password) throws LDAPException, GeneralSecurityException {
    if (!isValidConfiguration()) {
      logger.warn("Invalid LDAP configuration");
      return false;
    }
    LDAPConnection connection = getConnection();
    try {
      UserDto user = getUserByLogin(login);
      if (user != null) {
        try {
          BindResult result = connection.bind(user.getLdapBindDn(), password);
          logger.debug("LDAP login status for user " + login + ": " + result.toString());
          return result.getResultCode().equals(ResultCode.SUCCESS);
        } catch (Exception e) {
          logger.debug("LDAP login for user " + login + " failed", e);
          return false;
        }
      }

      return false;
    } finally {
      connection.close();
    }
  }

  @Override
  public UserDto getUserByLogin(String login) throws LDAPException, GeneralSecurityException {
    if (!isValidConfiguration()) {
      logger.warn("Invalid LDAP configuration");
      return null;
    }
    LDAPConnection connection = getConnection();
    try {
      String baseDn = configProperties.getLdapBaseDn();
      String uidAttribute = configProperties.getLdapUidAttribute();

      String emailAttribute = configProperties.getLdapEmailAttribute();

      Filter loginFilter = createLoginFilter(login);
      Filter f2 = createObjectClassFilter();

      Filter filter = Filter.createANDFilter(loginFilter, f2);
      SearchResult searchResult = connection.search(baseDn, SearchScope.SUB, filter);

      for (SearchResultEntry entry : searchResult.getSearchEntries()) {
        UserDto result = new UserDto();
        result.setLdapBindDn(entry.getDN());

        Attribute uid = entry.getAttribute(uidAttribute);
        if (uid != null) {
          result.setLogin(uid.getValue());
        } else {
          logger.warn("Invalid ldap entry: " + entry);
        }

        if (!emailAttribute.trim().isEmpty()) {
          Attribute emailName = entry.getAttribute(emailAttribute);
          if (emailName != null) {
            result.setEmail(emailName.getValue());
          }
        }

        return result;
      }
      searchResult = connection.search(baseDn, SearchScope.SUB, loginFilter);
      if (searchResult.getSearchEntries().size() > 0) {
        logger.debug("User '" + login + "' exists, but was filtered out [filter=" + filter.toString() + "]");
      }
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public boolean isValidConfiguration() {
    try {
      String baseDn = configProperties.getLdapBaseDn();
      if (baseDn == null || baseDn.trim().isEmpty()) {
        return false;
      }
      LDAPConnection connection = getConnection();
      if (connection != null) {
        connection.close();
        return true;
      }
      return false;
    } catch (Exception e) {
      logger.error(e, e);
      return false;
    }
  }

  private Filter createObjectClassFilter() throws LDAPException {
    String objectClass = configProperties.getLdapObjectClassFilter();

    if (objectClass == null || objectClass.trim().isEmpty() || objectClass.equals("*")) {
      return Filter.create("objectClass=*");
    }

    return Filter.createEqualityFilter("objectClass", objectClass);
  }

  private Filter createLoginFilter(String login) {
    String uidProperty = configProperties.getLdapUidAttribute();
    return Filter.createEqualityFilter(uidProperty, login);
  }

}
