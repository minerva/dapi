package org.lcsb.dapi.service;

import java.io.IOException;

import org.lcsb.dapi.controller.ObjectNotFound;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReleaseService {
  Release createReleaseFromFile(UploadedFile file, ExternalDatabase database, String name, License license)
      throws ParserException, IOException;

  Page<Release> getByExternalDatabase(ExternalDatabase database, Pageable pageable);

  Release getByExternalDatabase(String releaseName, ExternalDatabase database);

  void delete(String releaseName, ExternalDatabase database) throws ObjectNotFound;

}
