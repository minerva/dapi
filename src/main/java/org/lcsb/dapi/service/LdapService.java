package org.lcsb.dapi.service;

import java.security.GeneralSecurityException;

import org.lcsb.dapi.controller.dto.UserDto;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;

public interface LdapService {

  boolean login(String login, String password) throws LDAPException, GeneralSecurityException;

  UserDto getUserByLogin(String login) throws LDAPException, GeneralSecurityException;

  boolean isValidConfiguration();

  LDAPConnection getConnection() throws LDAPException, GeneralSecurityException;
}
