package org.lcsb.dapi.service;

import java.util.List;
import java.util.Map;

import org.lcsb.dapi.Pair;
import org.lcsb.dapi.dao.DrugProperty;
import org.lcsb.dapi.model.ChemicalEntity;
import org.lcsb.dapi.model.Release;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;

public interface DrugService {
  Page<ChemicalEntity> findByRelease(Release release, Pageable pageable, Map<DrugProperty, Object> filterOptions, List<Pair<String,SimpleBeanPropertyFilter>> resultColumns);

  ChemicalEntity findById(String id, Release release, List<Pair<String, SimpleBeanPropertyFilter>> resultColumns);

}
