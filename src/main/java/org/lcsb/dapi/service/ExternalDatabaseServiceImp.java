package org.lcsb.dapi.service;

import org.lcsb.dapi.dao.ExternalDatabaseDao;
import org.lcsb.dapi.model.ExternalDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ExternalDatabaseServiceImp implements ExternalDatabaseService {

  private ExternalDatabaseDao externalDatabaseDao;

  @Autowired
  public ExternalDatabaseServiceImp(ExternalDatabaseDao externalDatabaseDao) {
    this.externalDatabaseDao = externalDatabaseDao;
  }

  @Override
  public Page<ExternalDatabase> getAll(Pageable pageable) {
    return externalDatabaseDao.getAll(pageable);
  }

  @Override
  public ExternalDatabase getByName(String name) {
    return externalDatabaseDao.getByName(name);
  }
}