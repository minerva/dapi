package org.lcsb.dapi.service;

import org.lcsb.dapi.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

  void add(User object);

  User getUserByLogin(String name);

  Page<User> getAll(Pageable pageable);

  void acceptLicense(User user, Release release);

  void grantUserPrivilege(User user, PrivilegeType type, Integer objectId);

  void revokeUserPrivilege(User user, PrivilegeType type, Integer objectId);

  void grantUserPrivilege(User user, PrivilegeType type);

  void revokeUserPrivilege(User user, PrivilegeType type);

  User getUserByEmail(String email);

  void createVerificationToken(User user, String token);

  VerificationToken getVerificationToken(String token);

  void update(User user);
}
