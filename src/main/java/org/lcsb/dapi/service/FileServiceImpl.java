package org.lcsb.dapi.service;

import java.io.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.ConfigProperties;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.dao.FileDao;
import org.lcsb.dapi.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl implements FileService {

  private static Logger logger = LogManager.getLogger();

  private FileDao fileDao;

  private ConfigProperties config;

  @Autowired
  public FileServiceImpl(ConfigProperties config, FileDao fileDao) {
    this.fileDao = fileDao;
    this.config = config;
  }

  @Override
  public UploadedFile add(UploadedFile file) throws IOException {
    fileDao.add(file);

    new File(getFilePath(file)).createNewFile();
    return file;
  }

  @Override
  public String getFilePath(UploadedFile file) {
    return config.getFileStorage() + "/" + file.getId();
  }

  @Override
  public UploadedFile addContent(Integer fileId, byte[] content) throws InvalidArgumentException, IOException {
    UploadedFile file = fileDao.getById(fileId);
    File f = new File(getFilePath(file));
    if (f.length() + content.length > file.getLength()) {
      throw new InvalidArgumentException("Content too big");
    }

    FileOutputStream writer = new FileOutputStream(f, true); // Set true for append mode
    writer.write(content);
    writer.close();

    return file;
  }

  @Override
  public void delete(UploadedFile file) {
    String path = getFilePath(file);
    logger.debug("Removing file: " + path);
    new File(path).delete();
    fileDao.delete(file);
  }

  @Override
  public UploadedFile getById(Integer id) {
    return fileDao.getById(id);
  }

  @Override
  public Page<UploadedFile> getAll(Pageable pageable) {
    return fileDao.getAll(pageable);
  }

}
