package org.lcsb.dapi.service;

public class ObjectInConflictException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ObjectInConflictException(String string) {
    super(string);
  }

  public ObjectInConflictException() {
  }
}
