package org.lcsb.dapi.service.parser.ctdbase;

public enum CtdFileFormat {
  CSV,
  XML
}
