package org.lcsb.dapi.service.parser.ctdbase.csv;

import java.io.*;
import java.util.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.Pair;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.stereotype.Service;

@Service
class ChemicalDiseaseTargetParser {
  private static Logger logger = LogManager.getLogger();

  private IdentifierFactory identifierFactory;

  private static String[] HEADERS = { "ChemicalName", "ChemicalID", "CasRN", "DiseaseName", "DiseaseID",
      "DirectEvidence", "InferenceGeneSymbol", "InferenceScore", "OmimIDs", "PubMedIDs" };

  public ChemicalDiseaseTargetParser(IdentifierFactory identifierFactory) {
    this.identifierFactory = identifierFactory;
  }

  public List<Target> extractChemicalTargets(InputStream is, Collection<Chemical> chemicals) throws ParserException {
    try {
      Map<Identifier, Chemical> chemicalById = createChemicalByIdMap(chemicals);
      List<Target> result = new ArrayList<>();
      Reader in = new InputStreamReader(is);
      Iterable<CSVRecord> records;
      records = CSVFormat.DEFAULT
          .withHeader(HEADERS)
          .withCommentMarker('#')
          .parse(in);
      for (CSVRecord record : records) {
        Pair<Identifier, Target> targetData = extractTarget(record);
        Target target = targetData.getRight();
        Identifier identifier = targetData.getLeft();
        if (target != null) {
          Chemical chemical = chemicalById.get(identifier);
          if (chemical == null) {
            throw new ParserException("[" + identifier.toString() + "] Chemical with id doesn't exist");
          }
          chemical.addTarget(target);
          result.add(target);
        }
      }
      return result;
    } catch (IOException e) {
      throw new ParserException("Problem with input", e);
    }
  }

  Map<Identifier, Chemical> createChemicalByIdMap(Collection<Chemical> chemicals) {
    Map<Identifier, Chemical> result = new HashMap<>();
    for (Chemical chemical : chemicals) {
      if (result.get(chemical.getSourceIdentifier()) != null) {
        throw new InvalidArgumentException(chemical.getSourceIdentifier() + "Chemical identifier duplicated");
      }
      result.put(chemical.getSourceIdentifier(), chemical);
    }
    return result;
  }

  private Pair<Identifier, Target> extractTarget(CSVRecord record) {
    Identifier chemicalIdentifier = identifierFactory.createIdentifier(IdentifierType.MESH, record.get("ChemicalID"));
    Target target;
    if (record.get("InferenceGeneSymbol").isEmpty()) {
      logger.debug("InferenceGeneSymbol is empty");
      target = null;
    } else {
      target = new Target();
      target.setName(record.get("InferenceGeneSymbol"));
      target.setAssociatedDisease(identifierFactory.createIdentifier(record.get("DiseaseID")));
      target.addIdentifier(identifierFactory.createIdentifier(IdentifierType.HGNC, record.get("InferenceGeneSymbol")));

      String[] pubmeds = record.get("PubMedIDs").split("\\|", -1);
      for (String pubmed : pubmeds) {
        if (!pubmed.isEmpty()) {
          target.addReference(identifierFactory.createIdentifier(IdentifierType.PUBMED, pubmed));
        }
      }
    }

    return new Pair<>(chemicalIdentifier, target);
  }

  public List<Target> extractChemicalTargets(String filename, Collection<Chemical> chemicals)
      throws FileNotFoundException, ParserException {
    return extractChemicalTargets(new FileInputStream(filename), chemicals);
  }

}
