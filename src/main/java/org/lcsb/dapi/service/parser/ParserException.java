package org.lcsb.dapi.service.parser;

public class ParserException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ParserException(Throwable e) {
    super(e);
  }

  public ParserException(String message, Throwable e) {
    super(message, e);
  }

  public ParserException(String message) {
    super(message);
  }
}
