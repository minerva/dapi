package org.lcsb.dapi.service.parser.ctdbase.csv;

import java.io.*;
import java.util.List;
import java.util.zip.*;

import org.lcsb.dapi.model.Chemical;
import org.lcsb.dapi.model.Release;
import org.lcsb.dapi.service.parser.DapiParser;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CtdCsvParser extends DapiParser {

  private ChemicalDiseaseTargetParser chemicalDiseaseTargetParser;

  private ChemicalTargetParser chemicalTargetParser;

  private ChemicalParser chemicalParser;

  @Autowired
  public CtdCsvParser(ChemicalParser chemicalParser, ChemicalTargetParser chemicalTargetParser,
      ChemicalDiseaseTargetParser chemicalDiseaseTargetParser) {
    this.chemicalDiseaseTargetParser = chemicalDiseaseTargetParser;
    this.chemicalParser = chemicalParser;
    this.chemicalTargetParser = chemicalTargetParser;
  }

  @Override
  public Release extractRelease(InputStream is) throws IOException, ParserException {
    Release result = new Release();
    ZipInputStream zis = new ZipInputStream(is);

    InputStream chemicalsEntry = null;
    InputStream chemicalTargetsEntry = null;
    InputStream chemicalDiseaseTargetsEntry = null;
    try {
      ZipEntry zipEntry = zis.getNextEntry();
      while (zipEntry != null) {

        if (zipEntry.getName().startsWith("CTD_chemicals.")) {
          if (zipEntry.getName().endsWith(".gz")) {
            chemicalsEntry = new GZIPInputStream(copyStream(zis));
          } else {
            chemicalsEntry = copyStream(zis);
          }
        } else if (zipEntry.getName().startsWith("CTD_chem_gene_ixns.")) {
          if (zipEntry.getName().endsWith(".gz")) {
            chemicalTargetsEntry = new GZIPInputStream(copyStream(zis));
          } else {
            chemicalTargetsEntry = copyStream(zis);
          }
        } else if (zipEntry.getName().startsWith("CTD_chemicals_diseases.")) {
          if (zipEntry.getName().endsWith(".gz")) {
            chemicalDiseaseTargetsEntry = new GZIPInputStream(copyStream(zis));
          } else {
            chemicalDiseaseTargetsEntry = copyStream(zis);
          }
        } else {
          throw new ParserException("Unknown file: " + zipEntry.getName());
        }
        zipEntry = zis.getNextEntry();
      }
      List<Chemical> chemicals = chemicalParser.extractChemicals(chemicalsEntry);
      result.addChemicalEntities(chemicals);
      if (chemicalTargetsEntry != null) {
        chemicalTargetParser.extractChemicalTargets(chemicalTargetsEntry, chemicals);
      }
      if (chemicalDiseaseTargetsEntry != null) {
        chemicalDiseaseTargetParser.extractChemicalTargets(chemicalDiseaseTargetsEntry, chemicals);
      }
      return result;
    } finally {
      zis.closeEntry();
      zis.close();
    }
  }

}
