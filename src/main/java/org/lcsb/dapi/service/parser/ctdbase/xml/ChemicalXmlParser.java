package org.lcsb.dapi.service.parser.ctdbase.xml;

import java.io.*;
import java.util.*;

import javax.xml.parsers.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.NotImplementedException;
import org.lcsb.dapi.model.Chemical;
import org.lcsb.dapi.service.IdentifierFactory;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

@Service
class ChemicalXmlParser {
  private static Logger logger = LogManager.getLogger();

  private DocumentBuilder db;

  private IdentifierFactory identifierFactory;

  @Autowired
  public ChemicalXmlParser(IdentifierFactory identifierFactory) {
    this.identifierFactory = identifierFactory;
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    try {
      db = dbf.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      logger.error("Problem with initializing xml parser", e);
    }
  }

  public List<Chemical> extractChemicals(InputStream is) throws ParserException {
    try {
      List<Chemical> result = new ArrayList<>();
      Document doc = db.parse(is);
      Element element = doc.getDocumentElement();
      NodeList children = element.getChildNodes();
      for (int i = 0; i < children.getLength(); i++) {
        Node child = children.item(i);
        if (child instanceof Element) {
          result.add(extractChemical((Element) child));
        }
      }
      return result;
    } catch (SAXException | IOException e) {
      throw new ParserException(e);
    }
  }

  private Chemical extractChemical(Element element) {
    Chemical chemical = new Chemical();
    NodeList children = element.getChildNodes();
    for (int i = 0; i < children.getLength(); i++) {
      Node child = children.item(i);
      if (child instanceof Element) {
        switch (child.getNodeName()) {
        case ("ChemicalName"):
          chemical.setName(child.getTextContent());
          break;
        case ("ChemicalID"):
          chemical.setSourceIdentifier(identifierFactory.createIdentifier(child.getTextContent()));
          break;
        case ("Synonyms"):
          chemical.addSynonyms(Arrays.asList(child.getTextContent().split("\\|")));
          break;
        case ("ParentIDs"):
        case ("TreeNumbers"):
        case ("ParentTreeNumbers"):
          // ignore
          break;
        default:
          throw new NotImplementedException("Unknown chemical node: " + child.getNodeName());
        }
      }
    }
    return chemical;
  }

  public List<Chemical> extractChemicals(String filename) throws FileNotFoundException, ParserException {
    return extractChemicals(new FileInputStream(filename));
  }
}
