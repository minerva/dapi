package org.lcsb.dapi.service.parser.ctdbase;

import java.io.*;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.lcsb.dapi.model.Release;
import org.lcsb.dapi.service.parser.DapiParser;
import org.lcsb.dapi.service.parser.ParserException;
import org.lcsb.dapi.service.parser.ctdbase.csv.CtdCsvParser;
import org.lcsb.dapi.service.parser.ctdbase.xml.CtdXmlParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CtdParser extends DapiParser {

  private CtdCsvParser ctdCsvParser;

  private CtdXmlParser ctdXmlParser;

  @Autowired
  public CtdParser(CtdCsvParser ctdCsvParser, CtdXmlParser ctdXmlParser) {
    this.ctdCsvParser = ctdCsvParser;
    this.ctdXmlParser = ctdXmlParser;
  }

  @Override
  public Release extractRelease(InputStream is) throws IOException, ParserException {
    ByteArrayOutputStream baos = toBaos(is);
    InputStream is1 = new ByteArrayInputStream(baos.toByteArray());
    ZipInputStream zis = new ZipInputStream(is1);
    CtdFileFormat format = null;
    try {
      ZipEntry zipEntry = zis.getNextEntry();
      if (zipEntry == null) {
        throw new ParserException("Invalid input zip file");
      }
      if (zipEntry.getName().endsWith("xml") || zipEntry.getName().endsWith("xml.gz")) {
        format = CtdFileFormat.XML;
      } else if (zipEntry.getName().endsWith("csv") || zipEntry.getName().endsWith("csv.gz")) {
        format = CtdFileFormat.CSV;
      } else {
        throw new ParserException("Unknown file format for: " + zipEntry.getName());
      }
    } finally {
      zis.closeEntry();
      zis.close();
    }
    Release result;
    InputStream is2 = new ByteArrayInputStream(baos.toByteArray());
    switch (format) {
    case CSV:
      result = ctdCsvParser.extractRelease(is2);
      break;
    case XML:
      result = ctdXmlParser.extractRelease(is2);
      break;
    default:
      throw new ParserException("Unknown format: " + format);
    }
    if (result.getTimestamp() == null) {
      result.setTimestamp(Calendar.getInstance().getTime());
    }
    return result;
  }

}
