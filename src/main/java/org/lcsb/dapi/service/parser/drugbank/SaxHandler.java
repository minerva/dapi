package org.lcsb.dapi.service.parser.drugbank;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.StringEscapeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class SaxHandler extends DefaultHandler {
  private List<String> drugData = new ArrayList<>();
  private String releaseTimestamp;
  private String releaseName;

  private boolean validData;

  public String getReleaseTimestamp() {
    return releaseTimestamp;
  }

  public String getReleaseName() {
    return releaseName;
  }

  StringBuilder data = new StringBuilder();
  int depthCounter = 0;

  public List<String> getDrugData() {
    return drugData;
  }

  public void setDrugData(List<String> drugData) {
    this.drugData = drugData;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    if ("drugbank".equalsIgnoreCase(qName)) {
      int index1 = attributes.getIndex("version");
      releaseName = attributes.getValue(index1);

      int index2 = attributes.getIndex("exported-on");
      releaseTimestamp = attributes.getValue(index2);
    }
    if ("drug".equalsIgnoreCase(qName)) {
      if (attributes.getLength() > 0) {
        data = new StringBuilder();
        depthCounter++;
      } else {
        depthCounter++;
      }
    }
    data.append("<" + qName);

    if (attributes.getLength() > 0) {
      for (int i = 0; i < attributes.getLength(); i++) {
        data.append(" ");
        data.append(attributes.getQName(i));
        data.append("=");
        data.append("\"" + attributes.getValue(i) + "\"");
      }
    }

    data.append(">");

  }

  @Override
  public void characters(char ch[], int start, int length) throws SAXException {
    data.append(StringEscapeUtils.escapeXml10(new String(ch, start, length)));
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    data.append("</" + qName + ">");
    if ("drug".equalsIgnoreCase(qName)) {
      depthCounter--;
      if (depthCounter == 0) {
        drugData.add(data.toString());
      }
    }
  }

  @Override
  public void endDocument() throws SAXException {
    validData = true;
    if (releaseTimestamp == null) {
      validData = false;
    }
  }

  public boolean isValidData() {
    return validData;
  }

  public void setValidData(boolean validData) {
    this.validData = validData;
  }

}
