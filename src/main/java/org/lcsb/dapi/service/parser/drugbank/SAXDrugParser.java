package org.lcsb.dapi.service.parser.drugbank;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.*;

import org.lcsb.dapi.service.parser.ParserException;
import org.xml.sax.SAXException;

class SAXDrugParser {
  private SaxHandler handler;

  public List<String> parsedDrugs(InputStream stream) throws ParserException {
    try {
      SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
      handler = new SaxHandler();
      SAXParser saxParser = saxParserFactory.newSAXParser();
      saxParser.parse(stream, handler);
      if (!handler.isValidData()) {
        throw new ParserException("Invalid input file");
      }
      return handler.getDrugData();
    } catch (ParserConfigurationException | SAXException | IOException e) {
      throw new ParserException(e);
    }
  }

  public String getReleaseName() {
    return handler.getReleaseName();
  }

  public String getReleaseTimestamp() {
    return handler.getReleaseTimestamp();
  }
}
