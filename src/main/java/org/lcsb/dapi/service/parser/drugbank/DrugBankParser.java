package org.lcsb.dapi.service.parser.drugbank;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.lcsb.dapi.service.parser.DapiParser;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.stereotype.Service;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

@Service
public class DrugBankParser extends DapiParser {
  private static Logger logger = LogManager.getLogger();

  private DocumentBuilder db;

  private IdentifierFactory identifierFactory;

  public DrugBankParser(IdentifierFactory identifierFactory) {
    this.identifierFactory = identifierFactory;
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    try {
      db = dbf.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      logger.error("Problem with initializing xml parser", e);
    }
  }

  @Override
  public Release extractRelease(InputStream is) throws ParserException {
    try {
      ByteArrayOutputStream baos = toBaos(is);

      is = getUnzippedContent(baos);
      if (is == null) {
        is = new ByteArrayInputStream(baos.toByteArray());
      }

      Release release;
      SAXDrugParser saxDrugParser = new SAXDrugParser();
      List<String> drugData = saxDrugParser.parsedDrugs(is);

      release = new Release();
      release.setTimestamp(new SimpleDateFormat("yyyy-dd-MM").parse(saxDrugParser.getReleaseTimestamp()));
      release.setName(saxDrugParser.getReleaseName());

      for (int i = 0; i < drugData.size(); i++) {
        String drugTag = drugData.get(i);
        Drug drug = extractDrug(drugTag);
        release.addChemicalEntity(drug);
      }

      return release;
    } catch (IOException | ParseException | SAXException e) {
      logger.error(e, e);
      throw new ParserException(e);
    }
  }

  private InputStream getUnzippedContent(ByteArrayOutputStream baos) {
    try {
      ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(baos.toByteArray()));
      ZipEntry ze = zis.getNextEntry();
      if (ze == null) {
        return null;
      }
      return copyStream(zis);
    } catch (IOException e) {
      return null;
    }
  }

  private Drug extractDrug(String drugTag) throws SAXException, IOException {
    Drug drug = new Drug();
    InputStream stream = new ByteArrayInputStream(drugTag.getBytes(StandardCharsets.UTF_8));
    Document doc = db.parse(stream);
    Element e = doc.getDocumentElement();

    TargetFields fillTargets = new TargetFields(identifierFactory);

    drug.setName(extractDrugName(e));

    drug.setDescription(extractDrugDescription(e));

    drug.setApproved(extractDrugApproved(e));

    drug.addSynonyms(extractSynonyms(e));

    drug.addBrandNames(extractBrandNames(e));

    drug.addTargets(fillTargets.extractTargets(e));

    drug.setSourceIdentifier(extractSourceIdentifier(e));

    drug.setBloodBrainBarrier(extractBloodBrainBarrier(e));
    return drug;
  }

  // ************** Drug ******************/

  private String extractDrugName(Element e) {
    NodeList nodeList = e.getElementsByTagName("name");
    String drugName = "";
    if (nodeList != null && nodeList.getLength() > 0) {
      drugName = nodeList.item(0).getChildNodes().item(0).getNodeValue();
    }
    return drugName;
  }

  private String extractDrugDescription(Element e) {
    NodeList nodeList = e.getElementsByTagName("description");
    String description = "";
    if (nodeList != null) {
      if (nodeList.getLength() > 0 && !nodeList.item(0).getTextContent().isEmpty()) {

        description = nodeList.item(0).getChildNodes().item(0).getNodeValue();
      }
    }
    return description;
  }

  /**
   * 
   * @param e
   *          three cases arise here: 1->the only field is approved then return
   *          true 2->more than 1 field and one of them is approved then situation
   *          is ambiguous return null 3->more than 1 field and none of which is
   *          approved then return false
   */
  private Boolean extractDrugApproved(Element e) {
    NodeList nodeList = e.getElementsByTagName("groups");
    for (int j = 0; j < nodeList.getLength(); j++) {
      Element nextEle = (Element) nodeList.item(j);
      NodeList secondNodeList = nextEle.getElementsByTagName("group");
      boolean approvedFlag = false;

      if (secondNodeList.getLength() == 1) {
        if (secondNodeList.item(0).getTextContent().equalsIgnoreCase("approved")) {
          return true;
        } else {
          return false;
        }
      }

      else if (secondNodeList.getLength() > 1) {
        for (int k = 0; k < secondNodeList.getLength(); k++) {
          if (secondNodeList.item(k).getTextContent().equalsIgnoreCase("approved")) {
            approvedFlag = true;
            return null;
          }
        }
        if (!approvedFlag) {
          return false;
        }
      } else {
        return false;
      }
    }
    return false;
  }

  private List<String> extractSynonyms(Element e) {
    List<String> listOfSynonyms = new ArrayList<>();
    Element synonyms = null;

    NodeList nodeList = e.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node child = nodeList.item(i);
      if (Objects.equals(child.getNodeName(), "synonyms") && child instanceof Element) {
        synonyms = (Element) child;
      }
    }
    if (synonyms != null) {
      NodeList synonymList = synonyms.getElementsByTagName("synonym");
      for (int k = 0; k < synonymList.getLength(); k++) {
        if (synonymList != null && synonymList.getLength() > 0)
          listOfSynonyms.add(synonymList.item(k).getChildNodes().item(0).getNodeValue());
      }
    }
    return listOfSynonyms;
  }

  private List<String> extractBrandNames(Element e) {
    List<String> listOfBrandNames = new ArrayList<>();
    NodeList nodeList = e.getElementsByTagName("international-brand");
    for (int i = 0; i < nodeList.getLength(); i++) {
      Element productEle = (Element) nodeList.item(i);
      listOfBrandNames.add(productEle.getTextContent().trim());

    }
    return listOfBrandNames;
  }

  private Identifier extractSourceIdentifier(Element e) {
    NodeList resourceIdentifierList = e.getElementsByTagName("drugbank-id");
    if (resourceIdentifierList != null && resourceIdentifierList.getLength() > 0) {
      String id = resourceIdentifierList.item(0).getChildNodes().item(0).getNodeValue();
      return identifierFactory.createIdentifier(IdentifierType.DRUG_BANK, id);
    }
    return null;
  }

  private Boolean extractBloodBrainBarrier(Element e) {
    return null;
  }

}
