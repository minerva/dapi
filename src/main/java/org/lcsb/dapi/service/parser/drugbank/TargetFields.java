package org.lcsb.dapi.service.parser.drugbank;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

class TargetFields {
  private static Logger logger = LogManager.getLogger();

  private IdentifierFactory identifierFactory;

  public TargetFields(IdentifierFactory identifierFactory) {
    this.identifierFactory = identifierFactory;
  }

  public List<Target> extractTargets(Element e) {
    NodeList nodeList = e.getElementsByTagName("targets");
    List<Target> listOfTargets = new ArrayList<>();

    if (nodeList != null && nodeList.getLength() > 0) {
      for (int j = 0; j < nodeList.getLength(); j++) {
        Element targetEle = (Element) nodeList.item(j);
        NodeList targetList = targetEle.getElementsByTagName("target");

        for (int k = 0; k < targetList.getLength(); k++) {
          Element tarEle = (Element) targetList.item(k);
          Target target = new Target();

          target.setName(extractTargetName(tarEle));

          target.setSourceIdentifier(extractTargetSourceIdentifier(tarEle));

          target.setOrganism(extractTargetOrganism(tarEle));

          target.addIdentifiers(extractTargetIdentifiers(tarEle));

          target.addReferences(extractTargetReferences(tarEle));

          target.addTypes(extractTargetType(tarEle));

          listOfTargets.add(target);
        }
      }
    }
    return listOfTargets;
  }

  private List<Identifier> extractTargetReferences(Element tarEle) {
    List<Identifier> result = new ArrayList<>();
    NodeList referenceList = tarEle.getElementsByTagName("article");
    for (int i = 0; i < referenceList.getLength(); i++) {
      Element referenceElement = (Element) referenceList.item(i);
      NodeList pubmedList = referenceElement.getElementsByTagName("pubmed-id");
      for (int j = 0; j < pubmedList.getLength(); j++) {
        Element orgEle = (Element) pubmedList.item(j);
        String id = orgEle.getTextContent();
        if (id != null && !id.isEmpty()) {
          result.add(identifierFactory.createIdentifier(IdentifierType.PUBMED, id));
        }
      }
    }
    return result;
  }

  private String extractTargetName(Element tarEle) {
    NodeList nameList = tarEle.getElementsByTagName("name");
    String targetName = "";
    if (nameList != null && nameList.getLength() > 0) {
      targetName = nameList.item(0).getChildNodes().item(0).getNodeValue();
    }
    return targetName;
  }

  private Identifier extractTargetSourceIdentifier(Element tarEle) {
    NodeList sourceIdentifierList = tarEle.getElementsByTagName("id");
    if (sourceIdentifierList != null && sourceIdentifierList.getLength() > 0) {
      if (sourceIdentifierList != null && sourceIdentifierList.getLength() > 0) {
        String id = sourceIdentifierList.item(0).getChildNodes().item(0).getNodeValue();
        if (id != null && !id.isEmpty()) {
          return identifierFactory.createIdentifier(IdentifierType.DRUG_BANK_TARGET, id);
        }
      }
    }
    return null;
  }

  /**
   * @see Targets->target->polypeptide->organism->extract an attribute from
   *      organism
   */
  private Identifier extractTargetOrganism(Element tarEle) {
    Set<Identifier> sampleIdentifiers = new HashSet<>();
    NodeList polypeptideList = tarEle.getElementsByTagName("polypeptide");
    if (polypeptideList != null && polypeptideList.getLength() > 0) {
      for (int i = 0; i < polypeptideList.getLength(); i++) {
        Element polyEle = (Element) polypeptideList.item(i);
        NodeList orgList = polyEle.getElementsByTagName("organism");
        if (orgList != null && orgList.getLength() > 0) {
          for (int j = 0; j < orgList.getLength(); j++) {
            Element orgEle = (Element) orgList.item(j);
            String id = orgEle.getAttribute("ncbi-taxonomy-id");
            if (id != null && !id.isEmpty()) {
              sampleIdentifiers.add(identifierFactory.createIdentifier(IdentifierType.TAXONOMY, id));
            }
          }
        }
      }
    }
    if (sampleIdentifiers.size() > 1) {
      logger.warn("Multiple organisms for a single target");
    } else if (sampleIdentifiers.size() == 0) {
      return null;
    }

    return sampleIdentifiers.iterator().next();
  }

  private List<Identifier> extractTargetIdentifiers(Element tarEle) {
    List<Identifier> idList = new ArrayList<>();
    NodeList polypeptideList = tarEle.getElementsByTagName("polypeptide");

    if (polypeptideList != null && polypeptideList.getLength() > 0) {
      Element polyEle = (Element) polypeptideList.item(0);
      NodeList extIdentifierList = polyEle.getElementsByTagName("external-identifiers");
      for (int m = 0; m < extIdentifierList.getLength(); m++) {
        Element extIdentifierEle = (Element) extIdentifierList.item(m);
        NodeList extIdentifiers = extIdentifierEle.getElementsByTagName("external-identifier");

        for (int t = 0; t < extIdentifiers.getLength(); t++) {
          Element extIdEle = (Element) extIdentifiers.item(t);
          NodeList resourceList = extIdEle.getElementsByTagName("resource");
          NodeList identifierList = extIdEle.getElementsByTagName("identifier");
          if (resourceList != null && resourceList.getLength() > 0 && identifierList != null
              && identifierList.getLength() > 0) {
            if (resourceList.item(0).getChildNodes().item(0).getNodeValue().equalsIgnoreCase("UniProtKB")) {
              String id = identifierList.item(0).getChildNodes().item(0).getNodeValue();
              if (id != null && !id.isEmpty()) {
                idList.add(identifierFactory.createIdentifier(IdentifierType.UNIPROT, id));
              }
            }
          }
        }
      }
    }
    return idList;
  }

  private List<String> extractTargetType(Element tarEle) {
    List<String> result = new ArrayList<>();
    NodeList actionList = tarEle.getElementsByTagName("actions");
    if (actionList != null && actionList.getLength() > 0) {
      Element actionEle = (Element) actionList.item(0);
      NodeList aList = actionEle.getElementsByTagName("action");
      if (aList != null && aList.getLength() > 0) {
        result.add(aList.item(0).getChildNodes().item(0).getNodeValue());
      }
    }
    return result;
  }
}
