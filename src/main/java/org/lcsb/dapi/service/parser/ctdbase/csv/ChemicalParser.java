package org.lcsb.dapi.service.parser.ctdbase.csv;

import java.io.*;
import java.util.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.model.Chemical;
import org.lcsb.dapi.model.IdentifierType;
import org.lcsb.dapi.service.IdentifierFactory;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.stereotype.Service;

@Service
class ChemicalParser {
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private static String[] HEADERS = new String[] { "ChemicalName", "ChemicalID", "CasRN", "Definition", "ParentIDs",
      "TreeNumbers", "ParentTreeNumbers", "Synonyms", "DrugBankIDs" };

  private IdentifierFactory identifierFactory;

  public ChemicalParser(IdentifierFactory identifierFactory) {
    this.identifierFactory = identifierFactory;
  }

  public List<Chemical> extractChemicals(InputStream is) throws ParserException {
    try {
      List<Chemical> result = new ArrayList<>();
      Reader in = new InputStreamReader(is);
      Iterable<CSVRecord> records;
      records = CSVFormat.DEFAULT
          .withHeader(HEADERS)
          .withCommentMarker('#')
          .parse(in);
      for (CSVRecord record : records) {
        result.add(extractChemical(record));
      }
      return result;
    } catch (IOException e) {
      throw new ParserException(e);
    }
  }

  private Chemical extractChemical(CSVRecord record) {
    Chemical chemical = new Chemical();
    chemical.setName(record.get("ChemicalName"));
    chemical.setSourceIdentifier(identifierFactory.createIdentifier(record.get("ChemicalID")));
    for (String id : Arrays.asList(record.get("DrugBankIDs").split("\\|", -1))) {
      if (!id.isEmpty()) {
        chemical.addAnnotation(identifierFactory.createIdentifier(IdentifierType.DRUG_BANK, id));
      }
    }
    if (!record.get("CasRN").isEmpty()) {
      chemical.addAnnotation(identifierFactory.createIdentifier(IdentifierType.CAS, record.get("CasRN")));
    }
    chemical.addSynonyms(Arrays.asList(record.get("Synonyms").split("\\|")));
    return chemical;
  }

  public List<Chemical> extractChemicals(String filename) throws FileNotFoundException, ParserException {
    return extractChemicals(new FileInputStream(filename));
  }
}
