package org.lcsb.dapi.service.parser.ctdbase.xml;

import java.io.*;
import java.util.List;
import java.util.zip.*;

import org.lcsb.dapi.model.Chemical;
import org.lcsb.dapi.model.Release;
import org.lcsb.dapi.service.parser.DapiParser;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CtdXmlParser extends DapiParser {

  private ChemicalXmlParser chemicalParser;

  @Autowired
  public CtdXmlParser(ChemicalXmlParser chemicalParser) {
    this.chemicalParser = chemicalParser;
  }

  @Override
  public Release extractRelease(InputStream is) throws IOException, ParserException {
    Release result = new Release();
    ZipInputStream zis = new ZipInputStream(is);

    InputStream chemicalsEntry = null;
    try {
      ZipEntry zipEntry = zis.getNextEntry();
      while (zipEntry != null) {

        if (zipEntry.getName().startsWith("CTD_chemicals.")) {
          if (zipEntry.getName().endsWith(".gz")) {
            chemicalsEntry = new GZIPInputStream(copyStream(zis));
          } else {
            chemicalsEntry = copyStream(zis);
          }
        } else {
          throw new ParserException("Unknown file: " + zipEntry.getName());
        }
        zipEntry = zis.getNextEntry();
      }
      List<Chemical> chemicals = chemicalParser.extractChemicals(chemicalsEntry);
      result.addChemicalEntities(chemicals);
      return result;
    } finally {
      zis.closeEntry();
      zis.close();
    }
  }

}
