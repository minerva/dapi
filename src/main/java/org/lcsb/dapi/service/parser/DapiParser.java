package org.lcsb.dapi.service.parser;

import java.io.*;

import org.lcsb.dapi.model.Release;

public abstract class DapiParser {

  public abstract Release extractRelease(InputStream is) throws IOException, ParserException;

  public final Release extractRelease(String filename) throws IOException, ParserException {
    return extractRelease(new FileInputStream(filename));
  }

  protected InputStream copyStream(InputStream is) throws IOException {
    return new ByteArrayInputStream(toBaos(is).toByteArray());
  }

  protected ByteArrayOutputStream toBaos(InputStream is) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    byte[] buffer = new byte[1024];
    int len;
    while ((len = is.read(buffer)) > -1) {
      baos.write(buffer, 0, len);
    }
    baos.flush();
    return baos;
  }
}
