package org.lcsb.dapi.service.parser.ctdbase.csv;

import java.io.*;
import java.util.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.Pair;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.stereotype.Service;

@Service
class ChemicalTargetParser {
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private static String[] HEADERS = { "ChemicalName", "ChemicalID", "CasRN", "GeneSymbol", "GeneID", "GeneForms",
      "Organism", "OrganismID", "Interaction", "InteractionActions", "PubMedIDs" };


  private IdentifierFactory identifierFactory;

  public ChemicalTargetParser(IdentifierFactory identifierFactory) {
    this.identifierFactory = identifierFactory;
  }

  public List<Target> extractChemicalTargets(InputStream is, Collection<Chemical> chemicals) throws ParserException {
    try {
      Map<Identifier, Chemical> chemicalById = createChemicalByIdMap(chemicals);
      List<Target> result = new ArrayList<>();
      Reader in = new InputStreamReader(is);
      Iterable<CSVRecord> records;
      records = CSVFormat.DEFAULT
          .withHeader(HEADERS)
          .withCommentMarker('#')
          .parse(in);
      for (CSVRecord record : records) {
        Pair<Identifier, Target> target = extractTarget(record);
        Chemical chemical = chemicalById.get(target.getLeft());
        if (chemical == null) {
          throw new ParserException("[" + target.getLeft().toString() + "] Chemical with id doesn't exist");
        }
        Target similarTarget = findSimilarTarget(chemical, target.getRight());
        if (similarTarget != null) {
          mergeTargets(similarTarget, target.getRight());
        } else {
          chemical.addTarget(target.getRight());
          result.add(target.getRight());
        }
      }
      return result;
    } catch (IOException e) {
      throw new ParserException("Problem with input", e);
    }
  }

  private void mergeTargets(Target similarTarget, Target right) {
    similarTarget.addTypes(right.getTypes());
    similarTarget.setDescription(similarTarget.getDescription() + "\n\n" + right.getDescription());
  }

  private Target findSimilarTarget(Chemical chemical, Target target) {
    for (Target t : chemical.getTargets()) {
      if (t.getIdentifiers().containsAll(target.getIdentifiers())) {
        return t;
      }
    }
    return null;
  }

  Map<Identifier, Chemical> createChemicalByIdMap(Collection<Chemical> chemicals) {
    Map<Identifier, Chemical> result = new HashMap<>();
    for (Chemical chemical : chemicals) {
      if (result.get(chemical.getSourceIdentifier()) != null) {
        throw new InvalidArgumentException(chemical.getSourceIdentifier() + "Chemical identifier duplicated");
      }
      result.put(chemical.getSourceIdentifier(), chemical);
    }
    return result;
  }

  private Pair<Identifier, Target> extractTarget(CSVRecord record) {
    Identifier chemicalIdentifier = identifierFactory.createIdentifier(IdentifierType.MESH, record.get("ChemicalID"));
    Target target = new Target();
    if (!record.get("CasRN").isEmpty()) {
      target.addIdentifier(identifierFactory.createIdentifier(IdentifierType.CAS, record.get("CasRN")));
    }
    target.setName(record.get("GeneSymbol"));
    if (!record.get("GeneID").isEmpty()) {
      target.addIdentifier(identifierFactory.createIdentifier(IdentifierType.ENTREZ, record.get("GeneID")));
    }
    if (!record.get("OrganismID").isEmpty()) {
      target.setOrganism(identifierFactory.createIdentifier(IdentifierType.TAXONOMY, record.get("OrganismID")));
    }
    target.setDescription(record.get("Interaction"));

    String[] types = record.get("InteractionActions").split("|", -1);
    target.addTypes(types);

    return new Pair<>(chemicalIdentifier, target);
  }

  public List<Target> extractChemicalTargets(String filename, Collection<Chemical> chemicals)
      throws FileNotFoundException, ParserException {
    return extractChemicalTargets(new FileInputStream(filename), chemicals);
  }

}
