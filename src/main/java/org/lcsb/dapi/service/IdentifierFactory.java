package org.lcsb.dapi.service;

import org.lcsb.dapi.model.Identifier;
import org.lcsb.dapi.model.IdentifierType;

public interface IdentifierFactory {
  Identifier createIdentifier(String text);

  Identifier createIdentifier(IdentifierType type, String resource);
}
