package org.lcsb.dapi.service;

import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.dao.*;
import org.lcsb.dapi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImp implements UserService {

  private UserDao userDao;

  private VerificationTokenDao verificationTokenDao;

  private ReleaseDao releaseDao;

  private PrivilegeDao privilegeDao;

  @Autowired
  public UserServiceImp(UserDao userDao, VerificationTokenDao verificationTokenDao, ReleaseDao releaseDao,
      PrivilegeDao privilegeDao) {
    this.userDao = userDao;
    this.verificationTokenDao = verificationTokenDao;
    this.releaseDao = releaseDao;
    this.privilegeDao = privilegeDao;
  }

  @Override
  public void add(User user) {
    userDao.add(user);
  }

  @Override
  public Page<User> getAll(Pageable pageable) {
    return userDao.getAll(pageable);
  }

  @Override
  public User getUserByLogin(String login) {
    return userDao.getUserByLogin(login);
  }

  @Override
  public void acceptLicense(User user, Release release) {
    boolean accepted = false;
    for (AcceptedReleaseLicense license : user.getAcceptedReleaseLicenses()) {
      if (license.getRelease().getId() == release.getId()) {
        accepted = true;
      }
    }
    if (!accepted) {
      user.addAcceptedReleaseLicense(new AcceptedReleaseLicense(release));
      userDao.update(user);

      grantUserPrivilege(user, PrivilegeType.READ, release.getId());
    }
  }

  @Override
  public void grantUserPrivilege(User user, PrivilegeType type, Integer objectId) {
    user = getUserByLogin(user.getLogin());
    if (objectId != null && releaseDao.getById(objectId) == null) {
      throw new InvalidArgumentException("Release with given id doesn't exist");
    }
    user.addPrivilege(privilegeDao.getPrivilegeForTypeAndObjectId(type, objectId));
    userDao.update(user);
  }

  @Override
  public void revokeUserPrivilege(User user, PrivilegeType type) {
    revokeUserPrivilege(user, type, null);
  }

  @Override
  public void revokeUserPrivilege(User user, PrivilegeType type, Integer objectId) {
    user = getUserByLogin(user.getLogin());
    user.removePrivilege(privilegeDao.getPrivilegeForTypeAndObjectId(type, objectId));
    userDao.update(user);
  }

  @Override
  public void grantUserPrivilege(User user, PrivilegeType type) {
    grantUserPrivilege(user, type, null);
  }

  @Override
  public User getUserByEmail(String email) {
    return userDao.getUserByEmail(email);
  }

  @Override
  public void createVerificationToken(User user, String token) {
    verificationTokenDao.add(new VerificationToken(user, token));

  }

  @Override
  public VerificationToken getVerificationToken(String token) {
    return verificationTokenDao.getByToken(token);
  }

  @Override
  public void update(User user) {
    userDao.update(user);
  }

}