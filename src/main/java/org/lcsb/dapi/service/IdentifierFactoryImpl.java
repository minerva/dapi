package org.lcsb.dapi.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.dao.IdentifierDao;
import org.lcsb.dapi.model.Identifier;
import org.lcsb.dapi.model.IdentifierType;
import org.springframework.stereotype.Service;

@Service
public class IdentifierFactoryImpl implements IdentifierFactory {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private Map<Integer, Identifier> cachedDbObjects = new HashMap<>();

  private IdentifierDao identifierDao;

  public IdentifierFactoryImpl(IdentifierDao identifierDao) {
    this.identifierDao = identifierDao;
  }

  public Identifier createIdentifier(String text) {
    IdentifierType type = null;
    String resource = null;
    for (IdentifierType identifierType : IdentifierType.values()) {
      if (text.startsWith(identifierType.getIdentifierPrefix() + ":")) {
        type = identifierType;
        resource = text.substring(identifierType.getIdentifierPrefix().length() + 1);
      }
      for (String uri : identifierType.getUris()) {
        if (text.startsWith(uri + ":")) {
          type = identifierType;
          resource = text.substring(uri.length() + 1);
        }
      }
    }
    if (type == null) {
      String tmp[] = text.split(":");
      if (tmp.length != 2) {
        throw new InvalidArgumentException("Don't know how to handle identifier: " + text);
      }
      try {
        type = IdentifierType.valueOf(tmp[0]);
      } catch (IllegalArgumentException e) {
        throw new InvalidArgumentException("Unknown identifier type: " + tmp[0], e);
      }
      resource = tmp[1];
    }
    return createIdentifier(type, resource);
  }

  @Override
  public Identifier createIdentifier(IdentifierType type, String resource) {
    if (resource == null || resource.isEmpty()) {
      throw new InvalidArgumentException("Identifier resource cannot be empty");
    }
    Identifier result = identifierDao.getByTypeAndResource(type, resource);
    if (result == null) {
      result = new Identifier(type, resource);
      identifierDao.add(result);
    }
    if (cachedDbObjects.get(result.getId()) == null) {
      cachedDbObjects.put(result.getId(), result);
    }
    return cachedDbObjects.get(result.getId());
  }
}
