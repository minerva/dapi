package org.lcsb.dapi.service;

import org.lcsb.dapi.controller.ObjectExistsException;
import org.lcsb.dapi.dao.LicenseDao;
import org.lcsb.dapi.dao.ReleaseDao;
import org.lcsb.dapi.model.License;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LicenseServiceImp implements LicenseService {

  private LicenseDao licenseDao;
  private ReleaseDao releaseDao;

  @Autowired
  public LicenseServiceImp(LicenseDao licenseDao, ReleaseDao releaseDao) {
    this.licenseDao = licenseDao;
    this.releaseDao = releaseDao;
  }

  @Override
  public Page<License> getAll(Pageable pageable) {
    return licenseDao.getAll(pageable);
  }

  @Override
  public void add(License license) throws ObjectExistsException {
    if (licenseDao.getByContentAndUrl(license.getContent(), license.getUrl()) != null) {
      throw new ObjectExistsException("License already exists");
    }
    licenseDao.add(license);
  }

  @Override
  public License getById(int id) {
    return licenseDao.getById(id);
  }

  @Override
  public void delete(License license) throws ObjectInConflictException {
    if (releaseDao.getByLicense(license).size() > 0) {
      throw new ObjectInConflictException("License is referenced by releases");
    }
    licenseDao.delete(license);
  }
}