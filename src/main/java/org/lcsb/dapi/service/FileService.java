package org.lcsb.dapi.service;

import java.io.IOException;

import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.model.UploadedFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FileService {
  UploadedFile add(UploadedFile file) throws IOException;

  UploadedFile addContent(Integer fileId, byte[] content) throws InvalidArgumentException, IOException;

  void delete(UploadedFile file);

  UploadedFile getById(Integer id);

  String getFilePath(UploadedFile file);

  Page<UploadedFile> getAll(Pageable pageable);

}
