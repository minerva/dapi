package org.lcsb.dapi.service;

import org.lcsb.dapi.model.ExternalDatabase;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ExternalDatabaseService {
  Page<ExternalDatabase> getAll(Pageable pageable);

  ExternalDatabase getByName(String name);

}
