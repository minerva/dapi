--admin/admin login password
insert into user_table (login, crypted_password, deleted, enabled) values ('admin', '$2a$10$DS3fuserOYSqKqcnnLJEGO.X0ppU1kPwQvVhXdymuRBV.MwxyjdTu', false, true);
insert into user_privilege_map_table (user_id, privilege_id) values ((select id from user_table where login = 'admin'), (select id from privilege_table where type='IS_ADMIN'));


insert into license_table (content, name, url) values ('test-content', 'test-name', 'http://dapi.lcsb.uni.lu/test');

insert into uploaded_file_table (filename, length) values ('non-existing-file', 0);
insert into release_table (external_database_id, license_id, source_file_id, name, timestamp) 
  select external_database_table.id, max(license_table.id), max(uploaded_file_table.id), 'test-release', now() from external_database_table, license_table, uploaded_file_table where external_database_table.name ='DrugBank' group by external_database_table.id;

--DRUG Bivalirudin   
insert into identifier_table (database_identifier , resource_identifier) values ('DRUG_BANK', 'DB00006');

insert into chemical_entity_table (description, entity_type, name, release_id, source_identifier_id) select 'Bivalirudin is a synthetic 20 residue peptide (thrombin inhibitor) which reversibly inhibits thrombin. Once bound to the active site, thrombin cannot activate fibrinogen into fibrin, the crucial step in the formation of thrombus. It is administered intravenously. Because it can cause blood stagnation, it is important to monitor changes in hematocrit, activated partial thromboplastin time, international normalized ratio and blood pressure.', 'DRUG', 'Bivalirudin', max(release_table.id), max(identifier_table.id) from release_table, identifier_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Bivalirudinum' from chemical_entity_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Bivalirudin' from chemical_entity_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Hirulog' from chemical_entity_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Bivalirudina' from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Angiox', max(id) from chemical_entity_table;

insert into identifier_table (database_identifier , resource_identifier) values ('TAXONOMY', '9606');
insert into identifier_table (database_identifier , resource_identifier) values ('DRUG_BANK_TARGET', 'BE0000048');
insert into identifier_table (database_identifier , resource_identifier) values ('UNIPROT', 'P00734');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '11060732');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '11929334');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '11923794');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '11504570');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '11752352');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '11833835');

insert into target_table (chemical_entity_id , name) select max(id), 'Prothrombin' from chemical_entity_table;
update target_table set organism_id = (select id from identifier_table where database_identifier ='TAXONOMY' and resource_identifier = '9606') where name = 'Prothrombin';
update target_table set source_identifier_id = (select id from identifier_table where database_identifier ='DRUG_BANK_TARGET' and resource_identifier = 'BE0000048') where name = 'Prothrombin';
insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='UNIPROT' and resource_identifier ='P00734';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11060732';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11929334';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11923794';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11504570';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11752352';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11833835';
insert into target_type (target_id , type) select max(id), 'inhibitor' from target_table;

-----------------------------------------------------

insert into identifier_table (database_identifier , resource_identifier) values ('DRUG_BANK', 'DB00007');
insert into chemical_entity_table (description, entity_type, name, release_id, source_identifier_id) select 'Leuprolide belongs to the general class of drugs known as hormones or hormone antagonists. It is a synthetic 9 residue peptide analog of gonadotropin releasing hormone. Leuprolide is used to treat advanced prostate cancer. It is also used to treat uterine fibroids and endometriosis. Leuprolide is also under investigation for possible use in the treatment of mild to moderate Alzheimer''s disease.', 'DRUG', 'Leuprolide', max(release_table.id), max(identifier_table.id) from release_table, identifier_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Leuprorelin' from chemical_entity_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Leuproreline' from chemical_entity_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Leuprorelinum' from chemical_entity_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Leuprorelina' from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Enantone', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Memryte\n      Curaxis', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Prostap 3\n      Takeda UK', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Leupromer', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Lutrate', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Prostap SR\n      Takeda UK', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'LeuProMaxx\n      Baxter/Teva', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Viadur\n      Bayer AG', max(id) from chemical_entity_table;
insert into drug_brand_names (brand_names, drug_id) select 'Leuplin\n      Takeda', max(id) from chemical_entity_table;

insert into identifier_table (database_identifier , resource_identifier) values ('DRUG_BANK_TARGET', 'BE0000203');
insert into identifier_table (database_identifier , resource_identifier) values ('UNIPROT', 'P30968');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '9625809');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '10687850');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '10394541');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '16809153');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '15758569');

insert into target_table (chemical_entity_id , name) select max(id), 'Gonadotropin-releasing hormone receptor' from chemical_entity_table;
update target_table set organism_id = (select id from identifier_table where database_identifier ='TAXONOMY' and resource_identifier = '9606') where name = 'Gonadotropin-releasing hormone receptor';
update target_table set source_identifier_id = (select id from identifier_table where database_identifier ='DRUG_BANK_TARGET' and resource_identifier = 'BE0000203') where name = 'Gonadotropin-releasing hormone receptor';

insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='UNIPROT' and resource_identifier ='P30968';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='9625809';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='10687850';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='10394541';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='16809153';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='15758569';
insert into target_type (target_id , type) select max(id), 'agonist' from target_table;

-----------------------------------------------------
insert into identifier_table (database_identifier , resource_identifier) values ('DRUG_BANK', 'DB00008');
insert into chemical_entity_table (description, entity_type, name, release_id, source_identifier_id) select 'Peginterferon alfa-2a is a form of recombinant interferon used as part of combination therapy to treat chronic Hepatitis C, an infectious liver disease caused by infection with Hepatitis C Virus (HCV). HCV is a single-stranded RNA virus that is categorized into nine distinct genotypes, with genotype 1 being the most common in the United States, and affecting 72% of all chronic HCV patients [L852]. Treatment options for chronic Hepatitis C have advanced significantly since 2011, with the development of Direct Acting Antivirals (DAAs) resulting in less use of Peginterferon alfa-2a. Peginterferon alfa-2a is derived from the alfa-2a moeity of recombinant human interferon and acts by binding to human type 1 interferon receptors. Activation and dimerization of this receptor induces the body''s innate antiviral response by activating the janus kinase/signal transducer and activator of transcription (JAK/STAT) pathway. Use of Peginterferon alfa-2a is associated with a wide range of severe adverse effects including the aggravation and development of endocrine and autoimmune disorders, retinopathies, cardiovascular and neuropsychiatric complications, and increased risk of hepatic decompensation in patients with cirrhosis. The use of Peginterferon alfa-2a has largely declined since newer interferon-free antiviral therapies have been developed.\n\nIn a joint recommendation published in 2016, the American Association for the Study of Liver Diseases (AASLD) and the Infectious Diseases Society of America (IDSA) no longer recommend Peginterferon alfa-2a for the treatment of Hepatitis C [A19593]. Peginterferon alfa-2a was used alongside [DB00811] with the intent to cure, or achieve a sustained virologic response (SVR), after 48 weeks of therapy. SVR and eradication of HCV infection is associated with significant long-term health benefits including reduced liver-related damage, improved quality of life, reduced incidence of Hepatocellular Carcinoma, and reduced all-cause mortality [A19626].\n\nPeginterferon alfa-2a is available as a fixed dose injector (tradename Pegasys) used for the treatment of chronic Hepatitis C. Approved in 2002 by the FDA, Pegasys is indicated for the treatment of HCV with [DB00811] or other antiviral drugs [FDA Label]. When combined together, Peginterferon alfa-2a and [DB00811] have been shown to achieve a SVR between 36% for genotype 1 and 59% for genotypes 2-6 after 48 weeks of treatment.', 'DRUG', 'Peginterferon alfa-2a', max(release_table.id), max(identifier_table.id) from release_table, identifier_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Pegylated interferon alfa-2a' from chemical_entity_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), 'Pegylated interferon alpha2a' from chemical_entity_table;

insert into identifier_table (database_identifier , resource_identifier) values ('DRUG_BANK_TARGET', 'BE0000385');
insert into identifier_table (database_identifier , resource_identifier) values ('DRUG_BANK_TARGET', 'BE0000661');
insert into identifier_table (database_identifier , resource_identifier) values ('UNIPROT', 'P48551');
insert into identifier_table (database_identifier , resource_identifier) values ('UNIPROT', 'P17181');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '16953837');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '19955815');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '15898717');

insert into target_table (chemical_entity_id , name) select max(id), 'Interferon alpha/beta receptor 2' from chemical_entity_table;
update target_table set organism_id = (select id from identifier_table where database_identifier ='TAXONOMY' and resource_identifier = '9606') where name = 'Interferon alpha/beta receptor 2';
update target_table set source_identifier_id = (select id from identifier_table where database_identifier ='DRUG_BANK_TARGET' and resource_identifier = 'BE0000385') where name = 'Interferon alpha/beta receptor 2';

insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='UNIPROT' and resource_identifier ='P48551';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='16953837';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='19955815';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='15898717';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11752352';
insert into target_type (target_id , type) select max(id), 'agonist' from target_table;

insert into target_table (chemical_entity_id , name) select max(id), 'Interferon alpha/beta receptor 2' from chemical_entity_table;
update target_table set organism_id = (select id from identifier_table where database_identifier ='TAXONOMY' and resource_identifier = '9606') where name = 'Interferon alpha/beta receptor 1';
update target_table set source_identifier_id = (select id from identifier_table where database_identifier ='DRUG_BANK_TARGET' and resource_identifier = 'BE0000661') where name = 'Interferon alpha/beta receptor 1';

insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='UNIPROT' and resource_identifier ='P17181';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='16953837';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='19955815';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='15898717';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='11752352';
insert into target_type (target_id , type) select max(id), 'agonist' from target_table;

-----------------------------------------------------
insert into release_table (external_database_id, license_id, source_file_id, name, timestamp) 
  select external_database_table.id, max(license_table.id), max(uploaded_file_table.id), 'test-release', now() from external_database_table, license_table, uploaded_file_table where external_database_table.name ='CTD' group by external_database_table.id;

insert into identifier_table (database_identifier , resource_identifier) values ('MESH', 'C089250');
insert into chemical_entity_table (description, entity_type, name, release_id, source_identifier_id) select '', 'DRUG', '(0.017ferrocene)amylose', max(release_table.id), max(identifier_table.id) from release_table, identifier_table;
insert into chemical_entity_synonyms (chemical_entity_id , synonyms) select max(id), '(0.017 ferrocene)amylose' from chemical_entity_table;

insert into identifier_table (database_identifier , resource_identifier) values ('MESH', 'D000230');
insert into identifier_table (database_identifier , resource_identifier) values ('MESH', 'D000077192');
insert into identifier_table (database_identifier , resource_identifier) values ('MESH', 'D002051');
insert into identifier_table (database_identifier , resource_identifier) values ('MESH', 'D002277');
insert into identifier_table (database_identifier , resource_identifier) values ('HGNC', 'MYC');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '26432044');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '27602772');
insert into identifier_table (database_identifier , resource_identifier) values ('PUBMED', '2228319');

insert into target_table (chemical_entity_id , name) select max(id), 'MYC' from chemical_entity_table;
update target_table set associated_disease_id = (select id from identifier_table where database_identifier ='MESH' and resource_identifier = 'D000230') where id = (select max(id) from target_table);
insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='HGNC' and resource_identifier ='MYC';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='26432044';

insert into target_table (chemical_entity_id , name) select max(id), 'MYC' from chemical_entity_table;
update target_table set associated_disease_id = (select id from identifier_table where database_identifier ='MESH' and resource_identifier = 'D000077192') where id = (select max(id) from target_table);
insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='HGNC' and resource_identifier ='MYC';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='26432044';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='27602772';

insert into target_table (chemical_entity_id , name) select max(id), 'MYC' from chemical_entity_table;
update target_table set associated_disease_id = (select id from identifier_table where database_identifier ='MESH' and resource_identifier = 'D002051') where id = (select max(id) from target_table);
insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='HGNC' and resource_identifier ='MYC';

insert into target_table (chemical_entity_id , name) select max(id), 'MYC' from chemical_entity_table;
update target_table set associated_disease_id = (select id from identifier_table where database_identifier ='MESH' and resource_identifier = 'D002277') where id = (select max(id) from target_table);
insert into target_identifier (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='HGNC' and resource_identifier ='MYC';
insert into target_reference (identifier , target) select max(identifier_table.id), max(target_table.id) from target_table, identifier_table where database_identifier='PUBMED' and resource_identifier ='2228319';

