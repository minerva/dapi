delete from target_type ;
delete from target_reference ;
delete from target_identifier;
delete from target_table;
delete from drug_brand_names;
delete from chemical_entity_synonyms;
delete from chemical_entity_table;
delete from identifier_table ;
delete from release_table;
delete from uploaded_file_table;
delete from license_table;

delete from user_privilege_map_table;
delete from user_table;

