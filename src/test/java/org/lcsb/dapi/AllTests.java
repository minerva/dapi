package org.lcsb.dapi;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.lcsb.dapi.config.AllConfigTests;
import org.lcsb.dapi.controller.AllControllerTests;
import org.lcsb.dapi.dao.AllDaoTests;
import org.lcsb.dapi.service.AllServiceTests;

@RunWith(Suite.class)
@SuiteClasses({AllConfigTests.class,
  AllControllerTests.class,
  AllDaoTests.class,
  AllServiceTests.class})
public class AllTests {

}
