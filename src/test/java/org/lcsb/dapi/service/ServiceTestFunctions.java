package org.lcsb.dapi.service;

import java.util.Calendar;
import java.util.Date;

import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.dao.*;
import org.lcsb.dapi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public abstract class ServiceTestFunctions extends DapiTestFunctions {

  @Autowired
  private ExternalDatabaseDao externalDatabaseDao;

  @Autowired
  private LicenseDao licenseDao;

  @Autowired
  private FileDao fileDao;

  @Autowired
  private ReleaseDao releaseDao;

  @Autowired
  private UserDao userDao;

  @Autowired
  private PasswordEncoder passwordEncoder;

  public ServiceTestFunctions() {
    super();
  }

  protected Release createAndPersistRelease() {
    License license = new License("nam", "u", "");
    licenseDao.add(license);

    UploadedFile file = new UploadedFile();
    fileDao.add(file);

    Release result = new Release();
    result.setName("version.1.0");
    Date today = Calendar.getInstance().getTime();
    result.setTimestamp(today);
    result.setSourceFile(file);

    result.setExternalDatabase(externalDatabaseDao.getByName("DrugBank"));
    result.setLicense(license);

    releaseDao.add(result);
    return result;
  }

  protected User createAndPersistUser() {
    User user = new User();
    user.setLogin("user");
    user.setCryptedPassword(passwordEncoder.encode("passwd"));
    userDao.add(user);
    return user;
  }

}