package org.lcsb.dapi.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.lcsb.dapi.ConfigProperties;
import org.lcsb.dapi.controller.dto.UserDto;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;

@Rollback(true)
public class LdapServiceTest extends ServiceTestFunctions {
  static Logger logger = LogManager.getLogger();

  private LdapService ldapService;

  @Autowired
  ConfigProperties configProperties;

  @Before
  public void setUp() throws Exception {

    ldapService = Mockito.spy(new LdapServiceImpl(configProperties));
    Mockito.when(ldapService.getConnection()).thenAnswer(new Answer<LDAPConnection>() {

      @Override
      public LDAPConnection answer(InvocationOnMock invocation) throws Throwable {
        // Create the configuration to use for the server.
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
        config.addAdditionalBindCredentials("uid=piotr.gawron,cn=users,cn=accounts,dc=uni,dc=lu", "test_passwd");
        config.setSchema(null);

        // Create the directory server instance, populate it with data from the
        // "test-data.ldif" file, and start listening for client connections.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
        ds.importFromLDIF(true, "src/test/resources/ldap-test.ldif");
        ds.startListening();
        return ds.getConnection();
      }
    });
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testLogin() throws Exception {
    assertTrue(ldapService.login("piotr.gawron", "test_passwd"));
    assertFalse(ldapService.login("piotr.gawron", "invalid_password"));
  }

  @Test
  public void testLoginCloseConnection() throws Exception {
    List<LDAPConnection> connections = new ArrayList<>();
    Mockito.when(ldapService.getConnection()).thenAnswer(new Answer<LDAPConnection>() {

      @Override
      public LDAPConnection answer(InvocationOnMock invocation) throws Throwable {
        // Create the configuration to use for the server.
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
        config.addAdditionalBindCredentials("uid=piotr.gawron,cn=users,cn=accounts,dc=uni,dc=lu", "test_passwd");
        config.setSchema(null);

        // Create the directory server instance, populate it with data from the
        // "test-data.ldif" file, and start listening for client connections.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
        ds.importFromLDIF(true, "src/test/resources/ldap-test.ldif");
        ds.startListening();
        LDAPConnection result = ds.getConnection();
        connections.add(result);
        return result;
      }
    });

    assertTrue(ldapService.login("piotr.gawron", "test_passwd"));
    for (LDAPConnection ldapConnection : connections) {
      assertFalse("There is open ldap connection after login", ldapConnection.isConnected());
    }
  }

  @Test
  public void testGetUserByLogin() throws Exception {
    UserDto user = ldapService.getUserByLogin("piotr.gawron");
    assertNotNull(user);
    assertEquals("piotr.gawron", user.getLogin());
    assertEquals("piotr.gawron@uni.lu", user.getEmail());
  }

}
