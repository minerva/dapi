package org.lcsb.dapi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.*;

import org.junit.*;
import org.lcsb.dapi.controller.ObjectNotFound;
import org.lcsb.dapi.dao.DrugDao;
import org.lcsb.dapi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

public class DrugServiceTest extends ServiceTestFunctions {

  @Autowired
  private DrugService drugService;

  @Autowired
  private DrugDao drugDao;

  Release release;

  @Before
  public void setUp() {

    release = createAndPersistRelease();
  }

  @After
  public void tearDown() {
  }

  @Test
  public void getDrugsByReleaseTest() throws ObjectNotFound {

    Drug aspirin = createDummyDrug(release);
    drugDao.add(aspirin);

    Release release = aspirin.getRelease();
    List<ChemicalEntity> list = drugService
        .findByRelease(release, PageRequest.of(0, 10), new HashMap<>(), new ArrayList<>()).getContent();

    assertEquals(1, list.size());
    assertEquals("Aspirin", list.get(0).getName());

  }

  @Test
  public void getByIdTest() throws Exception {

    Drug aspirin = createDummyDrug(release);
    drugDao.add(aspirin);
    ChemicalEntity drug = drugService.findById("DB00001", release, new ArrayList<>());

    assertNotNull(drug);

    assertEquals("Aspirin", drug.getName());
    assertEquals("used to treat headache", drug.getDescription());
  }

}
