package org.lcsb.dapi.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.lcsb.dapi.service.parser.AllParserTests;

@RunWith(Suite.class)
@SuiteClasses({
    AllParserTests.class,
    DrugServiceTest.class,
    IdentifierFactoryTest.class,
    LdapServiceTest.class,
    UserServiceTest.class,
})
public class AllServiceTests {

}
