package org.lcsb.dapi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.SpringTestConfig;
import org.lcsb.dapi.config.EhcacheConfig;
import org.lcsb.dapi.model.Identifier;
import org.lcsb.dapi.model.IdentifierType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.sf.ehcache.Ehcache;

@ContextConfiguration(classes = SpringTestConfig.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class IdentifierFactoryTest {

  @Autowired
  private IdentifierFactory identifierFactory;

  @Autowired
  private EhcacheConfig ehcacheConfig;

  @Test
  public void testCreateIdentifierFromCtdSource() {
    Identifier identifier = identifierFactory.createIdentifier("MESH:C089251");
    assertEquals(identifierFactory.createIdentifier(IdentifierType.MESH, "C089251"), identifier);
  }

  @Test
  public void testCreateIdentifierFromOmim() {
    Identifier identifier = identifierFactory.createIdentifier("OMIM:613720");
    assertEquals(identifierFactory.createIdentifier(IdentifierType.OMIM, "613720"), identifier);
  }

  @Test
  public void testCreateIdentifierFromMiriamUri() {
    Identifier identifier = identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");
    assertEquals(identifierFactory.createIdentifier(IdentifierType.UNIPROT, "P30969"), identifier);
  }

  @Test
  public void testCreateIdentifierFromIdentifierPrefixedId() {
    Identifier identifier = identifierFactory.createIdentifier("hgnc.symbol:SNCA");
    assertEquals(identifierFactory.createIdentifier(IdentifierType.HGNC, "SNCA"), identifier);
  }

  @Test
  public void persistedObjectShouldReferToTheSameInstance() {
    Identifier identifier = identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");
    Identifier identifier2 = identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");
    assertTrue(identifier == identifier2);
  }

  @Test
  public void checkLevel2Caching() {
    identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");

    Ehcache tempManagers = ehcacheConfig.ehCacheManager().getEhcache(EhcacheConfig.IDENTIFIER_CACHE_INSTANCE);

    long beforeCachingHits = tempManagers.getStatistics().cacheHitCount();

    identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");
    identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");
    identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");
    identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");
    identifierFactory.createIdentifier("urn:miriam:uniprot:P30969");

    long afterCachingHits = tempManagers.getStatistics().cacheHitCount();

    assertTrue(beforeCachingHits < afterCachingHits);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateIdentifierFromInvalidInput() {
    identifierFactory.createIdentifier("blabla");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateIdentifierFromInvalidString() {
    identifierFactory.createIdentifier("blabla");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateIdentifierFromInvalidUrn() {
    identifierFactory.createIdentifier("blabla:bla");
  }

}
