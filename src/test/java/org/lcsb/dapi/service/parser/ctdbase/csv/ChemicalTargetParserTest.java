package org.lcsb.dapi.service.parser.ctdbase.csv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ChemicalTargetParserTest extends DapiTestFunctions {

  @Autowired
  private IdentifierFactory identifierFactory;

  ChemicalTargetParser parser;

  @Before
  public void setUp() {
    parser = new ChemicalTargetParser(identifierFactory);
  }

  @Test
  public void testExtractTargets() throws Exception {
    List<Chemical> chemicals = createChemicalList(new String[] { "C089250" });
    List<Target> result = parser.extractChemicalTargets("src/test/resources/testFiles/CTD_chem_gene_ixns.csv",
        chemicals);
    assertEquals(2, result.size());
    assertEquals(2, chemicals.get(0).getTargets().size());

    Target target = result.get(1);
    assertEquals("MYC", target.getName());
  }

  private List<Chemical> createChemicalList(String[] identifiers) {
    List<Chemical> chemicals = new ArrayList<>();
    for (String identifier : identifiers) {
      Chemical chemical = new Chemical();
      chemical.setSourceIdentifier(identifierFactory.createIdentifier(IdentifierType.MESH, identifier));
      chemicals.add(chemical);
    }
    return chemicals;
  }

  @Test
  public void testCreateChemicalById() throws Exception {
    Map<Identifier, Chemical> chemicalById = parser
        .createChemicalByIdMap(createChemicalList(new String[] { "C089250" }));
    assertNotNull(chemicalById.get(identifierFactory.createIdentifier(IdentifierType.MESH, "C089250")));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateChemicalByIdForInvalidList() {
    parser.createChemicalByIdMap(createChemicalList(new String[] { "C089250", "C089250" }));
  }

}
