package org.lcsb.dapi.service.parser.drugbank;

import static org.junit.Assert.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.lcsb.dapi.service.parser.ParserException;
import org.springframework.beans.factory.annotation.Autowired;

public class DrugBankParserTest extends DapiTestFunctions {
  Logger logger = LogManager.getLogger();

  @Autowired
  private IdentifierFactory identifierFactory;

  @Test
  public void lepirudinTest() throws Exception {

    String fileName = "testFiles/Lepirudin.txt";
    InputStream inputStream = new FileInputStream(new File(fileName));
    List<ChemicalEntity> druglist = new DrugBankParser(identifierFactory).extractRelease(inputStream)
        .getChemicalEntities();

    Drug lepirudin = (Drug) druglist.get(0);

    assertEquals("expected only one drug", 1, druglist.size());
    assertEquals("Expected was Lepirudin", "Lepirudin", lepirudin.getName());
    assertNotNull(lepirudin.isApproved());

    assertTrue(lepirudin.getSynonyms().contains("Hirudin variant-1"));

    assertEquals(1, lepirudin.getTargets().size());

    Target target = lepirudin.getTargets().get(0);
    assertEquals("References are not parsed properly", 6, target.getReferences().size());
    assertEquals("Prothrombin", target.getName());
    assertNotNull(target.getOrganism());
    assertNotNull(target.getSourceIdentifier());
  }

  @Test
  public void releaseLepirudinTest() throws Exception {
    String fileName = "testFiles/Lepirudin.txt";
    InputStream inputStream = new FileInputStream(new File(fileName));
    List<ChemicalEntity> druglist = new DrugBankParser(identifierFactory).extractRelease(inputStream)
        .getChemicalEntities();

    Drug lepirudin = (Drug) druglist.get(0);

    assertEquals("5.1", lepirudin.getRelease().getName());
  }

  @Test
  public void chlorphenesinTest() throws Exception {

    String fileName = "testFiles/Chlorphenesin.txt";

    InputStream inputStream = new FileInputStream(new File(fileName));
    List<ChemicalEntity> druglist = new DrugBankParser(identifierFactory).extractRelease(inputStream)
        .getChemicalEntities();

    Drug chlor = (Drug) druglist.get(0);

    assertEquals("expected only 3 drug", 3, druglist.size());
    assertEquals("Expected was Chlorphenesin", "Chlorphenesin", chlor.getName());

  }

  @Test
  public void threeDrugTest() throws Exception {
    String fileName = "testFiles/ThreeDrug.txt";
    InputStream inputStream = new FileInputStream(new File(fileName));
    List<ChemicalEntity> druglist = new DrugBankParser(identifierFactory).extractRelease(inputStream)
        .getChemicalEntities();

    Drug bivalirudin = (Drug) druglist.get(0);

    assertEquals("The expected number of drugs is 3 ", 3, druglist.size());
    logger.debug(bivalirudin.getBrandNames());
    assertTrue(bivalirudin.getBrandNames().contains("Angiox"));
    assertTrue(bivalirudin.getSynonyms().contains("Bivalirudina"));

    assertEquals(IdentifierType.DRUG_BANK_TARGET,
        bivalirudin.getTargets().get(0).getSourceIdentifier().getDatabaseIdentifier());

    assertEquals(IdentifierType.UNIPROT,
        bivalirudin.getTargets().get(0).getIdentifiers().get(0).getDatabaseIdentifier());
  }

  @Test
  public void multiDrugTest() throws Exception {
    String fileName = "testFiles/MultipleDrugs.txt";
    InputStream inputStream = new FileInputStream(new File(fileName));
    List<ChemicalEntity> druglist = new DrugBankParser(identifierFactory).extractRelease(inputStream)
        .getChemicalEntities();

    Drug alteplase = (Drug) druglist.get(0);
    assertEquals("The expected number of drugs is 9 ", 9, druglist.size());
    assertTrue(alteplase.getSynonyms().contains("Alteplasa"));
  }

  @Test(expected = ParserException.class)
  public void testInvalidXmlSchema() throws Exception {
    String content = "<xml>asd</xml>";
    InputStream stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
    new DrugBankParser(identifierFactory).extractRelease(stream);
  }

  @Test
  public void testParseZippedFile() throws Exception {

    String fileName = "src/test/resources/testFiles/drugbank/long-synonym.xml.zip";
    List<ChemicalEntity> druglist = new DrugBankParser(identifierFactory).extractRelease(fileName)
        .getChemicalEntities();

    assertEquals(1, druglist.size());
  }

  @Test
  public void testAmantadine() throws Exception {

    String fileName = "src/test/resources/testFiles/drugbank/amantadine.xml";
    InputStream inputStream = new FileInputStream(new File(fileName));
    List<ChemicalEntity> druglist = new DrugBankParser(identifierFactory).extractRelease(inputStream)
        .getChemicalEntities();

    Drug amantadine = (Drug) druglist.get(0);

    assertEquals(8, amantadine.getSynonyms().size());

    assertEquals(4, amantadine.getBrandNames().size());
  }

}
