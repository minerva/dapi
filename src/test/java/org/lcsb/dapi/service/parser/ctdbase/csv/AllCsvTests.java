package org.lcsb.dapi.service.parser.ctdbase.csv;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ChemicalDiseaseTargetParserTest.class,
    ChemicalParserTest.class,
    ChemicalTargetParserTest.class })
public class AllCsvTests {

}
