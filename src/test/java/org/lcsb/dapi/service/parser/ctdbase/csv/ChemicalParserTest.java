package org.lcsb.dapi.service.parser.ctdbase.csv;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ChemicalParserTest extends DapiTestFunctions {

  @Autowired
  IdentifierFactory identifierFactory;
  
  ChemicalParser chemicalParser;

  @Before
  public void setUp() {
    chemicalParser = new ChemicalParser(identifierFactory);
  }

  @Test
  public void testExtractChemicals() throws Exception {
    List<Chemical> result = chemicalParser.extractChemicals("src/test/resources/testFiles/CTD_chemicals.csv");
    assertEquals(1, result.size());

    Chemical amylose = result.get(0);
    assertEquals("(0.017ferrocene)amylose", amylose.getName());
    assertEquals(identifierFactory.createIdentifier(IdentifierType.MESH, "C089250"), amylose.getSourceIdentifier());
    assertEquals(1, amylose.getSynonyms().size());
  }

}
