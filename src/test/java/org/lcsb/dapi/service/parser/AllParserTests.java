package org.lcsb.dapi.service.parser;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.lcsb.dapi.service.parser.ctdbase.AllCtdbaseTests;
import org.lcsb.dapi.service.parser.drugbank.DrugBankParserTest;

@RunWith(Suite.class)
@SuiteClasses({ AllCtdbaseTests.class,
    DrugBankParserTest.class
})
public class AllParserTests {

}
