package org.lcsb.dapi.service.parser.ctdbase.xml;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ChemicalParserTest.class })
public class AllXmlTests {

}
