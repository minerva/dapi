package org.lcsb.dapi.service.parser.ctdbase;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.lcsb.dapi.service.parser.ctdbase.csv.AllCsvTests;
import org.lcsb.dapi.service.parser.ctdbase.xml.AllXmlTests;

@RunWith(Suite.class)
@SuiteClasses({
    AllCsvTests.class,
    AllXmlTests.class,
    CtdParserTest.class,

})
public class AllCtdbaseTests {

}
