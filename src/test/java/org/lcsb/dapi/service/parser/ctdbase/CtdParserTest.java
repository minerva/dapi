package org.lcsb.dapi.service.parser.ctdbase;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.Release;
import org.springframework.beans.factory.annotation.Autowired;

public class CtdParserTest extends DapiTestFunctions {

  @Autowired
  private CtdParser ctdParser;

  @Test
  public void testExtractReleaseFromCsvSources() throws Exception {
    Release release = ctdParser.extractRelease("src/test/resources/testFiles/ctd-csv.zip");
    assertNotNull(release);
    assertTrue(release.getChemicalEntities().size() > 0);
  }

  @Test
  public void testExtractReleaseFromCsvWithDisease() throws Exception {
    Release release = ctdParser.extractRelease("src/test/resources/testFiles/ctd-with-diseases-csv.zip");
    assertNotNull(release);
    assertTrue(release.getChemicalEntities().size() > 0);
  }

  @Test
  public void testExtractReleaseFromXmlSources() throws Exception {
    Release release = ctdParser.extractRelease("src/test/resources/testFiles/ctd-xml.zip");
    assertNotNull(release);
    assertTrue(release.getChemicalEntities().size() > 0);
  }

}
