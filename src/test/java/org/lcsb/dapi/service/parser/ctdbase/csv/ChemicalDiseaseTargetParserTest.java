package org.lcsb.dapi.service.parser.ctdbase.csv;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ChemicalDiseaseTargetParserTest extends DapiTestFunctions {

  @Autowired
  private IdentifierFactory identifierFactory;
  
  ChemicalDiseaseTargetParser parser;

  @Before
  public void setUp() {
    parser = new ChemicalDiseaseTargetParser(identifierFactory);
  }

  @Test
  public void testExtractTargets() throws Exception {
    List<Chemical> chemicals = createChemicalList(new String[] { "C089250" });
    List<Target> result = parser.extractChemicalTargets("src/test/resources/testFiles/CTD_chemicals_diseases.csv",
        chemicals);
    assertEquals(4, result.size());
    assertEquals(4, chemicals.get(0).getTargets().size());

    Target target = result.get(0);
    assertEquals("MYC", target.getName());
  }

  private List<Chemical> createChemicalList(String[] identifiers) {
    List<Chemical> chemicals = new ArrayList<>();
    for (String identifier : identifiers) {
      Chemical chemical = new Chemical();
      chemical.setSourceIdentifier(identifierFactory.createIdentifier(IdentifierType.MESH, identifier));
      chemicals.add(chemical);
    }
    return chemicals;
  }

}
