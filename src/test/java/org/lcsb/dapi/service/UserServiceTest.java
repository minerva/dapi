package org.lcsb.dapi.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.lcsb.dapi.model.Release;
import org.lcsb.dapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceTest extends ServiceTestFunctions {

  @Autowired
  private UserService userService;

  Release release;
  User user;

  @Before
  public void setUp() {
    user = createAndPersistUser();
    release = createAndPersistRelease();
  }

  @Test
  public void testAcceptLicense() {
    int privileges = user.getPrivileges().size();
    userService.acceptLicense(user, release);
    assertEquals(privileges + 1, user.getPrivileges().size());
  }

  @Test
  public void testAcceptLicenseTwice() {
    int privileges = user.getPrivileges().size();
    int acceptedLicenses = user.getAcceptedReleaseLicenses().size();
    userService.acceptLicense(user, release);
    userService.acceptLicense(user, release);
    assertEquals(privileges + 1, user.getPrivileges().size());
    assertEquals(acceptedLicenses + 1, user.getAcceptedReleaseLicenses().size());
  }

}
