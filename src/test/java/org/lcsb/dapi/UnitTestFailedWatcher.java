package org.lcsb.dapi;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class UnitTestFailedWatcher extends TestWatcher {
  @Override
  protected void failed(Throwable e, Description description) {
    if (!(e instanceof AssertionError)) {
      e.printStackTrace();
    }
  }
}
