package org.lcsb.dapi;

import org.junit.Rule;
import org.junit.runner.RunWith;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Rollback(true)
@ContextConfiguration(classes = SpringTestConfig.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class DapiTestFunctions {

  @Autowired
  IdentifierFactory identifierFactory;

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  protected Drug createDummyDrug(Release release) {
    Drug drug = new Drug();
    drug.setName("Aspirin");

    drug.setSourceIdentifier(identifierFactory.createIdentifier(IdentifierType.DRUG_BANK, "DB00001"));

    drug.setDescription("used to treat headache");

    drug.setBloodBrainBarrier(null);
    drug.setApproved(true);

    drug.addBrandName("GalxcoSmithKlien");

    drug.addSynonym("AspirinPainKiller");

    release.addChemicalEntity(drug);
    drug.setRelease(release);

    Target tarOne = new Target();
    tarOne.setName("kinase");

    tarOne.setOrganism(identifierFactory.createIdentifier(IdentifierType.TAXONOMY, "9606"));

    tarOne.setSourceIdentifier(identifierFactory.createIdentifier(IdentifierType.DRUG_BANK, "DB00002"));

    tarOne.addType("inhibitor");

    tarOne.addIdentifier(identifierFactory.createIdentifier(IdentifierType.UNIPROT, "P12345"));

    drug.addTarget(tarOne);

    return drug;
  }

}