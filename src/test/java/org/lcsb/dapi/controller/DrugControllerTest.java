package org.lcsb.dapi.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.lcsb.dapi.model.Drug;
import org.lcsb.dapi.model.Target;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.test.web.servlet.RequestBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public class DrugControllerTest extends ControllerTestFunctions {

  private static String UNKNOWN_DATABASE = "not_a_drugbank_database";

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void findByIdTest() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/{database}/releases/{version}/drugs/{drugId}",
        "DrugBank", TEST_DRUGBANK_RELEASE, TEST_DRUG_ID_BIVALIRUDIN)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("drug/get-by-id",
            pathParameters(parameterWithName("database").description("database name"),
                parameterWithName("version").description("release version of the database"),
                parameterWithName("drugId").description("identifier of the chemical in the database")),
            responseFields(getChemicalEntityDescription())))
        .andReturn().getResponse().getContentAsString();

    String drugName = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("name")
        .getAsString();

    assertEquals("Bivalirudin", drugName);

    int brandNamesCount = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("brandNames")
        .getAsJsonArray().size();

    assertEquals(1, brandNamesCount);

  }

  @Test
  public void testGetByIdWithoutAccess() throws Exception {
    RequestBuilder request = get("/api/database/DrugBank/releases/{release}/drugs/{drugId}", TEST_DRUGBANK_RELEASE,
        TEST_DRUG_ID_BIVALIRUDIN);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGetByInvalidId() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/DrugBank/releases/{release}/drugs/blabla", TEST_DRUGBANK_RELEASE)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetByInvalidReleaseId() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    RequestBuilder request = get("/api/database/DrugBank/releases/x.y/drugs/blabla")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void findByReleaseTest() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/DrugBank/releases/{release}/drugs/", TEST_DRUGBANK_RELEASE)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(3, drugs.size());

    Set<String> names = new HashSet<>();
    for (int i = 0; i < drugs.size(); i++) {
      names.add(drugs.get(i).getAsJsonObject().get("name").getAsString());
    }
    assertTrue(names.contains("Bivalirudin"));
    assertTrue(names.contains("Leuprolide"));
    assertTrue(names.contains("Peginterferon alfa-2a"));
  }

  @Test
  public void testPagination() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/{database}/releases/{version}/drugs/?size=1", "DrugBank",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(listDrugDocument("drug/get-all"))
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testFilterByName() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/{database}/releases/{version}/drugs/?name=leuprolide", "DrugBank",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(listDrugDocument("drug/get-filtered-by-name"))
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testFilterByTargetIdentifier() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/api/database/{database}/releases/{version}/drugs/?target_identifier=urn:miriam:uniprot:P30968", "DrugBank",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(listDrugDocument("drug/get-filtered-by-target-identifier"))
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testFilterByTargetDiseaseIdentifier() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/api/database/{database}/releases/{version}/drugs/?target_disease_identifier=urn:miriam:mesh:D000230", "CTD",
        TEST_CTD_RELEASE).session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(listDrugDocument("drug/get-filtered-by-target-disease-identifier"))
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testFilterByTargetIdentifierWithMultipleTargets() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/api/database/{database}/releases/{version}/drugs/?target_identifier=hgnc.symbol:MYC", "CTD",
        TEST_CTD_RELEASE).session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(listDrugDocument("drug/get-filtered-by-target-disease-identifier"))
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testFilterBySynonym() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/api/database/{database}/releases/{version}/drugs/?synonym=Leuprorelina", "DrugBank",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(listDrugDocument("drug/get-filtered-by-synonym"))
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testFilterBySynonymCaseInsensitive() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/api/database/{database}/releases/{version}/drugs/?synonym=leuprorelina", "DrugBank",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testFilterByUnknownTargetIdentifier() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/api/database/{database}/releases/{version}/drugs/?target_identifier=hgnc.symbol:SIRT3", "DrugBank",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(0, drugs.size());
  }

  @Test
  public void testFilterByInvalidTargetIdentifier() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/api/database/{database}/releases/{version}/drugs/?target_identifier=hgnc_symbol:SIRT3", "DrugBank",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  private RestDocumentationResultHandler listDrugDocument(String apiCallIdentifier) {
    return document(apiCallIdentifier,
        pathParameters(parameterWithName("database").description("database name"),
            parameterWithName("version").description("release version of the database")),

        getPaginationRequestDescription().and(
            parameterWithName("name").description("drug name (case insensitive)").optional(),
            parameterWithName("synonym").description("synonym (case insensitive)").optional(),
            parameterWithName("target_identifier")
                .description("target identifier in identifiers.org format (for example: 'uniprot:P30968')")
                .optional(),
            parameterWithName("target_disease_identifier")
                .description("target disease identifier in identifiers.org format (for example: 'mesh:D000230')")
                .optional()),

        getPaginationResponseDescription("drugs", Drug.class));
  }

  @Test
  public void testGetListWithoutAccess() throws Exception {
    RequestBuilder request = get("/api/database/DrugBank/releases/{release}/drugs/", TEST_DRUGBANK_RELEASE);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGetPageOutOfRange() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/DrugBank/releases/{release}/drugs/?page=10", TEST_DRUGBANK_RELEASE)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertEquals(0, drugs.size());
  }

  @Test
  public void testFindByInvalidRelease() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/DrugBank/releases/x.y/drugs/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void findByIdInUnknownDatabase() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/" + UNKNOWN_DATABASE + "/releases/{release}/drugs/{drugId}",
        TEST_DRUGBANK_RELEASE, TEST_DRUG_ID_BIVALIRUDIN)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void findByReleaseInInvalidDatabaseTest() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/" + UNKNOWN_DATABASE + "/releases/{release}/drugs/",
        TEST_DRUGBANK_RELEASE)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testGetFromCtd() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/database/ctd/releases/{release}/drugs/", TEST_CTD_RELEASE).session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    JsonArray drugs = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray();

    assertTrue(drugs.size() > 0);

  }

  @Test
  public void findByIdTestWithLimitedColumns() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String columns = "name,synonyms,targets";

    RequestBuilder request = get("/api/database/{database}/releases/{version}/drugs/{drugId}?columns=" + columns,
        "DrugBank", TEST_DRUGBANK_RELEASE,
        TEST_DRUG_ID_BIVALIRUDIN)
            .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("drug/get-by-id-limited-columns",
            pathParameters(parameterWithName("database").description("database name"),
                parameterWithName("version").description("release version of the database"),
                parameterWithName("drugId").description("identifier of the chemical in the database")),
            responseFields(getChemicalEntityDescription(columns))));
  }

  protected List<FieldDescriptor> getChemicalEntityDescription() {
    return getChemicalEntityDescription(null);
  }

  protected List<FieldDescriptor> getChemicalEntityDescription(String columnsString) {
    List<FieldDescriptor> list = new ArrayList<>(Arrays.asList(
        fieldWithPath("name")
            .description("chemical name"),
        fieldWithPath("description")
            .description("chemical description"),
        fieldWithPath("synonyms")
            .description("list of synonyms"),
        fieldWithPath("sourceIdentifier")
            .description("identifier referencing the entry in source database"),
        fieldWithPath("annotations")
            .description("list of annotations of the entry in identifiers.org format"),
        subsectionWithPath("targets")
            .description("list of drug targets")
            .type("Array<" + Target.class.getSimpleName() + ">")));

    list.addAll(PayloadDocumentation.applyPathPrefix("targets[].", getTargetDescription()));
    list.addAll(getChemicalDescription());
    list.addAll(getDrugDescription());

    if (columnsString != null && !columnsString.isEmpty()) {
      Set<String> columns = new HashSet<>(Arrays.asList(columnsString.split(",")));
      List<FieldDescriptor> result = new ArrayList<>();
      for (FieldDescriptor descriptor : list) {
        if (columns.contains(descriptor.getPath())) {
          result.add(descriptor);
        }
      }

      return result;
    } else {
      return list;
    }
  }

  private List<FieldDescriptor> getTargetDescription() {
    return Arrays.asList(
        fieldWithPath("name")
            .description("target name"),
        fieldWithPath("description")
            .description("description")
            .type("String"),
        fieldWithPath("organism")
            .description("organism id")
            .type("String"),
        fieldWithPath("sourceIdentifier")
            .description("identifier referenceing the entry in source database"),
        fieldWithPath("associatedDisease")
            .description("disease id associated with the chemical-target interaction")
            .type("String"),
        fieldWithPath("identifiers")
            .description("miriam uris identifying the target")
            .type("Array<String>"),
        fieldWithPath("references")
            .description("list of miriam uris identifying publications about the target")
            .type("Array<String>"),
        fieldWithPath("types")
            .description("list of types describing the chemical-target interaction")
            .type("Array<String>"));
  }

  private List<FieldDescriptor> getChemicalDescription() {
    return new ArrayList<>();
  }

  private List<FieldDescriptor> getDrugDescription() {
    return Arrays.asList(
        fieldWithPath("bloodBrainBarrier")
            .description("blood brain barrier").type("Boolean"),
        fieldWithPath("approved")
            .description("is the drug approved").type("Boolean"),
        fieldWithPath("brandNames")
            .description("brand names").type("Array<String>"));
  }
}