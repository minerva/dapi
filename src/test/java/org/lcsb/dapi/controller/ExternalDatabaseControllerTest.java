package org.lcsb.dapi.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.*;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.lcsb.dapi.model.ExternalDatabase;
import org.lcsb.dapi.service.ExternalDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.RequestBuilder;

import com.google.gson.JsonParser;

@Rollback(true)
@Transactional
public class ExternalDatabaseControllerTest extends ControllerTestFunctions {

  Logger logger = LogManager.getLogger();

  @Autowired
  public ExternalDatabaseService service;

  @After
  public void tearDown() {
  }

  @Test
  public void testGetDatabases() throws Exception {
    RequestBuilder request = get("/api/database/");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("database/get-all",
            getPaginationRequestDescription(),
            getPaginationResponseDescription("databases", ExternalDatabase.class).andWithPrefix("content[].",
                getDatabaseDescription())))
        .andReturn().getResponse().getContentAsString();

    int count = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray().size();

    assertTrue(count > 0);

  }

  @Test
  public void testNumberOfPaginatedDatabases() throws Exception {
    RequestBuilder request = get("/api/database/?size=1")
        .contentType(MediaType.APPLICATION_JSON);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int count = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray().size();

    assertTrue(count <= 1);

  }

  @Test
  public void testPaginationOutOfRange() throws Exception {
    RequestBuilder request = get("/api/database/?page=100")
        .contentType(MediaType.APPLICATION_JSON);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int count = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray().size();

    assertEquals(0, count);

  }
  
  protected List<FieldDescriptor> getDatabaseDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("name")
            .description("database name")));
    return result;
  }
  
}