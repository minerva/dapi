package org.lcsb.dapi.controller;

import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.*;
import java.text.ParseException;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.lcsb.dapi.SpringTestConfig;
import org.lcsb.dapi.dao.*;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.lcsb.dapi.service.UserService;
import org.lcsb.dapi.service.parser.ParserException;
import org.lcsb.dapi.service.parser.ctdbase.CtdParser;
import org.lcsb.dapi.service.parser.drugbank.DrugBankParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.payload.*;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.xml.sax.SAXException;

@ContextConfiguration(classes = SpringTestConfig.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class ControllerTestFunctions {

  @Rule
  public JUnitRestDocumentation jUnitRestDocumentation = new JUnitRestDocumentation();

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  protected static final String TEST_ADMIN_PASSWORD = "admin";
  protected static final String TEST_ADMIN_LOGIN = "admin";

  protected static final String TEST_DRUGBANK_RELEASE = "test-release";
  protected static final String TEST_DRUG_ID_BIVALIRUDIN = "DB00006";

  protected static final String TEST_CTD_RELEASE = "test-release";

  @Autowired
  private WebApplicationContext context;

  @Autowired
  private Filter springSecurityFilterChain;

  protected MockMvc mockMvc;

  @Autowired
  protected DrugDao drugDao;

  @Autowired
  protected LicenseDao licenseDao;

  @Autowired
  private FileDao fileDao;

  @Autowired
  protected UserDao userDao;

  @Autowired
  protected UserService userService;

  @Autowired
  protected ReleaseDao releaseDao;

  @Autowired
  private ExternalDatabaseDao externalDatabaseDao;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private IdentifierFactory identifierFactory;

  @Autowired
  private CtdParser ctdParser;

  public ControllerTestFunctions() {
    super();
  }

  @PostConstruct
  public void construct() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .addFilter(springSecurityFilterChain)
        .apply(org.springframework.restdocs.mockmvc.MockMvcRestDocumentation
            .documentationConfiguration(this.jUnitRestDocumentation)
            .uris().withHost("dapi.lcsb.uni.lu").withPort(80))
        .build();
  }

  /**
   * This method can be used to work around the fact that MockMvc cannot retrieve
   * cookies from Spring Security. The Reason for that is that MockMvc calls the
   * controller directly and (partially?) bypasses Spring.
   *
   * This method creates a mocked session that can be used in a request as opposed
   * to the token.
   *
   * FIXME: Find a better solution, that does not violate the spirit of
   * integration tests.
   */
  protected MockHttpSession createSession(String login, String password) throws Exception {
    RequestBuilder request = post("/api/doLogin")
        .param("login", login)
        .param("password", password);
    return (MockHttpSession) mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn()
        .getRequest()
        .getSession();
  }

  protected Release mockDatabaseFromFile(String fileName, User user)
      throws IOException, ParserConfigurationException, ParseException, SAXException, ParserException {

    License license = new License("name", "url", "");
    licenseDao.add(license);
    InputStream inputStream = new FileInputStream(new File(fileName));
    Release release = new DrugBankParser(identifierFactory).extractRelease(inputStream);
    release.setSourceFile(createFile());
    release.setLicense(license);
    release.setExternalDatabase(externalDatabaseDao.getByName("DrugBank"));

    releaseDao.add(release);

    userService.acceptLicense(user, release);

    return release;
  }

  protected Release mockCtdDatabaseFromFile(String fileName, User user)
      throws IOException, ParserConfigurationException, ParseException, SAXException, ParserException {

    License license = new License("name", "url", "");
    licenseDao.add(license);
    InputStream inputStream = new FileInputStream(new File(fileName));
    Release release = ctdParser.extractRelease(inputStream);
    release.setSourceFile(createFile());
    release.setLicense(license);
    release.setExternalDatabase(externalDatabaseDao.getByName("ctd"));
    release.setName(license.getId() + "");

    releaseDao.add(release);

    releaseDao.update(release);

    userService.acceptLicense(user, release);

    return release;
  }

  protected User createUser(String login, String password) {
    User user = new User();
    user.setLogin(login);
    user.setCryptedPassword(passwordEncoder.encode(password));
    user.setEnabled(true);
    userDao.add(user);

    return user;
  }

  protected RequestParametersSnippet getPaginationRequestDescription() {
    return requestParameters(
        parameterWithName("size")
            .description("page size")
            .optional(),
        parameterWithName("number")
            .optional()
            .description("page number"));
  }

  protected ResponseFieldsSnippet getPaginationResponseDescription(String objectNamePlural, Class<?> type) {
    return responseFields(
        subsectionWithPath("content")
            .description("list of " + objectNamePlural + " in the system following search criteria")
            .type("Array<" + type.getSimpleName() + ">"),
        fieldWithPath("totalPages")
            .description("how many pages are available"),
        fieldWithPath("totalElements")
            .description("how many " + objectNamePlural + " are in the database"),
        fieldWithPath("numberOfElements")
            .description("how many " + objectNamePlural + " are in the result list"),
        fieldWithPath("number")
            .description("page number"),
        fieldWithPath("size")
            .description("size of the page"));
  }

  protected List<FieldDescriptor> getCreateLicenseDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("name")
            .description("name"),
        fieldWithPath("url")
            .description("url where the license is located").optional(),
        fieldWithPath("content")
            .description("text of the license").type("String").optional()));
    return result;
  }

  protected List<FieldDescriptor> getLicenseDescription() {
    List<FieldDescriptor> result = getCreateLicenseDescription();
    result.add(
        fieldWithPath("id")
            .description("license identifier"));
    return result;
  }

  public Release createRelease() {
    License license = new License("nam", "u", "");
    licenseDao.add(license);

    UploadedFile file = createFile();

    Release result = new Release();
    result.setName("version.1.0");
    Date today = Calendar.getInstance().getTime();
    result.setTimestamp(today);
    result.setSourceFile(file);

    result.setExternalDatabase(externalDatabaseDao.getByName("DrugBank"));
    result.setLicense(license);

    releaseDao.add(result);
    return result;
  }

  protected List<FieldDescriptor> optional(List<FieldDescriptor> tokenDescription) {
    List<FieldDescriptor> result = PayloadDocumentation.applyPathPrefix("", tokenDescription);
    for (FieldDescriptor fieldDescriptor : result) {
      fieldDescriptor.optional();
    }
    return result;
  }

  public UploadedFile createFile() {
    UploadedFile file = new UploadedFile();
    fileDao.add(file);
    return file;
  }

}