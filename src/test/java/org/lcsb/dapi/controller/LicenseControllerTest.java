package org.lcsb.dapi.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.lcsb.dapi.dao.LicenseDao;
import org.lcsb.dapi.model.License;
import org.lcsb.dapi.model.Release;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.RequestBuilder;

import com.google.gson.JsonParser;

@Rollback(true)
@Transactional
public class LicenseControllerTest extends ControllerTestFunctions {

  @Autowired
  LicenseDao licenseDao;

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @After
  public void tearDown() {
  }

  @Test
  public void testCreateLicense() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String content = "{\"url\":\"https://creativecommons.org/licenses/by-nc/4.0/legalcode\", "
        + "\"name\":\"Creative Commons Attribution-NonCommercial 4.0 International\", "
        + "\"content\": \"Using Creative Commons Public Licenses\\n" +
        "Creative Commons public licenses provide a standard set of terms and conditions that "
        + "creators and other rights holders may use to share original works of authorship and "
        + "other material subject to copyright and certain other rights specified in the public "
        + "license below. The following considerations are for informational purposes only, are "
        + "not exhaustive, and do not form part of our licenses...\"}";
    RequestBuilder request = post("/api/license/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("license/create",
            requestFields(getCreateLicenseDescription()),
            responseFields(getLicenseDescription())))
        .andReturn().getResponse().getContentAsString();

    int id = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("id")
        .getAsInt();

    License license = licenseDao.getById(id);
    assertNotNull(license);

  }

  @Test
  public void testCreateLicenseWithTooLongContent() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String licenseContent = new String(new char[65536 + 1]).replace('\0', 'x');

    String content = "{\"url\":\"https://creativecommons.org/licenses/by-nc/4.0/legalcode\", "
        + "\"name\":\"Creative Commons Attribution-NonCommercial 4.0 International\", "
        + "\"content\": \"" + licenseContent + "\"}";
    RequestBuilder request = post("/api/license/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testCreateLicenseWithLongContent() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String licenseContent = new String(new char[65530]).replace('\0', 'x');

    String content = "{\"url\":\"https://creativecommons.org/licenses/by-nc/4.0/legalcode\", "
        + "\"name\":\"Creative Commons Attribution-NonCommercial 4.0 International\", "
        + "\"content\": \"" + licenseContent + "\"}";
    RequestBuilder request = post("/api/license/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCreateLicenseWithInvalidContent() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String content = "{\"url\":\"http://google.com/\"}";
    RequestBuilder request = post("/api/license/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testListLicenses() throws Exception {
    License license = new License("fancy name", "https://creativecommons.org/licenses/by-nc/4.0/legalcode",
        "Using Creative Commons Public Licenses\n" +
            "Creative Commons public licenses provide a standard set of terms and conditions that " +
            "creators and other rights holders may use to share original works of authorship and " +
            "other material subject to copyright and certain other rights specified in the public " +
            "license below. The following considerations are for informational purposes only, are " +
            "not exhaustive, and do not form part of our licenses...");
    licenseDao.add(license);
    RequestBuilder request = get("/api/license/");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("license/get-all",
            getPaginationRequestDescription(),
            getPaginationResponseDescription("licenses", License.class).andWithPrefix("content[].",
                getLicenseDescription())))
        .andReturn().getResponse().getContentAsString();

    int count = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray().size();
    assertEquals(licenseDao.getAll().size(), count);

  }

  @Test
  public void testDeleteLicenses() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    License license = new License("fancy name", "https://creativecommons.org/licenses/by-nc/4.0/legalcode", "blabla");
    licenseDao.add(license);

    int count = licenseDao.getAll().size();

    RequestBuilder request = delete("/api/license/{license}", license.getId() + "").session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("license/delete", pathParameters(parameterWithName("license").description("license id"))));
    assertEquals(count - 1, licenseDao.getAll().size());
  }

  @Test
  public void testDeleteLicenseInUse() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    Release release = mockDatabaseFromFile("testFiles/ThreeDrug.txt", userService.getUserByLogin(TEST_ADMIN_LOGIN));
    License license = release.getLicense();

    RequestBuilder request = delete("/api/license/{license}", license.getId() + "").session(session);

    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  public void testCreateLicenseThatAlreadyExist() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String content = "{\"url\":\"https://creativecommons.org/licenses/by-nc/4.0/legalcode\", "
        + "\"name\":\"Creative Commons Attribution-NonCommercial 4.0 International\", "
        + "\"content\": \"xyz\"}";
    RequestBuilder request = post("/api/license/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

}