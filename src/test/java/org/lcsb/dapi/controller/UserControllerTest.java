package org.lcsb.dapi.controller;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.*;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.lcsb.dapi.controller.dto.UserDto;
import org.lcsb.dapi.model.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.RequestBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonParser;

@Rollback(true)
@Transactional
@ActiveProfiles("emailProfile")
public class UserControllerTest extends ControllerTestFunctions {

  ObjectMapper mapper = new ObjectMapper();

  static Logger logger = LogManager.getLogger();

  @Autowired
  JavaMailSender mailSender;

  @After
  public void tearDown() {
  }

  @Test
  public void testListUsers() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    RequestBuilder request = RestDocumentationRequestBuilders.get("/api/users/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("user/get-all", getPaginationRequestDescription(),
            getPaginationResponseDescription("users", User.class).andWithPrefix("content[].", getUserDescription())))
        .andReturn().getResponse().getContentAsString();

    int count = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray()
        .size();

    assertTrue(count > 0);

  }

  private List<FieldDescriptor> getUserDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("id")
            .description("identifier of the user"),
        fieldWithPath("name")
            .description("user name")
            .type("String"),
        fieldWithPath("email")
            .description("user email address")
            .type("String"),
        fieldWithPath("deleted")
            .description("is the user deleted"),
        fieldWithPath("login")
            .description("user login"),
        fieldWithPath("enabled")
            .description("is the account enabled"),
        fieldWithPath("privileges")
            .description("set of user privileges")
            .type("Array<" + Privilege.class.getSimpleName() + ">").optional()));
    result.addAll(PayloadDocumentation.applyPathPrefix("privileges[].", getPrivilegeDescription()));
    result.add(fieldWithPath("acceptedReleaseLicenses")
        .description("set of accepted release licenses")
        .type("Array<" + AcceptedReleaseLicense.class.getSimpleName() + ">"));
    return result;
  }

  private List<FieldDescriptor> getUserDtoDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("email")
            .description("user email address")
            .type("String"),
        fieldWithPath("login")
            .description("user login"),
        fieldWithPath("password")
            .description("user password")));
    return result;
  }

  @Test
  public void testGrantPrivilegeForUser() throws Exception {
    User user = super.createUser("user", "passwd");
    assertEquals(0, user.getPrivileges().size());
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    String content = mapper.writeValueAsString(new Privilege(PrivilegeType.IS_ADMIN));
    RequestBuilder request = post("/api/users/{login}:grantPrivilege", user.getLogin())
        .session(session)
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("user/grant-privilege",
            pathParameters(parameterWithName("login").description("user login")),
            requestFields(getPrivilegeDescription()),
            responseFields(getUserDescription())));

    assertEquals(1, user.getPrivileges().size());
  }

  private List<FieldDescriptor> getPrivilegeDescription() {
    return Arrays.asList(
        fieldWithPath("type")
            .description("type of the privilege").type("String"),
        fieldWithPath("objectId")
            .description("identifier of the object that privilege refer to")
            .type("Number").optional());
  }

  @Test
  public void testGrantPrivilegeWithoutAccess() throws Exception {
    User user = super.createUser("user", "passwd");
    String content = mapper.writeValueAsString(new Privilege(PrivilegeType.IS_ADMIN));
    RequestBuilder request = post("/api/users/" + user.getLogin() + ":grantPrivilege")
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testRevokePrivilegeForUser() throws Exception {
    User user = super.createUser("user", "passwd");
    Release release = super.createRelease();
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);
    userService.grantUserPrivilege(user, PrivilegeType.READ, release.getId());
    assertEquals(2, user.getPrivileges().size());
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    String content = mapper.writeValueAsString(new Privilege(PrivilegeType.IS_ADMIN));
    RequestBuilder request = post("/api/users/{login}:revokePrivilege", user.getLogin())
        .session(session)
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("user/revoke-privilege",
            pathParameters(parameterWithName("login").description("user login")),
            requestFields(getPrivilegeDescription()),
            responseFields(getUserDescription())));

    assertEquals(1, user.getPrivileges().size());
  }

  @Test
  public void testRevokePrivilegeWithoutAccess() throws Exception {
    User user = super.createUser("user", "passwd");
    String content = mapper.writeValueAsString(new Privilege(PrivilegeType.IS_ADMIN));
    RequestBuilder request = post("/api/users/" + user.getLogin() + ":revokePrivilege")
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGrantInvalidPrivilegeForUser() throws Exception {
    User user = super.createUser("user", "passwd");
    assertEquals(0, user.getPrivileges().size());
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    String content = mapper.writeValueAsString(new Privilege(PrivilegeType.READ, -1));
    RequestBuilder request = post("/api/users/" + user.getLogin() + ":grantPrivilege")
        .session(session)
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    assertEquals(0, user.getPrivileges().size());
  }

  @Test
  public void testListUsersWithoutAccess() throws Exception {
    RequestBuilder request = get("/api/users/");

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testLoginFromDisabledAccount() throws Exception {
    User user = createUser("user", "passwd");
    user.setEnabled(false);
    userDao.update(user);

    RequestBuilder request = post("/api/doLogin")
        .param("login", "user")
        .param("password", "passwd");
    mockMvc.perform(request)
        .andExpect(status().isUnauthorized());
  }

  @Test
  public void testLogin() throws Exception {
    createUser("user", "passwd");

    RequestBuilder request = post("/api/doLogin")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content("login=user&password=passwd");
    mockMvc.perform(request)
        .andDo(document("login",
            requestParameters(parameterWithName("login").description("login"),
                parameterWithName("password").description("password"))))
        .andExpect(status().is2xxSuccessful());

    mockMvc.perform(request)
        .andDo(document("login", requestParameters(parameterWithName("login").description("login"),
            parameterWithName("password").description("password")),
            responseFields(
                fieldWithPath("info")
                    .description("status message"),
                fieldWithPath("login")
                    .description("user login"),
                fieldWithPath("token")
                    .description("session token"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testLogout() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    mockMvc.perform(RestDocumentationRequestBuilders.get("/api/users/").session(session))
        .andExpect(status().is2xxSuccessful());

    RequestBuilder request = post("/api/doLogout").session(session);

    mockMvc.perform(request)
        .andDo(document("logout", responseFields(fieldWithPath("status").description("status message"))))
        .andExpect(status().is2xxSuccessful());

    mockMvc.perform(RestDocumentationRequestBuilders.get("/api/users/").session(session))
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testRegisterUser() throws Exception {
    reset(mailSender);
    UserDto data = new UserDto();
    data.setLogin("xyz");
    data.setPassword("passwd");
    data.setEmail("piotr.gawron@uni.lu");

    String content = mapper.writeValueAsString(data);

    RequestBuilder request = post("/api/users/{login}", "xyz")
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andDo(document("user/create",
            pathParameters(parameterWithName("login").description("user login")),
            requestFields(getUserDtoDescription()),
            responseFields(getUserDescription())))
        .andExpect(status().is2xxSuccessful());

    Mockito.verify(mailSender).send(any(SimpleMailMessage.class));

    assertNotNull(userDao.getUserByLogin("xyz"));
  }

  @Test
  public void testRegisterUserWithDot() throws Exception {
    reset(mailSender);
    UserDto data = new UserDto();
    data.setLogin("piotr.gawron");
    data.setPassword("passwd");
    data.setEmail("piotr.gawron@uni.lu");

    String content = mapper.writeValueAsString(data);

    RequestBuilder request = post("/api/users/{login}", data.getLogin())
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    Mockito.verify(mailSender).send(any(SimpleMailMessage.class));

    assertNotNull(userDao.getUserByLogin(data.getLogin()));
  }

  @Test
  public void testConfirmRegistration() throws Exception {
    UserDto data = new UserDto();
    data.setLogin("xyz");
    data.setPassword("passwd");
    data.setEmail("piotr.gawron@uni.lu");

    String content = mapper.writeValueAsString(data);

    RequestBuilder request = post("/api/users/xyz")
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    User user = userDao.getUserByLogin("xyz");
    assertFalse(user.isEnabled());

    request = get("/registrationConfirm?token=" + user.getToken().getToken());

    mockMvc.perform(request)
        .andDo(document("user/confirm-registration",
            requestParameters(parameterWithName("token")
                .description("token obtained via email")),
            responseFields(getUserDescription())))
        .andExpect(status().is2xxSuccessful());

    assertTrue(user.isEnabled());
  }

  @Test
  public void testGetUserData() throws Exception {
    User user = super.createUser("user", "passwd");
    MockHttpSession session = createSession("user", "passwd");
    RequestBuilder request = RestDocumentationRequestBuilders.get("/api/users/{login}", user.getLogin())
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("user/get-user-data",
            pathParameters(parameterWithName("login").description("user login")),
            responseFields(getUserDescription())))

        .andReturn().getResponse().getContentAsString();

    int id = new JsonParser()
        .parse(response).getAsJsonObject().get("id").getAsInt();

    assertEquals(user.getId(), id);

  }

  @Test
  public void testIsNotAuthenticated() throws Exception {
    RequestBuilder request = RestDocumentationRequestBuilders.get("/isAuthenticated");

    mockMvc.perform(request)
        .andExpect(status().isForbidden())
        .andDo(document("is-authenticated"));
  }

  @Test
  public void testIsAuthenticated() throws Exception {
    super.createUser("user", "passwd");
    MockHttpSession session = createSession("user", "passwd");
    RequestBuilder request = RestDocumentationRequestBuilders.get("/isAuthenticated").session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetUserDataWithoutAccess() throws Exception {
    super.createUser("user", "passwd");
    MockHttpSession session = createSession("user", "passwd");
    RequestBuilder request = RestDocumentationRequestBuilders.get("/api/users/{login}", TEST_ADMIN_LOGIN)
        .session(session);

    mockMvc.perform(request).andExpect(status().isForbidden());
  }

  @Test
  public void testGetUserDataAnonymously() throws Exception {
    RequestBuilder request = RestDocumentationRequestBuilders.get("/api/users/{login}", TEST_ADMIN_LOGIN);

    mockMvc.perform(request).andExpect(status().isForbidden());
  }

}