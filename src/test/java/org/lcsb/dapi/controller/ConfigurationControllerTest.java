package org.lcsb.dapi.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.lcsb.dapi.service.ExternalDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.RequestBuilder;

@Rollback(true)
@Transactional
public class ConfigurationControllerTest extends ControllerTestFunctions {

  Logger logger = LogManager.getLogger();

  @Autowired
  public ExternalDatabaseService service;

  @After
  public void tearDown() {
  }

  @Test
  public void testGetDatabases() throws Exception {
    RequestBuilder request = RestDocumentationRequestBuilders.get("/api/configuration/");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("configuration/get", responseFields(
            fieldWithPath("commitId")
                .description("git commit hash that was used when building DAPI"),
            fieldWithPath("buildTime")
                .description("when DAPI was built"),
            fieldWithPath("buildVersion")
                .description("DAPI version"),
            fieldWithPath("dirty")
                .description("was the build dirty (there were uncommited changes during build process)"))));

  }

}
