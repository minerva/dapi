package org.lcsb.dapi.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    ConfigurationControllerTest.class,
    DrugControllerTest.class,
    ExternalDatabaseControllerTest.class,
    FileControllerTest.class,
    LicenseControllerTest.class,
    ReleaseControllerTest.class,
    UserControllerTest.class,
})
public class AllControllerTests {

}
