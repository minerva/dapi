package org.lcsb.dapi.controller;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.util.*;

import javax.transaction.Transactional;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.lcsb.dapi.ConfigProperties;
import org.lcsb.dapi.dao.FileDao;
import org.lcsb.dapi.model.UploadedFile;
import org.lcsb.dapi.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.RequestBuilder;

import com.google.gson.JsonParser;

@Rollback(true)
@Transactional
public class FileControllerTest extends ControllerTestFunctions {

  Logger logger = LogManager.getLogger();

  @Autowired
  FileDao fileDao;

  @Autowired
  FileService fileService;

  @Autowired
  ConfigProperties config;

  @After
  public void tearDown() {
  }

  @Test
  public void testCreateFile() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    String content = "{\"filename\":\"test\", \"length\":127}";
    RequestBuilder request = post("/api/files/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("file/create",
            requestFields(getCreateFileDescription()),
            responseFields(getFileDescription())))
        .andReturn().getResponse().getContentAsString();

    int id = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("id")
        .getAsInt();

    assertNotNull(fileDao.getById(id));

    assertTrue(new File(config.getFileStorage() + "/" + id).exists());
  }

  @Test
  public void testCreateFileWithIdOverride() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    String content = "{\"filename\":\"test\", \"id\":-1}";
    RequestBuilder request = post("/api/files/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int id = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("id")
        .getAsInt();

    assertTrue(id >= 0);

    assertNotNull(fileDao.getById(id));
  }

  @Test
  public void testCreateFileWithoutPrivileges() throws Exception {
    String content = "{\"filename\":\"test\", \"id\":-1}";
    RequestBuilder request = post("/api/files/")
        .content(content)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testUploadFile() throws Exception {
    int id = 0;
    try {
      MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
      String content = "{\"filename\":\"test\", \"length\":12}";
      RequestBuilder request = post("/api/files/")
          .content(content)
          .session(session)
          .contentType(MediaType.APPLICATION_JSON);

      String response = mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful())
          .andReturn().getResponse().getContentAsString();

      id = new JsonParser()
          .parse(response)
          .getAsJsonObject()
          .get("id")
          .getAsInt();

      content = "file content";
      request = patch("/api/files/{id}", id)
          .content(content)
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful())
          .andDo(document("file/upload-content",
              pathParameters(parameterWithName("id").description("file identifier")),
              responseFields(getFileDescription())));

      File file = new File(config.getFileStorage() + "/" + id);
      String fileContent = FileUtils.readFileToString(file, "UTF-8");
      assertEquals(content, fileContent);
    } finally {
      if (id > 0) {
        fileService.delete(fileDao.getById(id));
      }
    }
  }

  @Test
  public void testUploadInvalidFileContent() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    String content = "{\"filename\":\"test\", \"length\":1}";
    RequestBuilder request = post("/api/files/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    int id = new JsonParser()
        .parse(response)
        .getAsJsonObject()
        .get("id")
        .getAsInt();

    content = "123456789012";
    request = patch("/api/files/" + id)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetAll() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/api/files/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("file/get-all",
            getPaginationRequestDescription(),
            getPaginationResponseDescription("files", UploadedFile.class)))
        .andReturn().getResponse().getContentAsString();

    assertNotNull(response);
    assertFalse(response.isEmpty());
  }

  @Test
  public void testDeleteFileAll() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    UploadedFile file = super.createFile();
    RequestBuilder request = delete("/api/files/{id}", file.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("file/delete",
            pathParameters(parameterWithName("id").description("file id"))));

    assertNull(fileDao.getById(file.getId()));
  }

  protected List<FieldDescriptor> getCreateFileDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("filename")
            .description("name of the file"),
        fieldWithPath("length")
            .description("expected size of the file")));
    return result;
  }

  protected List<FieldDescriptor> getFileDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("id")
            .description("identifier"),
        fieldWithPath("filename")
            .description("file name"),
        fieldWithPath("length")
            .description("size of the file (in bytes)")));
    return result;
  }

}