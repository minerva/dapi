package org.lcsb.dapi.controller;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.transaction.Transactional;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.lcsb.dapi.InvalidArgumentException;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.RequestBuilder;

import com.google.gson.JsonParser;

@Rollback(true)
@Transactional
public class ReleaseControllerTest extends ControllerTestFunctions {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Autowired
  private FileService fileService;

  User user;

  @Before
  public void setUp() {
    user = createUser("user", "user");
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testCreateRelease() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("testFiles/ThreeDrug.txt");
    License license = createLicense();

    try {
      String content = "{\"file\":" + file.getId() + ", \"license\":" + license.getId() + "}";
      RequestBuilder request = post("/api/database/{database}/releases/",
          "DrugBank")
              .content(content)
              .session(session)
              .contentType(MediaType.APPLICATION_JSON);

      String response = mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful())
          .andDo(document("release/create",
              pathParameters(parameterWithName("database").description("database name")),
              requestFields(getCreateReleaseDescription()),
              responseFields(getReleaseDescription())))
          .andReturn().getResponse().getContentAsString();

      int id = new JsonParser()
          .parse(response)
          .getAsJsonObject()
          .get("id")
          .getAsInt();

      Release release = releaseDao.getById(id);
      assertNotNull(release);
      assertEquals(3, release.getChemicalEntities().size());
    } finally {
      removeFile(file);
    }
  }

  @Test
  public void testCreateReleaseFromCompressedDrugbank() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("src/test/resources/testFiles/drugbank/long-synonym.xml.zip");
    License license = createLicense();

    try {
      String content = "{\"file\":" + file.getId() + ", \"license\":" + license.getId() + "}";
      RequestBuilder request = post("/api/database/{database}/releases/",
          "DrugBank")
              .content(content)
              .session(session)
              .contentType(MediaType.APPLICATION_JSON);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());
    } finally {
      removeFile(file);
    }
  }

  @Test
  public void testDeleteRelease() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    Release release = mockDatabaseFromFile("testFiles/ThreeDrug.txt", user);

    RequestBuilder request = delete("/api/database/{database}/releases/{release}",
        "DrugBank", release.getName())
            .session(session);

    int count = releaseDao.getAll().size();
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("release/delete",
            pathParameters(parameterWithName("database").description("database name"),
                parameterWithName("release").description("release identifier"))));

    assertEquals(count - 1, releaseDao.getAll().size());
  }

  private License createLicense() {
    License license = new License("n", "u", "");
    licenseDao.add(license);
    return license;
  }

  @Test
  public void testCreateReleaseFromCtd() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("src/test/resources/testFiles/ctd-csv.zip");
    License license = createLicense();

    try {
      String content = "{\"file\":" + file.getId() + ", \"name\":\"xyz\", \"license\":" + license.getId() + "}";
      RequestBuilder request = post("/api/database/ctd/releases/")
          .content(content)
          .session(session)
          .contentType(MediaType.APPLICATION_JSON);

      String response = mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful())
          .andReturn().getResponse().getContentAsString();

      int id = new JsonParser()
          .parse(response)
          .getAsJsonObject()
          .get("id")
          .getAsInt();

      Release release = releaseDao.getById(id);
      assertNotNull(release);
      assertEquals(1, release.getChemicalEntities().size());
    } finally {
      removeFile(file);
    }

  }

  @Test
  public void testCreateReleaseForInvalidDatabase() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("testFiles/ThreeDrug.txt");

    try {
      String content = "{\"file\":" + file.getId() + "}";
      RequestBuilder request = post("/api/database/unkonwn_db/releases/")
          .content(content)
          .session(session)
          .contentType(MediaType.APPLICATION_JSON);

      mockMvc.perform(request)
          .andExpect(status().isNotFound());
    } finally {
      removeFile(file);
    }
  }

  @Test
  public void testCreateReleaseFromInvalidFileContent() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("pom.xml");
    License license = createLicense();

    try {
      String content = "{\"file\":" + file.getId() + ", \"license\":" + license.getId() + "}";
      RequestBuilder request = post("/api/database/drugbank/releases/")
          .content(content)
          .session(session)
          .contentType(MediaType.APPLICATION_JSON);

      mockMvc.perform(request)
          .andExpect(status().isBadRequest());
    } finally {
      removeFile(file);
    }
  }

  @Test
  public void testCreateReleaseWithoutFile() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    License license = createLicense();

    String content = "{\"license\":" + license.getId() + "}";
    RequestBuilder request = post("/api/database/drugbank/releases/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testCreateReleaseFromInvalidLicense() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("testFiles/ThreeDrug.txt");

    try {
      String content = "{\"file\":" + file.getId() + "}";
      RequestBuilder request = post("/api/database/drugbank/releases/")
          .content(content)
          .session(session)
          .contentType(MediaType.APPLICATION_JSON);

      mockMvc.perform(request)
          .andExpect(status().isBadRequest());
    } finally {
      removeFile(file);
    }
  }

  @Test
  public void testCreateReleaseWithoutPrivileges() throws Exception {
    UploadedFile file = createFile("testFiles/ThreeDrug.txt");

    try {
      String content = "{\"file\":" + file.getId() + "}";
      RequestBuilder request = post("/api/database/drugbank/releases/")
          .content(content)
          .contentType(MediaType.APPLICATION_JSON);

      mockMvc.perform(request)
          .andExpect(status().isForbidden());
    } finally {
      removeFile(file);
    }

  }

  @Test
  public void testCreateReleaseFromInvalidFile() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String content = "{\"file\":-1}";
    RequestBuilder request = post("/api/database/drugbank/releases/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

  @Test
  public void testCreateReleaseFromInvalidFileId() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);

    String content = "{\"file\":\"aloha\"}";
    RequestBuilder request = post("/api/database/drugbank/releases/")
        .content(content)
        .session(session)
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

  private void removeFile(UploadedFile file) {
    fileService.delete(file);
  }

  private UploadedFile createFile(String filename) throws IOException, InvalidArgumentException {
    long length = new File(filename).length();
    UploadedFile result = new UploadedFile();
    result.setFilename(filename);
    result.setLength((int) length);
    fileService.add(result);
    byte[] content = FileUtils.readFileToByteArray(new File(filename));
    fileService.addContent(result.getId(), content);
    return result;
  }

  @Test
  public void testListReleases() throws Exception {
    mockDatabaseFromFile("testFiles/ThreeDrug.txt", user);

    RequestBuilder request = get("/api/database/{database}/releases/", "DrugBank");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("release/get-all",
            pathParameters(parameterWithName("database").description("database name")),
            getPaginationRequestDescription(),
            getPaginationResponseDescription("drugs", Drug.class).andWithPrefix("content[].",
                getReleaseDescription())))
        .andReturn().getResponse().getContentAsString();

    int count = new JsonParser()
        .parse(response).getAsJsonObject().get("content")
        .getAsJsonArray().size();
    assertTrue(count > 0);

  }

  protected List<FieldDescriptor> getCreateReleaseDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("file")
            .description("id of the file from which release will be created"),
        fieldWithPath("license")
            .description("id of the license that is applicable for the release")));
    return result;
  }

  protected List<FieldDescriptor> getReleaseDescription() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("id")
            .description("identifier"),
        fieldWithPath("name")
            .description("name of the release"),
        fieldWithPath("timestamp")
            .description("when it was created")));
    result.addAll(PayloadDocumentation.applyPathPrefix("license.", getLicenseDescription()));
    return result;
  }

  @Test
  public void testCreateReleaseWithLongSynonymName() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("src/test/resources/testFiles/drugbank/long-synonym.xml");
    License license = createLicense();

    try {
      String content = "{\"file\":" + file.getId() + ", \"license\":" + license.getId() + "}";
      RequestBuilder request = post("/api/database/{database}/releases/",
          "DrugBank")
              .content(content)
              .session(session)
              .contentType(MediaType.APPLICATION_JSON);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());
      releaseDao.flush();
    } finally {
      removeFile(file);
    }

  }

  @Test
  public void testCreateReleaseFromInvalidCtd() throws Exception {
    MockHttpSession session = createSession(TEST_ADMIN_LOGIN, TEST_ADMIN_PASSWORD);
    UploadedFile file = createFile("src/test/resources/testFiles/drugbank/long-synonym.xml");
    License license = createLicense();

    try {
      String content = "{\"file\":" + file.getId() + ", \"name\":\"xyz\", \"license\":" + license.getId() + "}";
      RequestBuilder request = post("/api/database/ctd/releases/")
          .content(content)
          .session(session)
          .contentType(MediaType.APPLICATION_JSON);

      mockMvc.perform(request)
          .andExpect(status().isBadRequest());
    } finally {
      removeFile(file);
    }
  }

  @Test
  public void testAcceptRelease() throws Exception {
    MockHttpSession session = createSession("user", "user");

    Release release = mockDatabaseFromFile("testFiles/ThreeDrug.txt", userService.getUserByLogin(TEST_ADMIN_LOGIN));

    assertEquals(0, user.getAcceptedReleaseLicenses().size());

    RequestBuilder request = post("/api/database/{database}/releases/{release}:acceptLicense", "DrugBank",
        release.getName()).session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("release/accept-license",
            pathParameters(parameterWithName("database").description("database name"),
                parameterWithName("release").description("release name"))));

    assertEquals(1, user.getAcceptedReleaseLicenses().size());

    request = get("/api/users/{login}", "user").session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

  }

}