package org.lcsb.dapi.config;

import java.security.GeneralSecurityException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lcsb.dapi.ConfigProperties;
import org.lcsb.dapi.service.LdapService;
import org.lcsb.dapi.service.LdapServiceImpl;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;


@Profile("ldapTest")
@Configuration
public class LdapServiceTestConfiguration {
  static String TEST_LOGIN = "piotr.gawron";
  static String TEST_PASSWD = "test_passwd";
  static String LDAP_FILE_CONTENT = "./src/test/resources/ldap-test.ldif";

  Logger logger = LogManager.getLogger();

  @Autowired
  ConfigProperties configProperties;

  @Bean
  @Primary
  public LdapService createMockLdapService() throws LDAPException, GeneralSecurityException {

    LdapService ldapService = Mockito.spy(new LdapServiceImpl(configProperties));
    Mockito.doAnswer(new Answer<LDAPConnection>() {

      @Override
      public LDAPConnection answer(InvocationOnMock invocation) throws Throwable {
        // Create the configuration to use for the server.
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
        config.addAdditionalBindCredentials("uid=" + TEST_LOGIN + ",cn=users,cn=accounts,dc=uni,dc=lu", TEST_PASSWD);
        config.setSchema(null);

        // Create the directory server instance, populate it with data from the
        // "test-data.ldif" file, and start listening for client connections.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
        ds.importFromLDIF(true, LDAP_FILE_CONTENT);
        ds.startListening();
        return ds.getConnection();
      }
    }).when(ldapService).getConnection();
    return ldapService;
  }

}
