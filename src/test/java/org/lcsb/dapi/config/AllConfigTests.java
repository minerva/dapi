package org.lcsb.dapi.config;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LdapAuthenticationProviderTest.class })
public class AllConfigTests {

}
