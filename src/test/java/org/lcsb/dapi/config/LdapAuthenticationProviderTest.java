package org.lcsb.dapi.config;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.lcsb.dapi.service.ServiceTestFunctions;
import org.lcsb.dapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("ldapTest")
@Rollback(true)
public class LdapAuthenticationProviderTest extends ServiceTestFunctions {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Autowired
  private LdapAuthenticationProvider provider;

  @Autowired
  private UserService userService;

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testUserCreatedFromLdapShouldBeEnabled() throws Exception {
    provider.authenticate(new TestingAuthenticationToken(LdapServiceTestConfiguration.TEST_LOGIN,
        LdapServiceTestConfiguration.TEST_PASSWD));

    assertTrue(userService.getUserByLogin("piotr.gawron").isEnabled());
  }

}
