package org.lcsb.dapi;

import org.mockito.Mockito;
import org.springframework.context.annotation.*;
import org.springframework.mail.javamail.JavaMailSender;

@Profile("emailProfile")
@Configuration
public class EmailProfile {

  @Bean
  @Primary
  public JavaMailSender createMockJavaMailSender() {
    return Mockito.mock(JavaMailSender.class);
  }

}
