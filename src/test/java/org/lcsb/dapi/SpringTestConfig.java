package org.lcsb.dapi;

import javax.sql.DataSource;

import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@Import({ HibernateConfig.class, WebMvcConfig.class })
public class SpringTestConfig {
  @Bean
  public DataSourceInitializer dataSourceInitializer(DataSource dataSource,
      CustomDatabasePopulator customDatabasePopulator) {
    DataSourceInitializer initializer = new DataSourceInitializer();
    initializer.setDataSource(dataSource);
    CompositeDatabasePopulator populator = new CompositeDatabasePopulator();
    populator.addPopulators(customDatabasePopulator,
        new ResourceDatabasePopulator(new ClassPathResource("test-clean.sql"), new ClassPathResource("test-data.sql")));
    initializer.setDatabasePopulator(populator);
    initializer.setEnabled(true);
    return initializer;
  }

}
