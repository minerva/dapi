package org.lcsb.dapi.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.License;
import org.springframework.beans.factory.annotation.Autowired;

public class LicenseDaoTest extends DapiTestFunctions {

  @Autowired
  private LicenseDao licenseDao;

  @Test
  public void testGetByUrl() {
    String url = "http://google.com/";
    License license = new License("name", url, "");
    licenseDao.add(license);

    assertNotNull(licenseDao.getByUrl(url));
  }

}
