package org.lcsb.dapi.dao;

import javax.persistence.PersistenceException;

import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.Privilege;
import org.lcsb.dapi.model.PrivilegeType;
import org.springframework.beans.factory.annotation.Autowired;

public class PrivilegeDaoTest extends DapiTestFunctions {

  @Autowired
  private PrivilegeDao privilegeDao;

  @Test(expected = PersistenceException.class)
  public void testUniqueType() {
    privilegeDao.add(new Privilege(PrivilegeType.READ,1));
    privilegeDao.add(new Privilege(PrivilegeType.READ,1));
    privilegeDao.flush();
  }

}
