package org.lcsb.dapi.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BaseDaoImplTest.class,
    DrugDaoTest.class,
    ExternalDatabaseDaoTest.class,
    LicenseDaoTest.class,
    PrivilegeDaoTest.class,
    UserDaoTest.class,
})
public class AllDaoTests {

}
