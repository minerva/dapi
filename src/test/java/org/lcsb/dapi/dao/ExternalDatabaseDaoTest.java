package org.lcsb.dapi.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.springframework.beans.factory.annotation.Autowired;

public class ExternalDatabaseDaoTest extends DapiTestFunctions {

  @Autowired
  private ExternalDatabaseDao externalDatabaseDao;

  @Test
  public void testGetByNameIsCaseInsensitive() {
    assertNotNull(externalDatabaseDao.getByName("drugbank"));
    assertNotNull(externalDatabaseDao.getByName("DrugBank"));
    assertNotNull(externalDatabaseDao.getByName("DRUGBANK"));
  }

}
