package org.lcsb.dapi.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;

public class UserDaoTest extends DapiTestFunctions {

  @Autowired
  private UserDao userDao;

  @Test
  public void testAdd() {
    int count = userDao.getAll().size();
    User user = new User();
    user.setLogin("X");
    user.setCryptedPassword("Y");
    user.setName("Xxx");
    user.setEmail("a.a@a.lu");
    userDao.add(user);
    assertEquals(count + 1, userDao.getAll().size());
  }

  @Test
  public void testGetByLogin() {
    User user = new User();
    user.setLogin("Xasd");
    user.setCryptedPassword("Y");
    user.setName("Xxx");
    user.setEmail("a.a@a.lu");
    userDao.add(user);
    assertNotNull(userDao.getUserByLogin(user.getLogin()));
    assertNull(userDao.getUserByLogin(user.getLogin().toLowerCase()));
    assertNull(userDao.getUserByLogin(user.getLogin().toUpperCase()));
  }

}
