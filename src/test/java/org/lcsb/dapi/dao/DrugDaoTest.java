package org.lcsb.dapi.dao;

import static org.junit.Assert.*;

import java.util.*;

import javax.persistence.PersistenceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.lcsb.dapi.DapiTestFunctions;
import org.lcsb.dapi.model.*;
import org.lcsb.dapi.service.IdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

public class DrugDaoTest extends DapiTestFunctions {

  Logger logger = LogManager.getLogger();

  @Autowired
  private DrugDao drugDao;

  @Autowired
  private LicenseDao licenseDao;

  @Autowired
  private ReleaseDao releaseDao;

  @Autowired
  private ExternalDatabaseDao externalDatabaseDao;

  @Autowired
  private FileDao fileDao;

  @Autowired
  private IdentifierFactory identifierFactory;

  private Release release;

  @Before
  public void setUp() {

    License license = new License("nam", "u", "");
    licenseDao.add(license);

    UploadedFile file = new UploadedFile();
    fileDao.add(file);

    release = new Release();
    release.setName("version.1.0");
    Date today = Calendar.getInstance().getTime();
    release.setTimestamp(today);
    release.setSourceFile(file);

    release.setExternalDatabase(externalDatabaseDao.getByName("DrugBank"));
    release.setLicense(license);

    releaseDao.add(release);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testAdd() {
    int count = drugDao.getAll().size();
    Drug drug = createDummyDrug(release);
    drugDao.add(drug);

    assertEquals(count + 1, drugDao.getAll().size());
    assertEquals("Expected was Aspirin", "Aspirin", drug.getName());
    assertTrue(drug.getSynonyms().contains("AspirinPainKiller"));
  }

  @Test(expected = PersistenceException.class)
  public void testAddTwoIdenticalDrugs() {
    drugDao.add(createDummyDrug(release));
    drugDao.add(createDummyDrug(release));
    drugDao.flush();
  }

  @Test
  public void testDrugById() {

    Drug drug = createDummyDrug(release);
    drugDao.add(drug);

    ChemicalEntity drug2 = drugDao.getDrugById(drug.getSourceIdentifier().getResourceIdentifier(), drug.getRelease());

    assertNotNull(drug2);
    assertEquals(drug, drug2);
  }

  @Test
  public void testDrugByUnknownId() {
    drugDao.add(createDummyDrug(release));

    ChemicalEntity drug2 = drugDao.getDrugById("blablabla", release);

    assertNull(drug2);
  }

  @Test
  public void testDelete() {
    int count = drugDao.getAll().size();
    Drug drug = createDummyDrug(release);
    drugDao.add(drug);

    assertEquals(count + 1, drugDao.getAll().size());
    drugDao.delete(drug);
    assertEquals(count, drugDao.getAll().size());
  }

  @Test
  public void testUpdate() {
    Drug drug = createDummyDrug(release);
    Release release = drug.getRelease();
    drugDao.add(drug);

    drug.setDescription("This is a very generic drug available at all pharmacy stores");
    drugDao.update(drug);
    assertEquals("This is a very generic drug available at all pharmacy stores",
        drugDao.getDrugById("DB00001", release).getDescription());
  }

  @Test
  public void testSearch() {
    drugDao.clear();
    int count = drugDao.getAll().size();
    Drug drug = createDummyDrug(release);
    Release release = drug.getRelease();
    drugDao.add(drug);

    assertEquals(count + 1, drugDao.getByRelease(release).size());
    assertEquals("Aspirin", drugDao.getDrugById("DB00001", release).getName());
    assertEquals("used to treat headache", drugDao.getDrugById("DB00001", release).getDescription());
    assertEquals("version.1.0", drugDao.getDrugById("DB00001", release).getRelease().getName());
  }

  @Test
  public void testTarget() {
    Drug drug = createDummyDrug(release);
    Release release = drug.getRelease();
    drugDao.add(drug);

    assertEquals(1, drugDao.getDrugById("DB00001", release).getTargets().size());
    assertEquals("kinase", drugDao.getDrugById("DB00001", release).getTargets().get(0).getName());
    assertEquals("9606",
        drugDao.getDrugById("DB00001", release).getTargets().get(0).getOrganism().getResourceIdentifier());
    assertEquals("DB00002", drugDao.getDrugById("DB00001", release).getTargets().get(0).getSourceIdentifier()
        .getResourceIdentifier());
  }

  @Test
  public void testDrugByTargetId() {
    drugDao.add(createDummyDrug(release));

    Map<DrugProperty, Object> options = new HashMap<>();
    options.put(DrugProperty.TARGET_IDENTIFIER, identifierFactory.createIdentifier(IdentifierType.UNIPROT, "P12345"));
    options.put(DrugProperty.RELEASE, release);
    List<ChemicalEntity> drugs = drugDao.getByFilter(Pageable.unpaged(), options).getContent();

    assertEquals(1, drugs.size());
  }

  @Test
  public void testDrugByUnknownTargetIdDatabase() {
    drugDao.add(createDummyDrug(release));

    Map<DrugProperty, Object> options = new HashMap<>();
    options.put(DrugProperty.TARGET_IDENTIFIER, identifierFactory.createIdentifier(IdentifierType.CAS, "P12345"));
    options.put(DrugProperty.RELEASE, release);
    List<ChemicalEntity> drugs = drugDao.getByFilter(Pageable.unpaged(), options).getContent();

    assertEquals(0, drugs.size());
  }

  @Test
  public void testDrugByUnknownTargetId() {
    drugDao.add(createDummyDrug(release));

    Map<DrugProperty, Object> options = new HashMap<>();
    options.put(DrugProperty.TARGET_IDENTIFIER, identifierFactory.createIdentifier(IdentifierType.UNIPROT, "B11111"));
    options.put(DrugProperty.RELEASE, release);
    List<ChemicalEntity> drugs = drugDao.getByFilter(Pageable.unpaged(), options).getContent();

    assertEquals(0, drugs.size());
  }

}
