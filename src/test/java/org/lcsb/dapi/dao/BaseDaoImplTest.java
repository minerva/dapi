package org.lcsb.dapi.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.CALLS_REAL_METHODS;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.junit.Test;
import org.mockito.Mockito;

public class BaseDaoImplTest {

  @Test
  public void testClassShouldBeInLevel2Cache() {

    @Cache(usage = CacheConcurrencyStrategy.NONE)
    class Tmp {

    }
    BaseDaoImpl<?> daoImpl = Mockito.mock(BaseDaoImpl.class, CALLS_REAL_METHODS);

    assertFalse(daoImpl.classShouldBeInLevel2Cache(Object.class));
    assertTrue(daoImpl.classShouldBeInLevel2Cache(Tmp.class));
  }

}
